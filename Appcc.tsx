import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Thestack from './routes/Thestack';
import * as Sentry from 'sentry-expo'

export default function App() {

  Sentry.init({
    enableInExpoDevelopment: true,
    dsn: "https://5e0d9e5d1a4e42e08e2856d8e804b8da@o320329.ingest.sentry.io/5903411",
  });
  return <Thestack/>
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
