import Login from "../components/auth/login";
import Landing from "../components/auth/landing";
import Home from "../components/home/home";
import Phone from "../components/auth/phoneauth";
import Verify from "../components/auth/verifyphone";
import Checkout from "../components/orders/checkout";
import Myorder from "../components/orders/myorder";
import Loans from "../components/loans/loans";
import Signup from "../components/auth/signup";
import Verifysignup from "../components/auth/verifysignup";
import Name from "../components/auth/name";
import Category from "../components/auth/category";
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'


const screens = {
    Landing: {
        screen: Landing,
        detachPreviousScreen: true,
        navigationOptions: {
            hideheader: true,
            headerShown: false,
            detachPreviousScreen: true
        }
    },
    Login: {
        screen: Login,
        detachPreviousScreen: true,
        navigationOptions: {
            title: 'Login',
            hideheader: true,
            headerShown: false,
            detachPreviousScreen: true
        }
    },
    Home: {
        screen:Home,
        detachPreviousScreen: true,
        navigationOptions:{
            title: 'Home',
            headerShown: false,
            detachPreviousScreen: true,
        }
    },
    Phone: {
        screen: Phone,
        detachPreviousScreen: true,
        navigationOptions: {
            title: 'Enter your phone number',
            hideheader: true,
            headerShown: false,
            detachPreviousScreen: true
        }
    },
    Verify: {
        screen: Verify,
        detachPreviousScreen: true,
        navigationOptions: {
            title: 'Enter pin',
            hideheader: true,
            headerShown: false,
            detachPreviousScreen: true
        }
    },
    Checkout: {
        screen: Checkout,
        navigationOptions: {
            title: 'Checkout'
        }
    },
    Myorder: {
        screen: Myorder,
        navigationOptions: {
            title: 'My orders',
            hideheader: true,
            headerShown: false
        }
    },
    Signup: {
        screen: Signup,
        detachPreviousScreen: true,
        navigationOptions: {
            title: 'My orders',
            //hideheader: true,
            headerShown: false,
            detachPreviousScreen: true
        }
    },
    Verifysignup: {
        screen: Verifysignup,
        detachPreviousScreen: true,
        navigationOptions: {
            title: 'My orders',
            hideheader: true,
            detachPreviousScreen: true,
            headerShown: false
        }
    },
    UsaName: {
        screen: Name, 
        navigationOptions: {
            title: 'Name',
            hideheader: true,
            headerShown: false
        }
    },
    Category: {
        screen: Category,
        navigationOptions: {
            title: 'My orders',
            hideheader: true,
            headerShown: false
        }
    }

}

const Thestack = createStackNavigator(screens)

export default createAppContainer(Thestack)