import React from 'react';
import * as Sentry from "@sentry/react-native";
import Login from "./components/auth/login";
import Landing from "./components/auth/landing";
//import Home from "./components/home/home";
import Phone from "./components/auth/phoneauth";
import Verify from "./components/auth/verifyphone";
import Signup from "./components/auth/signup";
import Home from './components/home/home';
import Categories from './components/home/categories'
import Productpage from './components/home/productpage';
import Testa from './components/home/test';
import Cart from './components/cart/cart';
import Checkout from './components/cart/checkout'
import Business from './components/auth/businessname';
import Uploads from './components/auth/uploads';
import Success from './components/auth/success';
import Referal from './components/profile/referal';
import Resetp from './components/profile/resetpin';
//import store from './storage/store';
//import persistor from './storage/store'
import { getStore, getPersistor } from './storage/store';
import { NavigationContainer } from '@react-navigation/native';
//import { createStackNavigator } from '@react-navigation/stack';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ApplicationProvider, List, Button, Layout, Text } from '@ui-kitten/components';
import * as eva from '@eva-design/eva'
import { Provider } from "react-redux"
import { PersistGate } from 'redux-persist/integration/react';

const Stack = createNativeStackNavigator();
const myStore = getStore()
const myPersistor = getPersistor()

Sentry.init({
    dsn: "https://5e0d9e5d1a4e42e08e2856d8e804b8da@o320329.ingest.sentry.io/5903411",
    enableInExpoDevelopment: true,
    debug: true,
  });


const config = {
    animation: 'spring',
    config: {
      stiffness: 1000,
      damping: 500,
      mass: 3,
      overshootClamping: true,
      restDisplacementThreshold: 0.01,
      restSpeedThreshold: 0.01,
    },
  };


const Mystack = () => {
    return (
        <ApplicationProvider {...eva} theme={eva.light}>
            <Provider store={myStore}>
                <PersistGate loading={null} persistor={myPersistor}>
                <NavigationContainer>
                    <Stack.Navigator  >
                    <Stack.Screen
                            name="Home"
                            component={Home}
                            options={{
                                headerShown: false,
                                gestureEnabled: false
                            }} />

                        <Stack.Screen
                            name="Login"
                            component={Login}
                            options={{
                                headerShown: false,

                            }}
                        />
                        <Stack.Screen
                            name="Categories"
                            component={Categories}
                            options={{
                                headerShown: false,

                            }}
                        />
                        

                        <Stack.Screen
                            name="Phome"
                            component={Phone} />

                        <Stack.Screen
                            name="Signup"
                            component={Signup}
                            options={{
                                headerShown: false,
                                gestureEnabled: false
                            }}
                        />
                        <Stack.Screen
                            name="Signin"
                            component={Phone}
                            options={{
                                headerShown: false,
                                gestureEnabled: false
                            }}
                        />
                        <Stack.Screen
                            name="Verify"
                            component={Verify}
                            options={{
                                headerShown: false,
                                gestureEnabled: false
                            }}
                        />
                        <Stack.Screen
                            name="Product"
                            component={Productpage}
                            options={{
                                headerShown: false,
                                gestureEnabled: true,
                                /*transitionSpec: {
                                    open: config,
                                    close: config
                                }*/
                            }}
                            />
                            <Stack.Screen
                            name="Testa"
                            component={Testa}
                            options={{
                                headerShown: false,
                                gestureEnabled: true,
                                /*transitionSpec: {
                                    open: config,
                                    close: config
                                }*/
                            }}
                            />
                            <Stack.Screen
                            name="Cart"
                            component={Cart}
                            options={{
                                headerShown: false,
                                gestureEnabled: true,
                                /*transitionSpec: {
                                    open: config,
                                    close: config
                                }*/
                            }}
                            />
                            <Stack.Screen
                            name="Checkout"
                            component={Checkout}
                            options={{
                                headerShown: false,
                                gestureEnabled: true,
                                /*transitionSpec: {
                                    open: config,
                                    close: config
                                }*/
                            }}
                            />
                            <Stack.Screen
                            name="Business"
                            component={Business}
                            options={{
                                headerShown: false,
                                gestureEnabled: true,
                                /*transitionSpec: {
                                    open: config,
                                    close: config
                                }*/
                            }}
                            />
                            <Stack.Screen
                            name="Uploads"
                            component={Uploads}
                            options={{
                                headerShown: false,
                                gestureEnabled: true,
                                /*transitionSpec: {
                                    open: config,
                                    close: config
                                }*/
                            }}
                            />
                            <Stack.Screen
                            name="Success"
                            component={Success}
                            options={{
                                headerShown: false,
                                gestureEnabled: true,
                            }}
                            />
                            <Stack.Screen
                            name="Referal"
                            component={Referal}
                            options={{
                                headerShown: false,
                                gestureEnabled: true,
                            }}
                            />
                            <Stack.Screen
                            name="Resetp"
                            component={Resetp}
                            options={{
                                headerShown: false,
                                gestureEnabled: true,
                            }}
                            />
                    </Stack.Navigator >

                </NavigationContainer>
                </PersistGate>
            </Provider>
        </ApplicationProvider>
    )
}

export default function App() {
    return (
        <Mystack />
    )
}