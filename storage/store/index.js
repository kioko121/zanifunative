import { createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import rootReducer from "../reducers";
import { composeWithDevTools } from "redux-devtools-extension";
import AsyncStorage from "@react-native-async-storage/async-storage";

const persistConfig = {
    key: 'root',
    storage: AsyncStorage
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = createStore(persistedReducer)
const persistor = persistStore(store)

const getPersistor = () => persistor

const getStore = () => store

const getState = () => {
    return store.getState()
}

export {
    getStore, getState, getPersistor
}

export default {
    getStore, getState, getPersistor
}
/**
 * import { createStore } from "redux";
import rootReducer from "../reducers";
import { composeWithDevTools } from "redux-devtools-extension";

const store = createStore(rootReducer, composeWithDevTools())

export default store
*/