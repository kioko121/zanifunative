import { ADD_VERIFICATION } from "../constants/action-types";
import { ADD_KEY } from "../constants/action-types";
import { ADD_CHOSEN } from "../constants/action-types";
import { ADD_DARKMODE } from "../constants/action-types";
import { ADD_CART } from "../constants/action-types";
import { ADD_PHONE } from "../constants/action-types";
import { LOGOUT } from "../constants/action-types"

const initialState = {
    verification: "",
    key: "",
    chosen: "",
    darkmode: false,
    cart: "",
    phone: "",
    newbie: false,
    busnessid: "",
    cart2:[]
}

function rootReducer(state = initialState, action) {
    if (action.type == ADD_VERIFICATION) {
        return Object.assign({}, state, {
            verification: state.verification.concat(action.payload)
        })
    }
    if (action.type == ADD_KEY) {
        return Object.assign({}, state, {
            key: action.payload
        })
    }
    if (action.type == ADD_CHOSEN) {
        return Object.assign({}, state, {
            chosen: action.payload
        })
    }
    if (action.type == ADD_DARKMODE) {
        return Object.assign({}, state, {
            darkmode: action.payload
        })
    }
    if (action.type == "ADD_CART") {
        return Object.assign({}, state, {
            cart: action.payload
        })
    }
    if (action.type == LOGOUT) {
        return Object.assign({}, state, {
            key: action.payload
        })
    }

    if (action.type == ADD_PHONE){
        return Object.assign({}, state, {
            phone: action.payload
        })
    }

    if (action.type == "ADD_NEWBIE"){
        return Object.assign({}, state, {
            newbie: action.payload
        })
    }

    if (action.type == "ADD_ID"){
        return Object.assign({}, state, {
            busnessid: action.payload
        })
    }
    if (action.type == "ADD_CART2"){
        return Object.assign({}, state, {
            cart2: action.payload
        })
    }

    return state
}

export default rootReducer