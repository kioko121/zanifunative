import { ADD_VERIFICATION } from "../constants/action-types";

export function addVerification(payload)
{
    return { type: ADD_VERIFICATION, payload}
}