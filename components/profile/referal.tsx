import * as React from 'react'
import { Alert, ImageBackground, View, StyleSheet, ActivityIndicator, KeyboardAvoidingView, ScrollView, Image, TouchableNativeFeedback, Pressable } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, List, Layout, Text, Input, Icon, Select, SelectItem, Card, ListItem } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import { ToastAndroid, Modal } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import { Divider } from 'react-native-paper';
import { TextInput } from 'react-native-paper';
import * as Sentry from 'sentry-expo'
import Ionicons from '@expo/vector-icons/build/Ionicons';
import { Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
const { width, height } = Dimensions.get("window")
import { logger } from "react-native-logs"
import { Spinner } from '@ui-kitten/components';
import { invitetxt } from '../../globals';
import { Button} from 'react-native-paper'

export default function Referal({ navigation }) {


    let [fontsLoaded] = useFonts({
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })

    if (!fontsLoaded) {
        return (<View style={styles.container}>
            <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                <Spinner />
            </View>
        </View>
        )
    }

    else {
        return (
            <View style={styles.container}>
                <View style={{ paddingTop: 20, paddingBottom: 20, alignItems: 'flex-start', paddingLeft: 40 }}>
                    <Pressable onPress={() => navigation.goBack()}>
                        <MaterialCommunityIcons name="arrow-left" size={33} />
                    </Pressable>
                </View>
                <View style={{ alignItems: 'flex-start', justifyContent: 'center', paddingBottom: 60, paddingLeft: 40, paddingRight: 20 }}>
                    <Text style={{ fontFamily: 'Osanssemi', fontSize: 23 }}>Invite a friend</Text>

                </View>

                <View style={{ alignItems: 'center' }}>
                    <View style={{
                        //paddingTop: 20,
                        height: 180,
                        width: 180,
                        borderRadius: 90,
                        backgroundColor: '#f7f7f7',
                        alignItems: 'center',
                        alignContent: 'center',
                        justifyContent: 'center'
                    }}>
                        <Image source={require('../../assets/gift.png')} />

                    </View>
                </View>

                <View style={{ alignItems: 'center', paddingTop: 40}}>
                    <Text style={{ fontFamily: 'Osanssemi', fontSize: 20 }}>Invite your friends</Text>
                </View>

                <View style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 40, alignItems: 'center'}}>
                    <Text style={{ fontFamily: 'Osans' }}>{invitetxt}</Text>
                </View>

                <View style={{ alignItems: 'center', paddingTop: 20}}>
                    <Button
                        style={{
                            width: width * 1 / 1.2,
                            marginTop: 20
                        }}
                        color='#7a7a7a'
                        uppercase={false}
                        contentStyle={{
                            height: 60
                        }}
                        mode="contained"
                        labelStyle={{
                            fontFamily: 'Osanssemi',
                            color: 'white'
                         }}
                    >Invite Friends</Button>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 8,
        //paddingLeft: 20,
        //paddingRight: 20,
        paddingTop: Constants.statusBarHeight
    }
})