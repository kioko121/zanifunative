import * as React from 'react'
import { Alert, ImageBackground, View, StyleSheet, ActivityIndicator, KeyboardAvoidingView, ScrollView, Image, TouchableNativeFeedback, Pressable } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, List, Layout, Text, Input, Icon, Select, SelectItem, Card, ListItem } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import { ToastAndroid, Modal } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import { Divider } from 'react-native-paper';
import * as Sentry from 'sentry-expo'
import Ionicons from '@expo/vector-icons/build/Ionicons';
import { Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
const { width, height } = Dimensions.get("window")
import { logger } from "react-native-logs"
import { Spinner } from '@ui-kitten/components';
import { invitetxt } from '../../globals';
import { Button , TextInput} from 'react-native-paper'

export default function Resetp({ navigation }) {

    let [fontsLoaded] = useFonts({
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })

    if (!fontsLoaded) {
        return (<View style={styles.container}>
            <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                <Spinner />
            </View>
        </View>
        )
    }

    else {
        return (
            <View style={styles.container}>
                 <View style={{ paddingTop: 20, paddingBottom: 20, alignItems: 'flex-start', paddingLeft: 40 }}>
                    <Pressable onPress={() => navigation.goBack()}>
                        <MaterialCommunityIcons name="arrow-left" size={33} />
                    </Pressable>
                </View>
                <View style={{ alignItems: 'flex-start', justifyContent: 'center', paddingBottom: 60, paddingLeft: 40, paddingRight: 20 }}>
                    <Text style={{ fontFamily: 'Osanssemi', fontSize: 23 }}>Reset Pin</Text>
                </View>

                <View style={{ paddingLeft: 20, paddingRight: 20}}>
                    <TextInput
                        mode="outlined"
                        style={{  backgroundColor: 'white' }}
                        label="Enter your current pin"
                    />
                </View>
                <View style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 30}}>
                    <TextInput
                        mode="outlined"
                        style={{  backgroundColor: 'white' }}
                        label="Enter new pin"
                    />
                </View>

                <View style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 30}}>
                    <TextInput
                        mode="outlined"
                        style={{  backgroundColor: 'white' }}
                        label="Re-enter new pin"
                    />
                </View>

                <View style={{ alignItems: 'center', paddingTop: 20}}>
                    <Button
                        style={{
                            width: width * 1 / 1.2,
                            marginTop: 20
                        }}
                        color='#7a7a7a'
                        uppercase={false}
                        contentStyle={{
                            height: 60
                        }}
                        mode="contained"
                        labelStyle={{
                            fontFamily: 'Osanssemi',
                            color: 'white'
                         }}
                    >Reset Pin</Button>
                </View>
            </View>
        )
    }
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 8,
        //paddingLeft: 20,
        //paddingRight: 20,
        paddingTop: Constants.statusBarHeight
    }
})