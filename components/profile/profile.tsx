import * as React from 'react'
import { Alert, ImageBackground, View, StyleSheet, ActivityIndicator, KeyboardAvoidingView, ScrollView, Image, TouchableNativeFeedback, Pressable } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, List, Button, Layout, Text, Input, Icon, Select, SelectItem, Card, ListItem } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import { ToastAndroid, Modal } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import { Divider } from 'react-native-paper';
import { TextInput } from 'react-native-paper';
import * as Sentry from 'sentry-expo'
import Ionicons from '@expo/vector-icons/build/Ionicons';
import { Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
const { width, height } = Dimensions.get("window")
import { logger } from "react-native-logs"
import { Spinner } from '@ui-kitten/components';

export default function Profile({ navigation }) {

    const dispatch = useDispatch()
    const log = logger.createLogger()
    const isdark = useSelector((state) => state.darkmode)
    const phone = useSelector((state) => state.phone)

    let [fontsLoaded] = useFonts({
        'Hindbold': require('../../assets/HindSiliguri-Bold.ttf'),
        'Hindregular': require('../../assets/HindSiliguri-Regular.ttf'),
        'Hindmedium': require('../../assets/HindSiliguri-Medium.ttf'),
        'Hindsemi': require('../../assets/HindSiliguri-SemiBold.ttf'),
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })

    if (!fontsLoaded) {
        return (<View style={styles.container}>
            <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                <Spinner />
            </View>
        </View>
        )
    }
    else {
        return (

            <ScrollView style={styles.container}>
                {/*<View style={{ paddingTop: 60, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text style={{ fontFamily: 'Hindbold', fontSize: 27 }}>My account</Text>
                    <Pressable
                        onPress={() => {
                            //fireman.auth().currentUser?.updateProfile({ displayName: 'Justin' })
                            fireman.auth().signOut()
                            navigation.navigate('Login')
                        }}>
                        <Text
                            style={{
                                color: "#B6B3B3",
                                fontFamily: 'Hindregular',
                                fontSize: 18
                            }}
                        >Logout</Text>
                    </Pressable>



                        </View>*/}

                <View style={{ flexDirection: 'row', paddingTop: 40, justifyContent: 'space-between', alignItems: 'center' }}>
                    <View >
                        <Text style={{ fontFamily: 'Osanssemi', fontSize: 21 }}>My account</Text>
                    </View>



                    <Pressable onPress={() => {
                        /*log.debug(isdark)
                        dispatch({
                            type: "ADD_DARKMODE",
                            payload: true
                        })
                        log.debug(isdark)*/
                        dispatch({
                            type: "LOGOUT",
                            payload: ""
                        })
                        navigation.navigate('Login')
                    }}>
                        <View style={{

                            height: 40,
                            width: 80,
                            borderRadius: 10,
                            alignItems: 'center',
                            justifyContent: 'center',
                            //elevation: 5
                        }}>
                            <Text style={{ fontFamily: 'Osanssemi', color: 'grey', fontSize: 16 }}>Log out</Text>
                        </View>
                    </Pressable>

                </View>

                <View style={{ paddingTop: 40, flexDirection: 'column' }}>
                    <Text style={{ fontFamily: 'Osans', color: 'black' }}>{phone}</Text>
                    <Text style={{ fontFamily: 'Osans', color: 'grey' }}> View details</Text>
                </View>




                <View style={{ borderWidth: 0.5, borderRadius: 7, borderColor: 'gray', marginTop: height * 1/9 }}>
                    <Pressable>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 20 }}>
                            <Ionicons name="md-person-outline" size={20} color='black' />
                            <Text style={{ fontFamily: 'Osans' }}>Help & FAQ'S</Text>
                            <MaterialCommunityIcons name="chevron-right" color='black' size={20} />
                        </View>
                        <View style={{ height: 0.5, backgroundColor: 'gray' }} />
                    </Pressable>

                    <Pressable>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 20 }}>
                            <Ionicons name="ios-key-outline" size={20} color='black' />
                            <Text style={{ fontFamily: 'Osans' }}>Privacy Poilicy</Text>
                            <MaterialCommunityIcons name="chevron-right" color='black' size={20} />
                        </View>
                        <View style={{ height: 0.5, backgroundColor: 'gray' }} />
                    </Pressable>

                    <Pressable>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 20 }}>
                            <Ionicons name="ios-newspaper-outline" size={20} color='black' />
                            <Text style={{ fontFamily: 'Osans' }}>Terms & Conditions</Text>
                            <MaterialCommunityIcons name="chevron-right" color='black' size={20} />
                        </View>
                        <View style={{ height: 0.5, backgroundColor: 'gray' }} />
                    </Pressable>

                    <Pressable onPress={()=> navigation.navigate('Referal')}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 20 }}>
                            <Ionicons name="md-share-social-outline" size={20} color='black' />      
                            <Text style={{ fontFamily: 'Osans' }}>Refer a friend</Text>
                            <MaterialCommunityIcons name="chevron-right" color='black' size={20} />
                        </View>
                        <View style={{ height: 0.5, backgroundColor: 'gray' }} />
                    </Pressable>

                    <Pressable>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 20 }}>
                            <Ionicons name="md-document-text-outline" size={20} color='black' />
                            <Text style={{ fontFamily: 'Osans' }}>Your Documents</Text>
                            <MaterialCommunityIcons name="chevron-right" color='black' size={20} />
                        </View>
                        <View style={{ height: 0.5, backgroundColor: 'gray' }} />
                    </Pressable>

                    <Pressable onPress={() => navigation.navigate('Resetp')}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 20 }}>
                            <Ionicons name="md-person-outline" size={20} color='black' />
                            <Text style={{ fontFamily: 'Osans' }}>Reset Pin</Text>
                            <MaterialCommunityIcons name="chevron-right" color='black' size={20} />
                        </View>
                    </Pressable>

                </View>





            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 8,
        paddingLeft: 20,
        paddingRight: 20
    }
})