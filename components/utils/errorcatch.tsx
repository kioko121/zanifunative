import * as React from 'react';
import * as Sentry from "sentry-expo"

export default class ErrorBoundary extends React.Component {

    
    constructor(props) {
      super(props);
        this.state = { hasError: false };
        Sentry.init({
            dsn: "https://5e0d9e5d1a4e42e08e2856d8e804b8da@o320329.ingest.sentry.io/5903411",
            enableInExpoDevelopment: true,
            debug: true,
          });
    }

    
  
    static getDerivedStateFromError(error) {
      // Update state so the next render will show the fallback UI.
      return { hasError: true };
    }
  
    componentDidCatch(error, errorInfo) {
      // You can also log the error to an error reporting service
        Sentry.Native.captureException(error)
        Sentry.Native.captureException(errorInfo)
      //logErrorToMyService(error, errorInfo);
    }
  
    render() {
      if (this.state.hasError) {
        // You can render any custom fallback UI
        return <h1>Something went wrong.</h1>;
      }
  
      return this.props.children; 
    }
  }