import * as React from 'react'
import {
    ScrollView,
    View,
    StyleSheet,
    Pressable,
    FlatList,
    Image,
    Picker,
    BackHandler,
    KeyboardAvoidingView,
    Modal,
    Alert,
    ToastAndroid
} from 'react-native';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, List, Layout, Text, Input, ViewPager, Tooltip } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';

import Constants from 'expo-constants';

//import { FirebaseRecaptchaVerifierModal } from 'expo-firebase-recaptcha';
import { Dimensions } from 'react-native';
import { logger } from "react-native-logs"
const { width, height } = Dimensions.get("window")
import { useDispatch, useSelector } from 'react-redux';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { BlurView } from 'expo-blur';
import { Item } from 'react-native-paper/lib/typescript/components/List/List';
import Animated, { useSharedValue, useAnimatedStyle, withSpring } from 'react-native-reanimated';
import { fireman } from '../../api/base';
import { Spinner } from '@ui-kitten/components';
import { Chip, Snackbar } from 'react-native-paper';
import RBSheet from "react-native-raw-bottom-sheet";
import { RadioButton, TextInput, Button } from 'react-native-paper';
import { Radio, RadioGroup } from '@ui-kitten/components';


export default function Checkout({ navigation }) {

    let [fontsLoaded] = useFonts({

        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })

    function handleBackButtonClick() {
        navigation.navigate('Home');
        return true;
    }

    const [cart, addCart] = useState([])
    const [cartItems, addCartItems] = useState([])
    const [nihowmuch, setNihowmuch] = useState(0)
    const offset = useSharedValue(0);
    const [selected, setSelected] = useState(3);
    const [value, setValue] = React.useState('');
    const [owner, setOwner] = useState('Till Number')
    const [modalVisible, setModalVisible] = useState(false);
    const [loading, setLoading] = useState(false)
    const [checked, setChecked] = React.useState('first');
    const [tillnumber, setTillnumber] = useState()
    const [visiblegood, setVisiblegood] = React.useState(false);
    const [visiblebad, setVisiblebad] = React.useState(false);
    const [visibletill, setVisibletill] = useState(false)
    const [isloading, setIsloading] = useState(false)
    const [manual, setManual] = useState(false)
    const [message, setMessage] = useState('')

    const refRBSheet = useRef();

    const onToggleSnackBar = () => setVisiblegood(!visiblegood);
    const onDismissSnackBar = () => setVisiblegood(false);

    const onToggleSnackBarbad = () => setVisiblebad(!visiblebad);
    const onDismissSnackBarbad = () => setVisiblebad(false);

    const onToggleSnackBartill = () => setVisibletill(!visibletill);
    const onDismissSnackBartill = () => setVisibletill(false);



    const options = [
        { id: 1, name: "Zanifu" },
        { id: 2, name: "Mpesa" },
        { id: 3, name: "Pay on Delivery" },
        { id: 4, name: "Select" },
    ]

    const key = useSelector((state) => state.key)
    const phone = useSelector((state) => state.phone)
    const log = logger.createLogger()

    const getTill = (numba) => {
        setLoading(true)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${key}`);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`https://development.zanifu.com/api/tills/${numba}`, requestOptions)
            .then(response => {
                var status = response.status;
                console.log(status)

                if (status == 200) {
                    response.text().then((data) => {
                        var first = JSON.parse(data)
                        log.debug(first.data.till_name)
                        setOwner(first.data.till_name)
                        setTillnumber(numba)
                    })
                }
                if (status == 404) {
                    setOwner("Till not found")
                    setLoading(false)
                    setTillnumber()
                }
                else {
                    setLoading(false)
                }
            })
            .catch(error => console.log('error', error));
    }


    const getCartItems = async () => {
        try {
            var ninja = []
            var things = []
            var sum = 0
            var ref = fireman.database().ref(`/cartitems/${phone}`)
            addCart([])
            ref.once('value', (snapshot) => {
                snapshot.forEach(function (childSnapshot) {
                    var key = childSnapshot.key
                    var data = childSnapshot.val()

                    //log.debug(data)

                    var mig = data.price * data.quantity

                    //var kis = nihowmuch + mig

                    sum = sum + mig

                    //setNihowmuch(kis)
                    // ninja.push(data.price)

                    things.push({id: data.id, name: data.name, price: data.price, quantity: data.quantity, distributor: data.distrtibutor, image: data.image })
                    //ninja.push({id: data.id, quantity: data.quantity})
                    addCart(oldArray => [...oldArray, { id: data.id, quantity: data.quantity }])
                })
                setNihowmuch(sum)
                //setCart(things)
                console.log("*******************************")
                //console.log(things)
                //console.log(cart)
                addCartItems(things)
                //addCart(ninja)
                log.debug(cartItems)

                // for (let i = 0; i < ninja.length; i++) {
                //     sum += ninja[i];
                // }

                //setNihowmuch(sum)
            })


        } catch (err) {
            log.error(err)
        }
    }
    

    const zanifuPay = async () => {
        setIsloading(true)
        if (selected == 3) {
            ToastAndroid.showWithGravityAndOffset(
                'Please pick an option',
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50
            )
            setIsloading(false)
        } else {
            if (selected != 2  ) {
                if (!tillnumber) {
                    onToggleSnackBartill()
                setIsloading(false)
                }
            }
            if (selected == 2) {
                log.debug("manual.....")
                var myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                myHeaders.append("Accept", "application/json");
                myHeaders.append("Authorization", `Bearer ${key}`);

                var raw = JSON.stringify({
                    "payment_method": selected == 0 ? "zanifu_credit" : selected == 1 ? "mpesa" : selected == 2 ? "pay_later" : "none",
                    "till": 56349,
                    "phone_number": phone,
                    "products": cart
                });

                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };

                fetch("https://development.zanifu.com/api/checkout", requestOptions)
                    .then(response => {
                        var status = response.status;

                        if (status == 200) {

                            response.text().then((data) => {
                                log.debug(data)
                                setMessage(data)
                                onToggleSnackBar()

                                var ref = fireman.database().ref(`/cartitems/${phone}`)
                                ref.remove()
                                    .then(function (data) {
                                        navigation.navigate('Home')
                                    })
                            })



                            /*var ref = fireman.database().ref(`/cartitems/${phone}`)
                            ref.remove()
                                .then(function (data) {
                                    navigation.navigate('Home')
                            })*/

                            setIsloading(false)


                        } else {

                            console.log(status)
                            
                            onToggleSnackBarbad()
                            setIsloading(false)


                        }
                    })
                    .catch(error => console.log('error', error));
            }
            else {
                var myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                myHeaders.append("Accept", "application/json");
                myHeaders.append("Authorization", `Bearer ${key}`);

                var raw = JSON.stringify({
                    "payment_method": selected == 0 ? "zanifu_credit" : selected == 1 ? "mpesa" : selected == 2 ? "pay_later" : "none",
                    "till": tillnumber,
                    "phone_number": phone,
                    "products": cart
                });

                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };

                fetch("https://development.zanifu.com/api/checkout", requestOptions)
                    .then(response => {
                        var status = response.status;

                        if (status == 200) {

                            response.text().then((data) => {
                                log.debug(data)
                                setMessage(data)
                                onToggleSnackBar()

                                var ref = fireman.database().ref(`/cartitems/${phone}`)
                                ref.remove()
                                    .then(function (data) {
                                        navigation.navigate('Home')
                                    })
                            })



                            /*var ref = fireman.database().ref(`/cartitems/${phone}`)
                            ref.remove()
                                .then(function (data) {
                                    navigation.navigate('Home')
                            })*/

                            setIsloading(false)


                        } else {

                            console.log(status)
                            
                            onToggleSnackBarbad()
                            setIsloading(false)


                        }
                    })
                    .catch(error => console.log('error', error));
            }
        }
    }
    useEffect(() => {
        getCartItems()
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };

    }, [])


    if (!fontsLoaded) {
        return (<View style={styles.container}>
            <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingTop: height * 1 / 2 }}>
                <Spinner
                />
            </View>
        </View>
        )
    }
    else {
        return (
            <LinearGradient
                // Button Linear Gradient
                colors={['#fff', '#fff', '#fff']}
                style={styles.container}
            >
                <Snackbar
                    //duration={700000}
                    style={{ backgroundColor: 'green' }}
                    visible={visiblegood}
                    onDismiss={onDismissSnackBar}
                /*action={{
                    label: 'Undo',
                    onPress: () => {
                        // Do something
                    },
                }}*/
                >
                    {message}
                </Snackbar>

                <Snackbar
                    //duration={700000}
                    style={{ backgroundColor: 'red' }}
                    visible={visiblebad}
                    onDismiss={onDismissSnackBarbad}
                >
                    Something went wrong
                </Snackbar>

                <Snackbar
                    //duration={700000}
                    style={{ backgroundColor: 'red' }}
                    visible={visibletill}
                    onDismiss={onDismissSnackBartill}
                >
                    Till number Required
                </Snackbar>

                <RBSheet
                    height={height * 1 / 2}
                    ref={refRBSheet}
                    closeOnDragDown={true}
                    closeOnPressMask={false}
                    customStyles={{
                        wrapper: {
                            backgroundColor: "#0008"
                        },
                        draggableIcon: {
                            backgroundColor: "#000"
                        }
                    }}
                >

                    <View style={{ paddingLeft: 40, paddingRight: 40, alignItems: 'flex-start' }}>
                        <RadioGroup
                            selectedIndex={selected}
                            onChange={index => {
                                setSelected(index)
                                if (index == 2) {
                                    setManual(true)
                                } else {
                                    setManual(false)
                                }
                                refRBSheet.current.close()
                            }}

                        >
                            <Radio style={styles.radioCard}>
                                <View style={{ flexDirection: 'column' }}>
                                    <Text style={{ fontFamily: 'Osans' }}>Zanifu Credit</Text>
                                    <Text style={{
                                        color: '#a09d9d',
                                        fontFamily: 'Osans'
                                    }}> Pay with Zanifu credit and get a loan to pay for your stock</Text>
                                </View>
                            </Radio>
                            <Radio style={styles.radioCard}>
                                <View style={{}}>
                                    <Text style={{ fontFamily: 'Osans' }}>Mpesa</Text>
                                    <Text style={{
                                        color: '#a09d9d',
                                        fontFamily: 'Osans'
                                    }}> Pay safely with Mpesa</Text>
                                </View>
                            </Radio>
                            <Radio style={styles.radioCard}>
                                <View style={{}}>
                                    <Text style={{ fontFamily: 'Osans' }}>Pay on Delivery</Text>
                                    <Text style={{
                                        color: '#a09d9d',
                                        fontFamily: 'Osans'
                                    }}> Pay when the stock is delivered</Text>
                                </View>
                            </Radio>
                        </RadioGroup>

                    </View>

                </RBSheet>


                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingTop: Constants.statusBarHeight * 1
                }}>
                    <View style={{
                        //backgroundColor: '#fff',
                        //borderRadius: 10,
                        marginLeft: 20,
                        //elevation: 2,
                        //padding: 10
                        width: 50,
                        height: 50,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Pressable onPress={() => navigation.navigate('Home')}>
                            <Ionicons name="arrow-back-sharp" color="black" size={33} />
                        </Pressable>
                    </View>


                </View>

                <ScrollView>

                    <View style={{ paddingLeft: width * 1 / 9, paddingTop: 20 }}>
                        <Text style={{
                            fontFamily: 'Osanssemi',
                            fontSize: 30
                        }}>Checkout</Text>
                    </View>
                    <View style={{ paddingLeft: width * 1 / 9, paddingTop: 10 }}>
                        <Text style={{
                            fontFamily: 'Osanssemi',
                            fontSize: 18,
                            color: 'grey'
                        }}>Your Items</Text>
                    </View>

                    <KeyboardAvoidingView
                        style={{
                            alignItems: 'center',
                            paddingTop: height * 1 / 33,
                            width: width,
                            paddingBottom: height * 1 / 6

                        }}
                    >
                        <FlatList
                            numColumns={1}
                            data={cartItems}
                            renderItem={
                                (item) => (
                                    <Pressable>
                                        <View style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'space-between',
                                            padding: 1,
                                            elevation: 0,
                                            borderRadius: 20,
                                            backgroundColor: '#fff',
                                            marginBottom: 10,
                                            borderWidth: 0,
                                            width: width * 1 / 1.2,
                                        }}>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View
                                                    style={{
                                                        width: 30,
                                                        height: 30,
                                                        alignItems: 'center',
                                                        justifyContent: 'center',
                                                        alignContent: 'center',
                                                        backgroundColor: '#f1f0f0',
                                                        borderRadius: 5
                                                    }}>
                                                    <Text style={{ fontFamily: 'Osanssemi' }}>{item.item.quantity}</Text>
                                                </View>
                                                <View style={{
                                                    paddingLeft: 20,
                                                    justifyContent: 'space-between'
                                                }}>
                                                    <Text adjustsFontSizeToFit numberOfLines={2} ellipsizeMode='tail' style={{ fontFamily: 'Osans', color: 'grey' }}>
                                                        {item.item.name}
                                                    </Text>
                                                </View>
                                            </View>
                                            <Text adjustsFontSizeToFit numberOfLines={2} ellipsizeMode='tail' style={{ fontFamily: 'Osanssemi', fontWeight: 'w900', color: 'black', fontSize: 20 }}>
                                                {`KES ${item.item.price}`}
                                            </Text>
                                        </View>
                                    </Pressable>
                                )
                            }
                        ></FlatList>

                        <View style={{ width: width * 1 / 1.2 }}>
                            <Chip mode='outlined' style={{ width: 130, backgroundColor: '#f3f3f3', borderWidth: 1, margin: 8, alignContent: 'center', alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}
                                onPress={() => {
                                    navigation.navigate('Home')
                                }}><Ionicons name="add-sharp" color="black" size={13} /><Text style={{ color: 'black', fontFamily: 'Osans' }}>Add Items</Text></Chip>
                        </View>
                        <View style={{
                            width: width,
                            paddingLeft: width * 1 / 9,
                            paddingTop: 20
                        }}>
                            <Text style={{ fontFamily: 'Osanssemi', color: '#474444', fontSize: 18 }}>How do you want to pay?</Text>
                        </View>

                        <Pressable onPress={() => refRBSheet.current.open()}>
                            <View style={{
                                width: width,
                                paddingLeft: width * 1 / 9,
                                paddingRight: width * 1 / 9,
                                paddingTop: 20,
                                height: 70
                            }}>
                                <View style={{ backgroundColor: '#f3f3f3', height: 50, alignContent: 'center', justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'Osans' }}>{options[selected]['name']}</Text>
                                </View>
                            </View>
                        </Pressable>

                        {
                            selected == 0 ?
                                <View>
                                    <View style={{ paddingBottom: 20, paddingTop: 20 }}>
                                        {
                                            loading ? <Spinner /> : <Text style={{ fontFamily: 'Osans' }}>
                                                {owner}
                                            </Text>
                                        }
                                    </View>

                                    <TextInput
                                        style={{ width: width * 1 / 1.2, height: 50 }}
                                        //textStyle={{ fontFamily: 'Osans' }}
                                        placeholder='Till number'
                                        value={value}
                                        keyboardType="number-pad"
                                        onChangeText={
                                            text => {
                                                setValue(text)
                                                log.debug(key)
                                                getTill(text)
                                            }
                                        }

                                    />

                                </View>
                                :
                                selected == 1 ? <View>
                                    <View style={{ paddingBottom: 20, paddingTop: 20 }}>
                                        {
                                            loading ? <Spinner /> : <Text style={{ fontFamily: 'Osans' }}>
                                                {owner}
                                            </Text>
                                        }
                                    </View>

                                    <TextInput
                                        style={{ width: width * 1 / 1.2, height: 50 }}
                                        //textStyle={{ fontFamily: 'Osans' }}
                                        placeholder='Till number'
                                        value={value}
                                        keyboardType="number-pad"
                                        onChangeText={
                                            text => {
                                                setValue(text)
                                                log.debug(key)
                                                getTill(text)
                                            }
                                        }

                                    />

                                </View> :
                                    selected == 2 ? <Text></Text> : <View />
                        }


                        <KeyboardAvoidingView style={{
                            width: width,
                            paddingLeft: width * 1 / 9,
                            paddingRight: width * 1 / 9,
                            paddingTop: 20,
                            height: 70
                        }}>

                        </KeyboardAvoidingView>
                    </KeyboardAvoidingView>

                    <BlurView
                        intensity={300}
                        style={{
                            position: 'relative',
                            height: 150,
                            width: width,
                            bottom: 150,
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'column'
                        }}>
                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            alignContent: 'space-between',
                        }}>
                            <View style={{ paddingRight: width * 1 / 2.7 }}>
                                <Text style={{ color: 'grey', fontFamily: 'Osansbold', fontSize: 16 }}>{`Total items ${cartItems.length}`}</Text>
                            </View>
                            <View>
                                <Text style={{ color: '#1e2c4b', fontFamily: 'Hindbold', fontSize: 20 }}>{`Ksh ${nihowmuch}`}</Text>
                            </View>
                        </View>
                        <View>
                            <Button
                                loading={isloading}
                                style={styles.primarybtn}
                                color='#3b3d3e'
                                uppercase={false}
                                labelStyle={{
                                    fontFamily: 'Osansbold'
                                }}
                                contentStyle={{
                                    height: 60
                                 }}
                                mode="contained"
                                onPress={
                                () => {
                                    //log.debug(cart)
                                    //log.debug(nihowmuch)
                                    //offset.value = 2
                                    zanifuPay()
                                }
                            }>Place Order</Button>
                        </View>

                    </BlurView>

                </ScrollView>





            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'Inter_900Black',
        flex: 1,
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#eff1f7',

    },
    primarybtn: {
        width: width * 1 / 1.2,
        height: 50,
        //alignItems: 'center',
        alignContent: 'center',
        justifyContent: 'center',
        marginTop: 20
    },

    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        fontFamily: 'Hindsemi'
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    radioCard: {
        //flexDirection: 'row-reverse',
        borderWidth: 1,
        padding: 20,
        borderColor: 'grey',
        borderRadius: 10,
        alignItems: 'flex-start',
        alignContent: 'flex-start',
        justifyContent: 'flex-start'

    }
})