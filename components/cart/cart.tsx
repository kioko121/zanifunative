import * as React from 'react'
import { ScrollView, View, StyleSheet, Button, Pressable, FlatList, Image, Vibration } from 'react-native';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, List, Layout, Text, Input, ViewPager, Tooltip } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import Constants from 'expo-constants';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import PhoneInput from 'react-native-phone-number-input';
//import { FirebaseRecaptchaVerifierModal } from 'expo-firebase-recaptcha';
import { Dimensions } from 'react-native';
import { logger } from "react-native-logs"
const { width, height } = Dimensions.get("window")
import { useDispatch, useSelector } from 'react-redux';
import { log } from 'react-native-reanimated';
import NumericInput from 'react-native-numeric-input';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { BlurView } from 'expo-blur';
import { Item } from 'react-native-paper/lib/typescript/components/List/List';
import Animated, { useSharedValue, useAnimatedStyle, withSpring } from 'react-native-reanimated';
import { fireman } from '../../api/base';
import { Spinner } from '@ui-kitten/components';


export default function Cart({ route, navigation }) {
    
    //const { data } = route.params;

    let [fontsLoaded] = useFonts({
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
        
    })

    const dispatch = useDispatch()

    
    const [cart, addCart] = useState([])
    const [cartItems, addCartItems] = useState([])
    const [nihowmuch, setNihowmuch] = useState(0)
    const offset = useSharedValue(0);

    const key = useSelector((state) => state.key)
    const phone = useSelector((state) => state.phone)
    const cart2 = useSelector((state) => state.cart2)
    const log = logger.createLogger()

    const customSpringStyles = useAnimatedStyle(() => {
        return {

            transform: [
                {
                    translateX: withSpring(offset.value * 255, {
                        damping: 20,
                        stiffness: 90,
                    }),
                },
            ],
        };
    });

    /*const getCartItemsold = async () => {

        //log.debug(cart2)
        try {
            var ninja = []
            var things = []
            var sum = 0
            addCart([])


            for (var i = 0; i < data.length; i++) {

                log.warn(data[i]["price"])
                var mig = data[i]["price"] * data[i]["quantity"]
                sum = sum + mig
            }
            
            setNihowmuch(sum)
            addCart(data)
            addCartItems(data)

            


        } catch (err) {
            log.error(err)
        }
    }
    */

    const getCartItems = async () => {
        try {
            var ninja = []
            var things = []
            var sum = 0
            var ref = fireman.database().ref(`/cartitems/${phone}`)
            addCart([])
            ref.once('value', (snapshot) => {
                snapshot.forEach(function (childSnapshot) {
                    var key = childSnapshot.key
                    var data = childSnapshot.val()

                    //log.debug(data)

                    var mig = data.price * data.quantity

                    sum = sum + mig

                    //setNihowmuch(kis)
                    // ninja.push(data.price)

                    things.push({id: data.id, name: data.name, price: data.price, quantity: data.quantity, distributor: data.distrtibutor, image: data.image })
                    //ninja.push({id: data.id, quantity: data.quantity})
                    addCart(oldArray => [...oldArray, { id: data.id, quantity: data.quantity }])
                })
                setNihowmuch(sum)
                //setCart(things)
                console.log("*******************************")
                //console.log(things)
                //console.log(cart)
                addCartItems(things)
                //addCart(ninja)
                log.debug(cartItems)

                // for (let i = 0; i < ninja.length; i++) {
                //     sum += ninja[i];
                // }

                //setNihowmuch(sum)
            })


        } catch (err) {
            log.error(err)
        }
    }

    useEffect(() => {


        getCartItems()

    }, [])


    if (!fontsLoaded) {
        return (<View style={styles.container}>
            <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingTop: height * 1 / 2 }}>
                <Spinner
                />
            </View>
        </View>
        )
    }
    else {
        return (
            <LinearGradient
                // Button Linear Gradient
                colors={['#fff', '#fff', '#fff']}
                style={styles.container}
            >
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingTop: Constants.statusBarHeight * 1
                }}>
                    <View style={{

                        marginLeft: 20,

                        //padding: 10
                        //width: 50,
                        height: 50,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={{ fontFamily: 'Osans', fontSize: 26 }}>Your Cart</Text>
                    </View>


                    <View style={{
                        //backgroundColor: '#fff',
                        borderRadius: 10,
                        marginRight: 20,
                        //elevation: 2,
                        width: 50,
                        height: 50,
                        justifyContent: 'center',
                        alignItems: 'center'
                        //padding: 10
                    }}>
                        <Pressable onPress={() => {
                            var ref = fireman.database().ref(`/cartitems/${phone}`)
                            ref.remove()
                            navigation.navigate('Home')
                        }}>
                            <Ionicons name="close" color="black" size={23} />
                        </Pressable>
                    </View>

                </View>

                <View
                    style={{
                        alignItems: 'center',
                        paddingTop: height * 1 / 20,
                        width: width,
                        paddingBottom: height * 1 / 3

                    }}
                >
                    <FlatList
                        numColumns={1}
                        data={cartItems}
                        renderItem={
                            (item) => (
                                <Pressable>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'flex-start',
                                        padding: 8,
                                        elevation: 0,
                                        //borderRadius: 10,
                                        backgroundColor: '#fff',
                                        marginBottom: 10,
                                        //borderWidth: 0.6
                                    }}>
                                        <View style={{ flexDirection: 'column'}}> 

                                        <View style={{
                                            width: width * 1 / 1.2,

                                            flexDirection: 'row',
                                            justifyContent: 'space-between'
                                        }}>
                                            <Text adjustsFontSizeToFit numberOfLines={2} ellipsizeMode='tail' style={{ fontFamily: 'Osans', color: 'black' }}>
                                                {item.item.name}
                                            </Text>

                                            <Text adjustsFontSizeToFit numberOfLines={2} ellipsizeMode='tail' style={{ fontFamily: 'Osanssemi', color: 'black', fontSize: 20 }}>
                                                {`KES ${item.item.price}`}
                                            </Text>
                                        </View>

                                        <NumericInput
                                            rounded
                                            totalWidth={80}
                                            value={item.item.quantity}
                                            onChange={
                                                value => {

                                                    var id = item.item.id
                                                    var qty = value
                                                    //var jsa = { 'id': id, 'quantity': qty }

                                                    var itemname = item.item.title.replace(/[^a-zA-Z ]/g, "");

                                                    /*var ref = fireman.database().ref(`carts/${key}`)
                                                    ref.child(itemname).set({
                                                        //'name': item.name,
                                                        'id': id,
                                                        'quantity': qty,
                                                    })
                                                    */


                                                    var ref = fireman.database().ref(`cartitems/${phone}`)
                                                    ref.child(itemname).set({
                                                        //'name': item.name,
                                                        'id': id,
                                                        'name': item.item.name,
                                                        'image': item.item.image,
                                                        'price': item.item.price,
                                                        'distrtibutor': item.item.distributor.distributor_name,
                                                        'quantity': qty,
                                                    })


                                                    Vibration.vibrate(1)
                                                    //color.value= 'white'
                                                    // setColor('white')

                                                }
                                            } />
                                            </View>

                                    </View>
                                    <View style={{ height: 1, backgroundColor: 'grey' }} />
                                </Pressable>
                            )
                        }
                    ></FlatList>
                </View>

                <BlurView
                    intensity={300}
                    style={{
                        position: 'absolute',
                        height: 150,
                        width: width,
                        bottom: 0,
                        justifyContent: 'center',
                        alignItems: 'center',
                        flexDirection: 'column'
                    }}>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        alignContent: 'space-between',
                        width: width * 1/1.2,
                        paddingBottom: 20
                    }}>
                        <View >
                            <Text style={{ color: 'black', fontFamily: 'Osans', fontSize: 24 , fontWeight: '600'}}>{`Subtotal ${cartItems.length}`}</Text>
                        </View>
                        <View>
                            <Text style={{ color: '#1e2c4b', fontFamily: 'Osans', fontWeight: '600' ,fontSize: 24 }}>{`Ksh ${nihowmuch}`}</Text>
                        </View>
                    </View>
                    <Animated.View style={customSpringStyles}>
                        <Pressable
                            onPress={() => {
                                log.debug(cart)
                                log.debug(nihowmuch)
                                offset.value = 2
                                navigation.navigate('Checkout')

                            }}>

                            <LinearGradient
                                // Button Linear Gradient
                                colors={['#3b3d3e', '#3b3d3e', '#3b3d3e']}
                                style={{
                                    height: 50,
                                    width: width * 1 / 1.2,
                                    borderRadius: 6,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    flexDirection: 'row',
                                    //elevation: 4
                                }}
                            >
                                <Text style={{ fontFamily: 'Osanssemi', color: '#fff' }}>Go to checkout</Text>
                                
                            </LinearGradient>
                        </Pressable>
                    </Animated.View>

                </BlurView>

            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'Inter_900Black',
        flex: 1,
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#eff1f7',

    }
})