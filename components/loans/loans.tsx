import * as React from 'react'
import { ImageBackground, FlatList, View, StyleSheet, ActivityIndicator, KeyboardAvoidingView, ScrollView, Image, TouchableNativeFeedback } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, Layout, Text, Input, Icon, Select, SelectItem, Card } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import { fireman } from '../../api/base';
import { logger } from "react-native-logs"
import { ToastAndroid } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import { Divider } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';
import { Dimensions } from 'react-native';
import { Spinner } from '@ui-kitten/components';
import { useSelector } from 'react-redux';
import { Button } from 'react-native-paper'
import { payloantxt } from '../../globals'

const { width, height } = Dimensions.get("window")
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";
export default function Loans({ navigation }) {

  let [fontsLoaded] = useFonts({
    'Hindbold': require('../../assets/HindSiliguri-Bold.ttf'),
    'Hindregular': require('../../assets/HindSiliguri-Regular.ttf'),
    'Hindmedium': require('../../assets/HindSiliguri-Medium.ttf'),
    'Hindsemi': require('../../assets/HindSiliguri-SemiBold.ttf'),
  })

  const [loans, setLoans] = useState([])
  const [total, setTotal] = useState()

  const [data, setData] = useState(false)
  const key = useSelector((state) => state.key)
  const log = logger.createLogger()
  const noob = useSelector((state) => state.newbie)

  const goToApply = () => {
    navigation.navigate('Business')
  }

  const getLoansold = () => {
    console.log('getting loans___________________________')
    try {
      //var simu = fireman.auth().currentUser?.phoneNumber
      // console.log(simu)

      var ref = fireman.database().ref('loans')
      var query = ref.orderByChild('against').equalTo('+254700089885')
      query.once('value', function(snapshot) {
        setData(true)
        var vitu = []
        var nun = 0
        snapshot.forEach(function(childSnapshot) {
          var key = childSnapshot.key
          var data = childSnapshot.val()

          if (data.status == "UNPAID") {
            nun = nun + data.amount
            console.log("kuna unpaid hapa")
          }

          vitu.push({ key: key, against: data.against, amount: data.amount, date: data.date, paidto: data.paidto, status: data.status })

          //console.log(data)
        })

        console.log(` nun is ${nun}`)
        //setTotal(nun)
        setLoans(vitu)
        global.totalLoan = nun
      })

    } catch (error) {
      console.log(error)
    }
  }

  const getLoans = () => {
    try {
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Accept", "application/json");
      myHeaders.append("Authorization", `Bearer ${key}`);

      var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
      };

      fetch("https://development.zanifu.com/api/loans", requestOptions)
        .then(response => {
          var status = response.status
          if (status == 200) {
            response.text().then((data) => {
              var things = JSON.parse(data);

              var res = things.data.map(bill => bill.amount).reduce((acc, bill) => bill + acc);
              setTotal(res)

              log.debug(things.data)
              setLoans(things.data)
              setData(true)
            })


          }
        })
        .catch(error => console.log('error', error));
    }
    catch (error) {
      log.error(error)
    }
  }

  useEffect(() => {

    getLoans()

  }, [navigation])

  return (
    <>
      {data ?
        <View style={styles.container}>
          <View style={{
            paddingTop: Constants.statusBarHeight * 2
          }}>
            <Text style={{ fontFamily: 'Open Sans Bold', fontSize: 27, color: 'black' }}> Loans</Text>
          </View>

          <View style={{ alignItems: 'center' }}>
            <View style={{
              marginTop: 40,
              height: 180,
              width: width * 1 / 1.1,
              borderWidth: 1,
              borderColor: 'gray',
              borderRadius: 6,
              padding: 10
            }}>
              <Text style={{ color: 'grey', fontFamily: 'Open Sans Regular' }}>Total loan amount</Text>

              <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 20 }}>
                <Text style={{ fontFamily: 'Open Sans SemiBold', color: '#4d4d4d', fontSize: 23 }}>{`KES ${total}`}</Text>
                <Button
                  color="black"
                  uppercase={false}
                  labelStyle={{ fontFamily: 'Open Sans SemiBold' }}
                  style={{ backgroundColor: '#f1f1f1', width: 140 }}
                >Repay Loan</Button>
              </View>

              <View style={{ paddingTop: 20 }}>
                <Text style={{ fontFamily: 'Open Sans Regular', color: 'gray' }}>{payloantxt}</Text>
              </View>


            </View>

            <View style={{
              paddingTop: 20,
              paddingLeft: 20,
              paddingRight: 20,
              width: width,
              alignItems: 'flex-start'
            }}>
              <Text style={{ fontFamily: 'Open Sans SemiBold', color: '#4d4d4d', fontSize: 20 }}>YOUR LOANS</Text>
            </View>

            <View style={{ width: width, paddingLeft: 20, paddingRight: 20, paddingTop: 20 }}>
              <FlatList
                data={loans}
                renderItem={
                  (item) => (
                    <View>
                      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ fontFamily: 'Open Sans Regular', fontSize: 18 }}>{item.item.amount}</Text>
                        <Text style={{ fontFamily: 'Open Sans Regular', fontSize: 18 }}>{item.item.status}</Text>
                      </View>
                      <View style={{ paddingTop: 10}}>
                      <Text style={{ fontFamily: 'Open Sans Regular', color: 'gray', fontSize: 14}}>{`Paid to ${item.item.distributor}`}</Text>
                      </View>
                      <View>
                      <Text style={{ fontFamily: 'Open Sans Regular', color: 'gray', fontSize: 14}}>{`Balance ${item.item.balance}`}</Text>
                      </View>
                      <View style={{ height: 1, backgroundColor: 'gray', marginBottom: 20, marginTop: 10}}/>
                    </View>
                  )
                }
              />
            </View>
          </View>
        </View> :
        <View style={{ justifyContent: 'center', alignItems: 'center', alignContent: 'center', flex: 1, backgroundColor: "#fff" }}>
          <View style={{ backgroundColor: "#c3c3c3", height: 100, justifyContent: 'center', borderRadius: 50 }}>
            <Image height={30} width={30} resizeMode='contain' source={require('../../assets/cih.png')} /></View>

          <View style={{ paddingTop: 20 }}>
            <Text style={{ fontFamily: 'Hindsemi', color: 'grey', fontSize: 18 }}>There are no loans at the moment</Text>
          </View>

          <View style={{ width: 240, paddingTop: 20 }}>
            <Text style={{ fontFamily: 'Hindregular', fontSize: 14, color: 'grey' }}>Sign up for Zanifu credit to unlock loans to pay for your stock</Text>
          </View>

          <View style={{ paddingTop: 30 }}>
            {
              noob ? <Button
                style={{
                  backgroundColor: '#767272',
                  borderColor: '#767272'
                }}
                color="white"
                uppercase={false}
                onPress={goToApply}>

                Apply for Zanifu Credit

              </Button> : <View />
            }
          </View>

        </View>}
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    fontFamily: 'Open Sans Regular',
    flex: 1,
    backgroundColor: '#fff',
    padding: 8,
    paddingLeft: 20,
    paddingRight: 20
  }
})
