import * as React from 'react'
import { Alert, ImageBackground, View, StyleSheet, ActivityIndicator, KeyboardAvoidingView, ScrollView, Image, TouchableNativeFeedback, Pressable } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, Button, Layout, Text, Input, Icon, Select, SelectItem, Card } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import PinInput from 'react-pin-input';
import firebase from 'firebase';
import { fireman } from '../../api/base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { ToastAndroid, Modal } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { color } from 'react-native-reanimated';
import { Divider } from 'react-native-paper';
import { TextInput } from 'react-native-paper';

export default function Payloan({ navigation }) {

    const [amount, setAmount] = useState()
    const [modalVisible, setModalVisible] = useState(false)

    let [fontsLoaded] = useFonts({
        'Hindbold': require('../../assets/HindSiliguri-Bold.ttf'),
        'Hindregular': require('../../assets/HindSiliguri-Regular.ttf'),
        'Hindmedium': require('../../assets/HindSiliguri-Medium.ttf'),
        'Hindsemi': require('../../assets/HindSiliguri-SemiBold.ttf'),
    })

    if (!fontsLoaded) {
        return (<View style={styles.container}>
            <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                <LottieView
                    autoPlay
                    style={{ width: 800, height: 800 }}
                    source={require('../../assets/9305-loading-bloob.json')}
                />
            </View>
        </View>
        )
    }
    else {
        return (

                <ScrollView style={styles.container}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => {
                            Alert.alert("Modal has been closed.");
                            setModalVisible(!modalVisible);
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <LottieView
                                    autoPlay
                                    style={{ width: 120, height: 120 }}
                                    source={require('../../assets/9305-loading-bloob.json')}
                                />
                                <Text style={{ fontFamily: 'Hindbold', fontSize: 20 }}>Connecting to Mpesa</Text>
                                <Pressable
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => setModalVisible(!modalVisible)}
                                >
                                    <View style={{ paddingTop: 20}}>
                                        <Text style={{ fontFamily: 'Hindregular', color: 'grey'}}>This may take a few seconds...</Text>
                                    </View>
                                    <View style={{ paddingTop: 20}}>
                                        <Text style={{ fontFamily: 'Hindregular', color: 'grey'}}>
                                            The MPESA menu will appear shortly. Enter your PIN, ensure your balance is enough.
                                            </Text>
                                    </View>
                                </Pressable>
                            </View>
                        </View>
                    </Modal>
                    <View style={{ paddingTop: 60 }}>
                        <TouchableNativeFeedback>
                            <MaterialCommunityIcons name="arrow-left" color={'black'} size={40} />
                        </TouchableNativeFeedback>
                    </View>
                    <View style={{ paddingTop: 20 }}>
                        <Text style={{ fontFamily: 'Hindbold', fontSize: 30 }}>Repay Loan</Text>
                    </View>

                    <View style={{ paddingTop: 40 }}>
                        <View>
                            <Text style={{ fontFamily: 'Hindregular', fontSize: 18, color: 'grey' }}>
                                Enter Payment Amount
                            </Text>
                        </View>
                        <TextInput
                            style={{ backgroundColor: 'white', fontFamily: 'Hindbold' }}
                            mode="outlined"
                            label="Repayment amount"
                            value={amount}
                            onChangeText={text => setAmount(text)}
                            //onTextInput
                            placeholder="Amount to pay"
                            left={<TextInput.Affix text="KES" />}
                        />

                        <View>
                            <Button style={{ backgroundColor: '#3b3d3e', borderColor: '#3b3d3e', marginTop: 120 }}
                                onPress={() => setModalVisible(true)}
                            >
                                <Text style={{ fontFamily: 'Hindbold', color: 'white' }}>Repay Loan</Text>
                            </Button>
                        </View>
                    </View>
                </ScrollView>


        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 8,
        paddingLeft: 20,
        paddingRight: 20
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        height: 400,
        width: 300,
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },

})