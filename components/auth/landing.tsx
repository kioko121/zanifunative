import React, { useState, useEffect } from 'react';
import { TouchableWithoutFeedback, StyleSheet, View, Image, Dimensions, ToastAndroid } from 'react-native';
import { Appearance } from 'react-native';
import { ApplicationProvider, Button, Layout, Text, Input, Icon } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import { fireman } from '../../api/base';
import { useDispatch, useSelector } from 'react-redux';
import { Spinner } from '@ui-kitten/components';

export default function Landing({ navigation }) {
    
    const key = useSelector((state) => state.key)

    const checkUser  = async () => {

          if (key) {
            // User is signed in.
            navigation.navigate('Home')
          } else {
            // No user is signed in.
              navigation.navigate('Login')
          }
        
      }

    useEffect(() => {
        checkUser()
    },[])
    return (
        <ApplicationProvider {...eva} theme={eva.light}>
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <Spinner/>

                </View>
            </View>
        </ApplicationProvider>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
