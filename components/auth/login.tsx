import * as React from 'react'
import {
    ImageBackground,
    View,
    StyleSheet,
    Image,
    TouchableNativeFeedback,
    StatusBar,
    BackHandler
} from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect } from 'react'
import { ApplicationProvider, Button, Layout, Text, Input, Icon, Select, SelectItem, Card } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import { Dimensions } from 'react-native';
import { ToastAndroid } from 'react-native';
import { Spinner } from '@ui-kitten/components';

const { width, height } = Dimensions.get("window")

export default function Login({ navigation: { push } }) {
    let [fontsLoaded] = useFonts({
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })

    const zep = true

    const pressed = () => {
        push('Signup')
    }

    function handleBackButtonClick() {
        //push('Login');
        return true;
    }

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    })


    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <Spinner
                    />
                </View>

            </View>
        )
    } else {
        return (

            <View style={styles.container}>
                <StatusBar translucent backgroundColor='transparent' barStyle='light-content' />
                <ImageBackground resizeMode='stretch' source={require('../../assets/backog.png')} style={styles.image}>

                    <View style={styles.container2}>
                        <Image resizeMode='center' source={require('../../assets/logo.png')} style={{ width: width * 1 / 3, height: height * 1 / 2 }}></Image>
                        <View style={{ padding: 35 , paddingBottom: 100, width: width * 1/1.2}}>
                            <Text style={{ fontFamily: 'Osansbold', textAlign: 'center', color: '#fff', fontSize: 20 }}>Order and pay for your stock with zanifu Loan, mpesa or cash delivery</Text>
                        </View>
                    </View>
                    <View style={{ paddingBottom: 20 , position:'absolute', bottom: 90}}>
                        <Button
                            //status='basic'
                            //appearance='filled'
                            style={{
                                width: 300,
                                backgroundColor: '#343434',
                                borderRadius: 10,
                                paddingBottom: 20,
                                paddingTop: 20
                            }}
                            onPress={
                                () => {
                                    push('Signup')
                                    /*ToastAndroid.showWithGravityAndOffset(
                                        "route under construction ",
                                        ToastAndroid.SHORT,
                                        ToastAndroid.BOTTOM,
                                        25,
                                        50
                                    )*/
                                }
                            }
                        ><Text style={{ fontFamily: 'Hindbold', fontSize: 20 }}>Sign Up</Text></Button>
                    </View>
                    <View style={{ flexDirection: 'row', position: 'absolute', bottom: 40 }}>
                        <Text style={{ fontFamily: 'Osanssemi' , color: 'grey'}}>Have an account ? </Text>
                        <TouchableNativeFeedback onPress={() => push('Signin')}>
                            <Text style={{ fontFamily: 'Osanssemi', color: '#343434', fontSize: 16 }}>Sign In </Text>
                        </TouchableNativeFeedback>
                    </View>
                </ImageBackground>

            </View>

        )
    }
}


const styles = StyleSheet.create({
    container: {
        fontFamily: 'Inter_900Black',
        flex: 1,
        justifyContent: 'center',
        //paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        padding: 0,
    },

    container2: {
        fontFamily: 'Inter_900Black',
        //flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        bottom: 180

    },
    select: {

    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        flex: 1,
        height: height * 1/1.3,
        justifyContent: "center",
        alignItems: "center",
    },

});