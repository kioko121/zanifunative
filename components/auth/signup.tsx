import * as React from 'react'
import { View, StyleSheet, KeyboardAvoidingView, Pressable, ToastAndroid, Image, Keyboard, BackHandler } from 'react-native';
import { useState, useEffect, useRef } from 'react'
import { Text, Input, ViewPager, Card, SelectItem } from '@ui-kitten/components';
import { useFonts } from '@expo-google-fonts/inter';
import LottieView from 'lottie-react-native'

import { MaterialCommunityIcons } from '@expo/vector-icons';
import Constants from 'expo-constants';
import PhoneInput from 'react-native-phone-number-input';
import { Spinner, Select } from '@ui-kitten/components';
import { Dimensions } from 'react-native';
import { logger } from "react-native-logs"
const { width, height } = Dimensions.get("window")
import * as ImagePicker from 'expo-image-picker';
import Ionicons from '@expo/vector-icons/build/Ionicons';
import { TextInput, Button } from 'react-native-paper'
import { useDispatch, useSelector } from 'react-redux';

export default function Signup({ navigation }) {

    let [fontsLoaded] = useFonts({
        'Hindbold': require('../../assets/HindSiliguri-Bold.ttf'),
        'Hindregular': require('../../assets/HindSiliguri-Regular.ttf'),
        'Hindmedium': require('../../assets/HindSiliguri-Medium.ttf'),
        'Hindsemi': require('../../assets/HindSiliguri-SemiBold.ttf'),
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })

    const phoneInput = useRef<PhoneInput>(null);
    const [phone, setPhone] = useState('')
    const [pin, setPin] = useState('')
    const log = logger.createLogger()
    const [categories, setCategories] = useState([])
    const [selectedIndex, setSelectedIndex] = useState(0)
    const [name, setName] = useState('')
    const [business, setBusiness] = useState('')
    const [email, setEmail] = useState('')
    const [location, setLocation] = useState('')
    const [valid, setValidemail] = useState(true)
    const [selectedcate, setSelectedCate] = useState()
    const [isloading, setIsloading] = useState(false)

    const [checkphone, setCheckphone] = useState(0)
    const [checkpin, setCheckpin] = useState(false)
    const [checkname, setCheckname] = useState(0)
    const [checkbusiness, setCheckbusiness] = useState(0)
    const [checkemail, setCheckemail] = useState(0)
    const [checklocation, setChecklocation] = useState(0)
    const [idimage, setIdimage] = useState(null);
    const [idimageback, setIdimageback] = useState(null);
    const [permit, setPermit] = useState(null)
    const [userid, setUserid] = useState(null)
    const [tempauth, setTempauth] = useState(null)
    const [btn2, setBtn2] = useState(true)
    const [btn3, setBtn3] = useState(true)
    const [catestatus, setCatestatus] = useState('basic')
    const dispatch = useDispatch()

    function handleBackButtonClick() {
        navigation.navigate('Login');
        return true;
    }

    const uploadimages = () => {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "multipart/form-data");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${tempauth}`);

        var formdata = new FormData();
        formdata.append("front", idimage);
        formdata.append("back", idimageback);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: formdata,
            redirect: 'follow'
        };

        fetch(`https://development.zanifu.com/api/merchants/${userid}/national-id`, requestOptions)
            .then(response => {
                var status = response.status
                log.debug(status)

                if (status == 200) {
                    setSelectedIndex(3)
                }
                response.text().then((data) => {
                    var vitu = JSON.parse(data)

                    log.debug(vitu)
                })
            })

            .catch(error => {
                log.debug(`Bearer ${tempauth}`)
                log.debug(`https://development.zanifu.com/api/merchants/${userid}/national-ids`)
                log.error('error', error)

                ToastAndroid.showWithGravityAndOffset(
                    'Something went wrong, please try again later',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            });



    }

    const uploadPermit = () => {
        setBtn3(false)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "multipart/form-data");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${tempauth}`);

        var formdata = new FormData();
        formdata.append("business_permit", permit);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: formdata,
            redirect: 'follow'
        };

        fetch(`https://development.zanifu.com/api/merchants/${userid}/business-permit`, requestOptions)
            .then(response => {
                var status = response.status
                log.debug(status)

                if (status == 200) {
                    setSelectedIndex(4)
                }
                response.text().then((data) => {
                    var vitu = JSON.parse(data)

                    log.debug(vitu)
                })
            })
            .catch(error => {
                log.debug(`Bearer ${tempauth}`)
                log.debug(`https://development.zanifu.com/api/merchants/${userid}/business-permit`)
                log.error('error', error)

                ToastAndroid.showWithGravityAndOffset(
                    'Something went wrong, please try again later',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            });
    }


    const pickIdImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        console.log(result);

        if (!result.cancelled) {
            setIdimage(result.uri);

            if (idimageback == null) {
                ToastAndroid.showWithGravityAndOffset(
                    'Please fill back',
                    ToastAndroid.SHORT,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            }

            else {
                uploadimages()
            }
        }
    };
    const pickIdImageback = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        console.log(result);

        if (!result.cancelled) {
            setIdimageback(result.uri);

            if (idimage == null) {
                ToastAndroid.showWithGravityAndOffset(
                    'Please fill front',
                    ToastAndroid.SHORT,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            }

            else {
                uploadimages()
            }

        };
    }
    const pickPermit = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        console.log(result);

        if (!result.cancelled) {
            setPermit(result.uri);

            uploadPermit()
        }
    };

    const validate = (email) => {
        const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

        log.debug(expression.test(String(email).toLowerCase()))
        var uchafu = expression.test(String(email).toLowerCase())
        if (uchafu) {
            setCheckemail(0)
        } else {
            setCheckemail(1)
        }
        return expression.test(String(email).toLowerCase())
    }

    const getStarted = async () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://development.zanifu.com/api/categories", requestOptions)
            .then(response => response.text())
            .then(result => setCategories(JSON.parse(result)))
            .catch(error => console.log('error', error));
    }

    const updateMerch = (id, token) => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${token}`);

        var raw = JSON.stringify({
            "merchant_name": name,
            "business_name": business,
            "email": email,
            "location": location
        });

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`https://development.zanifu.com/api/merchants${id}`, requestOptions)
            .then(response => {
                var status = response.status
                log.debug(status)

                response.text().then((data) => {
                    var vitu = JSON.parse(data)

                    log.debug(vitu)
                })

                if (status == 200) {
                    setSelectedIndex(2)
                }
            })
            .catch(error => console.log('error', error));
    }


    const Sendit = () => {



        var myHeaders = new Headers(); 
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Accept", "application/json");

        var raw = JSON.stringify({
            "phone_number": phone,
            "pin": pin,
            "category_id": selectedcate
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://development.zanifu.com/api/merchants", requestOptions)
            .then(response => {
                var status = response.status

                log.debug(status)
                if (status == 422) {
                    ToastAndroid.showWithGravityAndOffset(
                        'User already signed up',
                        ToastAndroid.SHORT,
                        ToastAndroid.BOTTOM,
                        25,
                        50
                    )
                    navigation.navigate('Login')
                    setIsloading(false)
                }

                if (status == 201) {
                    response.text().then((data) => {

                        var vitu = JSON.parse(data)

                        log.debug(vitu.data.id)

                        setUserid(vitu.data.id)
                        setTempauth(vitu.data.token)

                        //updateMerch(vitu.data.id, vitu.data.token)

                        dispatch({
                            type: "ADD_KEY",
                            payload: vitu.data.token
                        })

                        dispatch({
                            type: "ADD_PHONE",
                            payload: phone
                        })

                        dispatch({
                            type: "ADD_NEWBIE",
                            payload: true
                        })

                        dispatch({
                            type: "ADD_ID",
                            payload: vitu.data.id
                        })

                        setIsloading(false)
                        navigation.navigate('Home')


                    })
                }
            })
            .catch(error => console.log('error', error));


    }

    useEffect(() => {
        getStarted()
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, [])

    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <Spinner
                    />
                </View>

            </View>
        )
    } else {

        return (

            <ViewPager
                style={styles.container}
                selectedIndex={selectedIndex}
                swipeEnabled={false}
                scrollEnabled={false}
                onSelect={index => setSelectedIndex(index)}>

                {/*//page 1 */}
                <View style={styles.container} >
                    <View
                        style={{
                            paddingTop: Constants.statusBarHeight * 2,
                            alignItems: 'flex-start',


                        }}>

                    </View>
                    <View style={{
                        alignContent: 'flex-start',
                        width: width * 1 / 1.2
                    }}>
                        <Pressable onPress={() => navigation.goBack()}>
                            <Ionicons name="arrow-back-sharp" color="black" size={33} />
                        </Pressable>
                    </View>

                    <View style={{ paddingTop: 20, width: width * 1 / 1.2, alignItems: 'flex-start' }}>
                        <Text style={{ fontFamily: 'Osanssemi', fontSize: 23 }}>Enter your phone number</Text>
                    </View>

                    <KeyboardAvoidingView style={{ alignItems: 'center', paddingTop: 40 }}>
                        <PhoneInput
                            textInputStyle={{ fontFamily: 'Osans', justifyContent: 'center', alignContent: 'center', height: 50 }}
                            codeTextStyle={{ fontFamily: 'Osans' }}

                            textContainerStyle={{ height: 50, backgroundColor: '#fff' }}
                            containerStyle={{ backgroundColor: '#fff', borderWidth: 1, borderColor: 'grey', width: width * 1 / 1.16, borderRadius: 8, paddingRight: 20, height: 60 }}
                            ref={phoneInput}
                            //defaultValue={value}
                            defaultCode="KE"
                            layout="first"
                            onChangeText={(text) => {
                                //setValue(text);
                                //setPhone(text)
                            }}
                            onChangeFormattedText={(text) => {
                                //setFormattedValue(text);
                                console.log(text)
                                setPhone(text)
                            }}
                        //withDarkTheme
                        //withShadow
                        //autoFocus
                        />

                        <View style={{ paddingTop: height * 1 / 15 }}>
                            <Button
                                style={{
                                    width: width * 1 / 1.2,
                                    //padding: 8,
                                    marginTop: 20
                                }}
                                color='#3b3d3e'
                                uppercase={false}
                                contentStyle={{
                                    height: 60
                                }}
                                mode="contained"
                                labelStyle={{
                                    fontFamily: 'Osanssemi',
                                    color: 'white'
                                }}
                                onPress={
                                    () => {
                                        if (phone == '') {
                                            ToastAndroid.showWithGravityAndOffset(
                                                'Please fill phone',
                                                ToastAndroid.LONG,
                                                ToastAndroid.BOTTOM,
                                                25,
                                                50
                                            )
                                        }
                                        else {
                                            setSelectedIndex(1)
                                        }
                                    }
                                }>Continue</Button>
                        </View>
                    </KeyboardAvoidingView>
                </View>

                {/*page two two*/}
                <View style={styles.container}>
                    <View
                        style={{
                            paddingTop: Constants.statusBarHeight * 2
                        }}>

                        <View style={{
                            alignContent: 'flex-start',
                            width: width * 1 / 1.2
                        }}>
                            <Pressable onPress={() => navigation.goBack()}>
                                <Ionicons name="arrow-back-sharp" color="black" size={33} />
                            </Pressable>
                        </View>

                        <View style={{ paddingTop: 10 }}>
                            <Text style={{ fontFamily: 'Osanssemi', fontSize: 23 }}>Verify your phone</Text>
                        </View>

                        <View style={{ paddingTop: 20 }}>
                            <Text style={{ fontFamily: 'Osans', color: 'grey', fontSize: 19 }}>Create pin to use</Text>
                        </View>

                        < View style={{ paddingTop: 20 }}>
                            {
                                checkpin ? <TextInput
                                    placeholder='Zanifu Pin'
                                    mode='outlined'
                                    error
                                    keyboardType="number-pad"

                                    maxLength={4}

                                    onChangeText={
                                        text => {
                                            setPin(text)
                                            console.log(text)
                                        }
                                    }
                                    style={{
                                        backgroundColor: '#fff',
                                        borderRadius: 10,
                                        width: width * 1 / 1.16,
                                        fontFamily: 'Osans'
                                    }}
                                /> : <TextInput
                                    placeholder='Zanifu Pin'
                                    mode='outlined'


                                    keyboardType="number-pad"

                                    maxLength={4}

                                    onChangeText={
                                        text => {
                                            setPin(text)
                                            console.log(text)
                                        }
                                    }
                                    style={{
                                        backgroundColor: '#fff',
                                        borderRadius: 10,
                                        width: width * 1 / 1.16,
                                        fontFamily: 'Osans'
                                    }}
                                />
                            }
                        </View>

                        <View style={{ paddingTop: height * 1 / 15 }}>
                            <Button
                                style={{
                                    width: width * 1 / 1.2,
                                    marginTop: 20

                                }}
                                color='#3b3d3e'
                                uppercase={false}
                                contentStyle={{
                                    height: 60
                                 }}
                                mode="contained"
                                onPress={
                                () => {
                                    if (pin == '') {
                                        setCheckpin(true)
                                    }
                                    else {
                                        setSelectedIndex(2)
                                    }
                                }
                            }>Continue </Button>
                        </View>

                    </View>
                </View>

                {/*page 2 of two of two */}
                <KeyboardAvoidingView style={styles.container}>
                    <View
                        style={{
                            paddingTop: Constants.statusBarHeight * 2,
                            width: width * 1 / 1.2
                        }}>

                        <View style={{
                            alignContent: 'flex-start',
                            width: width * 1 / 1.2
                        }}>
                            <Pressable onPress={() => navigation.goBack()}>
                                <Ionicons name="arrow-back-sharp" color="black" size={33} />
                            </Pressable>
                        </View>

                        <View style={{ paddingTop: 10 }}>
                            <Text style={{ fontFamily: 'Osanssemi', fontSize: 23 }}>Select a category that fits your business best</Text>
                        </View>

                        <View style={{ paddingTop: 20 }}>
                            <Text style={{ fontFamily: 'Osans', color: 'grey', fontSize: 16 }}>This helps us to display products relevant to your business</Text>
                        </View>

                        <View style={{ paddingTop: 40 }}>
                            <Select
                                size='large'
                                showSoftInputOnFocus={false}
                                style={{ backgroundColor: 'white', fontFamily: 'Osans' }}
                                status={catestatus}
                                onFocus={()=> Keyboard.dismiss()}
                                selectedIndex={selectedcate}
                                onSelect={index => {
                                    setSelectedCate(index.row)
                                }}>
                                <SelectItem title='Duka' />
                                <SelectItem title='Bar' />
                                <SelectItem title='Wines & spirits' />
                                <SelectItem title='Lpg gas' />
                            </Select>
                        </View>

                        <View style={{ paddingTop: height * 1 / 15 }}>
                            <Button
                                style=
                                {{
                                    width: width * 1 / 1.2,
                                    //padding: 8,
                                    marginTop: 20
                                }}
                                loading={isloading}
                                color='#3b3d3e'
                                uppercase={false}  
                                contentStyle={{
                                    height: 60
                                 }}
                                mode="contained"
                                onPress={
                                    () => {
                                    setIsloading(true)
                                    if (selectedcate == null) {
                                        console.log('zero')
                                        setCatestatus('danger')
                                    }
                                    else {

                                        //setSelectedIndex(3)
                                        //console.log(selectedcate)
                                        Sendit()
                                        //setIsloading(false)
                                    }
                                }
                            }><Text style={{ fontFamily: 'Osanssemi', color: 'white' }}>Continue</Text></Button>
                        </View>
                    </View>
                </KeyboardAvoidingView>







                {/* oldddddd*/}

                {/*//page 2 */}
                <View style={styles.container}>
                    <View style={{ paddingTop: 60, justifyContent: 'flex-start', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                        <Pressable onPress={() => navigation.goBack()}>
                            <MaterialCommunityIcons name="arrow-left" size={33} />
                        </Pressable>
                    </View>
                    <View style={{ paddingTop: 20 }}>
                        <Text style={{ fontFamily: 'Hindbold', color: 'black', fontSize: 20 }}>Fill in your details</Text>
                    </View>

                    < View style={{ paddingTop: 20 }}>
                        <Input
                            placeholder='Your name'
                            textStyle={{
                                fontFamily: 'Hindregular'
                            }}
                            //mode="outlined"
                            value={name}
                            size="large"
                            onChangeText={
                                text => {
                                    setName(text)
                                    console.log(text)
                                }
                            }
                            style={{
                                borderWidth: checkname,
                                backgroundColor: '#eef2f5',
                                borderColor: 'red',
                                borderRadius: 20,
                                width: width * 1 / 1.16,

                            }}
                        />
                    </View>

                    <View style={{ marginTop: 10 }}>
                        <Input
                            placeholder='Name of business'
                            textStyle={{
                                fontFamily: 'Hindregular'
                            }}
                            //mode="outlined"
                            value={business}
                            size="large"
                            onChangeText={
                                text => {
                                    setBusiness(text)
                                    console.log(text)
                                }
                            }
                            style={{
                                borderWidth: checkbusiness,
                                backgroundColor: '#eef2f5',
                                borderColor: 'red',
                                borderRadius: 20,
                                width: width * 1 / 1.16,


                            }}
                        />
                    </View>

                    < View style={{ paddingTop: 10 }}>
                        <Input
                            placeholder='Email'
                            textStyle={{
                                fontFamily: 'Hindregular'
                            }}
                            //mode="outlined"
                            keyboardType='email-address'
                            value={email}
                            size="large"
                            onChangeText={
                                text => {
                                    validate(text)
                                    setEmail(text)
                                    console.log(text)
                                }
                            }
                            style={{
                                borderWidth: checkemail,
                                backgroundColor: '#eef2f5',
                                borderColor: 'red',
                                borderRadius: 20,
                                width: width * 1 / 1.16,

                            }}
                        />
                    </View>
                    <View style={{ paddingTop: 10 }}>
                        <Input
                            placeholder='Location'
                            textStyle={{
                                fontFamily: 'Hindregular'
                            }}
                            //mode="outlined"
                            value={location}
                            size="large"
                            onChangeText={
                                text => {
                                    setLocation(text)
                                    console.log(text)
                                }
                            }
                            style={{
                                borderWidth: checklocation,
                                backgroundColor: '#eef2f5',
                                borderColor: 'red',
                                borderRadius: 20,
                                width: width * 1 / 1.16,

                            }}
                        />
                    </View>

                    <View style={{ paddingBottom: 20, paddingTop: btn2 ? 80 : 10 }}>
                        {btn2 ?

                            <Button onPress={() => Sendit()} style={{ backgroundColor: "#5568fe", borderColor: "#3B3D3E", width: 350, height: 60, borderRadius: 20 }}><Text style={{ fontFamily: 'Hindbold', fontSize: 20 }}>Continue</Text></Button> :
                            <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                                <LottieView
                                    autoPlay
                                    style={{ width: 150, height: 150 }}
                                    source={require('../../assets/loadinghand.json')}
                                />
                            </View>}

                    </View>
                </View>

                {/*//page 3 */}
                <View style={styles.container}>
                    <View style={{ paddingTop: 60, justifyContent: 'flex-start', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                        <Pressable onPress={() => navigation.goBack()}>
                            <MaterialCommunityIcons name="arrow-left" size={33} />
                        </Pressable>
                    </View>
                    <View style={{ paddingTop: 20 }}>
                        <Text style={{ fontFamily: 'Hindbold', color: 'black', fontSize: 20 }}>Upload National ID</Text>
                    </View>

                    <View style={{
                        paddingTop: height * 1 / 22
                    }}>
                        {
                            idimage == null ? <View style={{ alignItems: 'center' }}>
                                <Pressable onPress={pickIdImage}>
                                    <View style={{
                                        borderWidth: 1,
                                        height: 130,
                                        width: 200,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: 20,
                                        borderColor: 'red'
                                    }}>
                                        <Ionicons name="close-circle-outline" size={40} color='red' />
                                        <Text style={{
                                            fontFamily: 'Hindsemi',
                                            color: 'red'
                                        }}>ID Front</Text>
                                    </View>
                                </Pressable>
                                <View style={{ paddingTop: 20 }}>
                                    <Text style={{ fontFamily: 'Hindsemi' }}>Click card to upload</Text>
                                </View>
                            </View>
                                : <Card style={{
                                    borderWidth: 1,
                                    height: 200,
                                    width: 200,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Image source={{ uri: idimage }} style={{ width: 200, height: 200 }} />
                                </Card>
                        }

                        {
                            idimageback == null ? <View style={{ alignItems: 'center' }}>
                                <Pressable onPress={pickIdImageback}>
                                    <View style={{
                                        borderWidth: 1,
                                        height: 130,
                                        width: 200,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: 20,
                                        borderColor: 'red'
                                    }}>
                                        <Ionicons name="close-circle-outline" size={40} color='red' />
                                        <Text style={{
                                            fontFamily: 'Hindsemi',
                                            color: 'red'
                                        }}>ID back</Text>
                                    </View>
                                </Pressable>
                                <View style={{ paddingTop: 20 }}>
                                    <Text style={{ fontFamily: 'Hindsemi' }}>Click card to upload</Text>
                                </View>
                            </View>
                                : <Card style={{
                                    borderWidth: 1,
                                    height: 200,
                                    width: 200,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Image source={{ uri: idimageback }} style={{ width: 200, height: 200 }} />
                                </Card>
                        }
                    </View>

                    <View style={{ paddingTop: height * 1 / 17 }}>
                        <Button onPress={() => setSelectedIndex(3)} style={{ backgroundColor: "#5568fe", borderColor: "#3B3D3E", width: 350, height: 60, borderRadius: 20 }}><Text style={{ fontFamily: 'Hindbold', fontSize: 20 }}>Continue</Text></Button>
                    </View>

                </View>

                {/*//page 4 */}
                <View style={styles.container}>
                    <View style={{ paddingTop: 60, justifyContent: 'flex-start', alignItems: 'flex-start', alignContent: 'flex-start' }}>
                        <Pressable onPress={() => navigation.goBack()}>
                            <MaterialCommunityIcons name="arrow-left" size={33} />
                        </Pressable>
                    </View>
                    <View style={{ paddingTop: 20 }}>
                        <Text style={{ fontFamily: 'Hindbold', color: 'black', fontSize: 20 }}>Upload permit</Text>
                    </View>

                    <View style={{
                        paddingTop: height * 1 / 22
                    }}>

                        {
                            permit == null ? <View style={{ alignItems: 'center' }}>
                                <Pressable onPress={pickPermit}>
                                    <View style={{
                                        borderWidth: 1,
                                        height: 130,
                                        width: 200,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: 20,
                                        borderColor: 'red'
                                    }}>
                                        <Ionicons name="close-circle-outline" size={40} color='red' />
                                        <Text style={{
                                            fontFamily: 'Hindsemi',
                                            color: 'red'
                                        }}>Business Permit</Text>
                                    </View>
                                </Pressable>
                                <View style={{ paddingTop: 20 }}>
                                    <Text style={{ fontFamily: 'Hindsemi' }}>Click card to upload</Text>
                                </View>
                            </View>
                                : <Card style={{
                                    borderWidth: 1,
                                    height: 200,
                                    width: 200,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Image source={{ uri: permit }} style={{ width: 200, height: 200 }} />
                                </Card>
                        }
                    </View>

                    <View style={{ paddingTop: height * 1 / 17 }}>
                        {btn3 ? <Button onPress={() => Sendit()} style={{ backgroundColor: "#5568fe", borderColor: "#3B3D3E", width: 350, height: 60, borderRadius: 20 }}><Text style={{ fontFamily: 'Hindbold', fontSize: 20 }}>Continue</Text></Button> : <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                            <LottieView
                                autoPlay
                                style={{ width: 150, height: 150 }}
                                source={require('../../assets/loadinghand.json')}
                            />
                        </View>}
                    </View>

                </View>
                {/*//page 4 */}
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <View>
                        <Text style={{ fontFamily: 'Hindbold', fontSize: 28 }}>Successfull sign up</Text>
                    </View>
                    <View>
                        <LottieView
                            autoPlay
                            style={{ width: 250, height: 250 }}
                            source={require('../../assets/51643-podcast-girl.json')}
                        />
                    </View>
                    <View>
                        <Text style={{ fontFamily: 'Hindbold' }}>We will get back to you shortly</Text>
                    </View>

                    <View style={{ paddingTop: 20 }}>
                        <Button onPress={() => navigation.navigate('Login')} style={{ backgroundColor: "#5568fe", borderColor: "#3B3D3E", width: 350, height: 60, borderRadius: 20 }}><Text style={{ fontFamily: 'Hindbold', fontSize: 20 }}>Continue</Text></Button>
                    </View>
                </View>

            </ViewPager>


        )
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        //padding: 8,

        alignItems: 'center'
        //flexDirection: 'column',
        //justifyContent: 'space-between'
        //alignContent: 'center',
        //justifyContent:'center'
    }
})