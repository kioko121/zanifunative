import * as React from 'react'
import { View, StyleSheet, Pressable, ToastAndroid } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import {  Text } from '@ui-kitten/components';
import LottieView from 'lottie-react-native'
import PhoneInput from "react-native-phone-number-input";
import { fireman } from '../../api/base';
import { FirebaseRecaptchaVerifierModal } from 'expo-firebase-recaptcha';
import firebase from 'firebase';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Button } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { logger } from "react-native-logs"
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import { Dimensions } from 'react-native';
import { Spinner } from '@ui-kitten/components';

const { width, height } = Dimensions.get("window")

export default function Phone({ navigation }) {

    const log = logger.createLogger()

    const phoneInput = useRef<PhoneInput>(null);
    const [phone, setPhone] = useState('')
    const [pin, setPin] = useState()
    const recaptchaVerifier = useRef(null)
    const [verificationId, setVerificationId] = React.useState();
    const [verificationCode, setVerificationCode] = React.useState();
    const attemptInvisibleVerification = false;
    const [loading, setLoading] = useState(true)
    const [isloading, setIsloading] = useState(false)

    const dispatch = useDispatch()
    const verification = useSelector((state) => state.verification)

    let [fontsLoaded] = useFonts({
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })



    const onPress = async () => {
        setIsloading(true)
        console.log(phone)
        try {

            if (phone != '') {
                navigation.navigate('Verify', {
                    current: phone
                })
    
                setLoading(false)
            } else {
                ToastAndroid.showWithGravityAndOffset(
                    'Please fill phone',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
                setIsloading(false)
            }
            /*
            const phoneProvider = new firebase.auth.PhoneAuthProvider()
            phoneProvider
                .verifyPhoneNumber(phone, recaptchaVerifier.current)
                .then(data => {
                    setVerificationId(data)
                    /*AsyncStorage.clear()

                    AsyncStorage.multiSet([
                        ['verificationid', verificationId]
                    ])
                    
                    if (data) {
                        dispatch({
                            type: "ADD_VERIFICATION",
                            payload: data
                        })


                        log.info(verification)


                        navigation.navigate('Verify', {
                            verification: data
                        })

                    }
                })
            //.then(setVerificationId)
            */

        } catch (err) {
            setIsloading(false)
            console.log(err)
            console.log(err.message)
        }


    }

    useEffect(() => {
        setLoading(false)
    }, [])

    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <Spinner/>
                </View>

            </View>
        )
    } else {
        return (

            <View style={styles.container}>


                <View
                    style={{
                        //paddingLeft: 20,
                        //paddingRight: 20,
                        paddingBottom: 30,
                        paddingTop: 20
                    }}>

                    <View style={{
                        paddingTop: 0,
                        paddingLeft: 40,
                        paddingRight: 40,
                        paddingBottom: 20,
                        alignItems: 'flex-start',
                        width: width
                    }}>
                        <Pressable onPress={() => navigation.goBack()} style={{ paddingBottom: 20}}>
                            <MaterialCommunityIcons name="arrow-left" size={33} />
                        </Pressable>
                        <Text style={{ fontFamily: 'Osans', color: 'black', fontSize: 25 }}>Enter your Phone number</Text>
                    </View>

                    
                </View>

                <View style={{ alignItems: 'center' }}>
                    <PhoneInput
                        textInputStyle={{ fontFamily: 'Osans', justifyContent: 'center', alignContent: 'center', height: 50, fontSize: 14 }}
                        codeTextStyle={{ fontFamily: 'Osanssemi' }}
                        textContainerStyle={{ height: 50, backgroundColor: '#fff' }}
                        containerStyle={{ backgroundColor: '#fff', height: 60, justifyContent: 'center', alignContent: 'center', borderWidth: 1, borderColor: 'grey', width: 340, borderRadius: 10, paddingRight: 20, elevation: 0 }}
                        ref={phoneInput}
                        //defaultValue={value}
                        defaultCode="KE"
                        layout="first"
                        onChangeText={(text) => {
                            //setValue(text);
                            //setPhone(text)
                        }}
                        onChangeFormattedText={(text) => {
                            //setFormattedValue(text);
                            console.log(text)
                            setPhone(text)
                            dispatch({
                                type: "ADD_PHONE",
                                payload: text
                            })
                        }}
                        //withDarkTheme
                        withShadow
                        autoFocus
                    />

                    <View style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 40, paddingLeft: 20, paddingRight: 20 }}>


                    </View>
                    <View style={{ paddingBottom: 20, paddingTop: 80 }}>
                        <Button
                            mode='contained'
                            uppercase={false}
                            color='#3b3d3e'
                            compact={false}
                            loading={isloading}
                            onPress={() => onPress()}
                             contentStyle={{
                                height: 60,
                            }}
                            labelStyle={{
                                fontFamily: 'Osanssemi',
                                color: 'white'
                             }}
                            style={{
                                borderRadius: 10,
                                width: 350,
                                //paddingBottom: 20,
                                //paddingTop: 20,
                                height: 60,
                            }}>
                            Continue
                        </Button>
                    </View>
                </View>
            </View>


        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //paddingTop: height * 1 / 8,
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        padding: 8,
        paddingLeft: 20,
        alignItems: 'center',
        //justifyContent:'center',
        paddingRight: 20

    },

});