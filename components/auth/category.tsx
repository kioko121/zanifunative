import * as React from 'react'
import { ImageBackground, Picker, View, StyleSheet, ActivityIndicator, KeyboardAvoidingView, ScrollView, Image, TouchableNativeFeedback } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, Button, Layout, Text, Input, Icon, Select, SelectItem, Card } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import PinInput from 'react-pin-input';
import firebase from 'firebase';
import { fireman } from '../../api/base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { ToastAndroid } from 'react-native';
import { Pressable } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { TextInput } from 'react-native-paper';


export default function Category({ navigation }) {

    const [pin, setPin] = useState()
    const pinInput = useRef()
    const [value, setValue] = useState('');
    const [name, setName] = useState()
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });
    const CELL_COUNT = 6;
    const numba = ''
    const forverification = navigation.getParam('verification')
    const [selectedIndex, setSelectedIndex] = useState()
    const [selectedValue, setSelectedValue] = useState();


    let [fontsLoaded] = useFonts({
        'Hindbold': require('../../assets/HindSiliguri-Bold.ttf'),
        'Hindregular': require('../../assets/HindSiliguri-Regular.ttf'),
        'Hindmedium': require('../../assets/HindSiliguri-Medium.ttf'),
        'Hindsemi': require('../../assets/HindSiliguri-SemiBold.ttf'),
    })

    const doVerify = (nun) => {
        AsyncStorage.multiGet(['verificationid']).then((data) => {
            //setPin('')
            console.log(data.length),
                console.log(data[data.length - 1][1])

            console.log("&&&&&&&&&&&&&&&&&&&")

            //navigation.navigate('Home')

            console.log(nun)

            var varid = data[0][1]

            console.log('******************************')
            console.log(forverification)

            const cridential = firebase.auth.PhoneAuthProvider.credential(
                forverification,
                nun
            )

            fireman.auth().signInWithCredential(cridential).then((result) => {
                console.log(`pin used was ${nun}`)
                console.log(result)
                navigation.navigate('Home')
            }).catch((error) => {
                console.log(error)
                ToastAndroid.showWithGravityAndOffset(
                    error.message,
                    ToastAndroid.SHORT,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            })


        })
        //const cridential = firebase.auth.PhoneAuthProvider.credential(

        //)
    }

    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <LottieView
                        autoPlay
                        style={{ width: 800, height: 800 }}
                        source={require('../../assets/9305-loading-bloob.json')}
                    />
                </View>

            </View>
        )
    } else {
        return (
            <ApplicationProvider {...eva} theme={eva.light}>
                <KeyboardAvoidingView style={styles.container}>

                    <View>
                        <View style={{ paddingTop: 60 }}>
                            <Pressable onPress={() => navigation.goBack()}>
                                <MaterialCommunityIcons name="arrow-left" size={33} />
                            </Pressable>
                        </View>
                        <View style={{ alignItems: 'flex-start', justifyContent: 'flex-start', paddingBottom: 30, paddingLeft: 20, paddingRight: 20, paddingTop: 0 }}>
                            <Text style={{ fontFamily: 'Hindsemi', fontSize: 23 }}>Select the category that fits your business best</Text>
                        </View>

                        <View style={{ paddingTop: 0, paddingLeft: 20, paddingRight: 20 }}>
                            <View style={{ paddingBottom: 20 }}>
                                <Text style={{ fontFamily: 'Hindregular', fontSize: 14, color: 'grey' }}>This helps us to display products relevant to your businesss</Text>
                            </View>
                            <View
                                style={{
                                    borderWidth: 1,
                                    borderRadius: 5
                                }}
                            >
                                <Picker
                                    selectedValue={selectedValue}
                                    style={{ borderColor: 'black', borderWidth: 1 }}
                                    onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                                >
                                    <Picker.Item label="Duka" value="duka" />
                                    <Picker.Item label="Bar" value="bar" />
                                    <Picker.Item label="Wines & spirits" value="wines&spirits" />
                                    <Picker.Item label="Lpg gas" value="lpggas" />
                                </Picker>
                            </View>

                        </View>
                    </View>
                    <View style={{ alignItems: 'center', paddingBottom: 80 }}>
                        <Button style={{ backgroundColor: '#3b3d3e', borderColor: '#3b3d3e', marginTop: 120, width: 300 }}
                            onPress={() => {
                                navigation.navigate('Home')
                            }}
                        >
                            <Text style={{ fontFamily: 'Hindbold', color: 'white' }}>Save</Text>
                        </Button>
                    </View>
                </KeyboardAvoidingView>


            </ApplicationProvider>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'Inter_900Black',
        flex: 1,
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        padding: 0,
        paddingLeft: 20,
        paddingRight: 20,
        flexDirection: 'column',
        justifyContent: 'space-between'
        //alignItems: 'center'
    },
    root: { flex: 1, padding: 20 },
    title: { textAlign: 'center', fontSize: 30 },
    codeFieldRoot: { marginTop: 40 },
    cell: {
        padding: 0,
        margin: 10,
        width: 40,
        height: 40,
        lineHeight: 38,
        fontSize: 24,
        borderWidth: 2,
        borderColor: '#00000030',
        textAlign: 'center',
    },
    focusCell: {
        padding: 10,
        borderColor: '#000',
    },
})