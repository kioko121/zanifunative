import * as React from 'react'
import { ImageBackground, View, StyleSheet, FlatList, ActivityIndicator, KeyboardAvoidingView, ScrollView, Image, TouchableNativeFeedback } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { Layout, Text, Input, Icon, Select, SelectItem, Card, ViewPager } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import LottieView from 'lottie-react-native'
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import PinInput from 'react-pin-input';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { ToastAndroid } from 'react-native';
import { Pressable } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';
import { logger } from "react-native-logs"
import { Spinner } from '@ui-kitten/components';
import Ionicons from '@expo/vector-icons/build/Ionicons';
import { Dimensions } from 'react-native';
import { TextInput, Button, Chip } from 'react-native-paper'
import * as ImagePicker from 'expo-image-picker';
import * as DocumentPicker from 'expo-document-picker';
import { regmessage, reachtext } from '../../globals';

const { width, height } = Dimensions.get("window")

export default function Success({ route, navigation }) {
    let [fontsLoaded] = useFonts({
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })

    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <Spinner
                    />
                </View>

            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <View style={{
                    alignItems: 'center',
                    alignContent: 'center',
                    justifyContent: 'center',
                    height: height * 1 / 4,
                    borderBottomLeftRadius: 180,
                    borderBottomRightRadius: 180,
                    backgroundColor: '#eeeeee'
                }}>
                    <Ionicons name="checkmark-circle" color="black" size={83} />
                </View>
                <View style={{ paddingTop: height * 1/10, alignItems: 'center' }}>
                    <Text style={{ fontFamily: 'Osansbold', fontSize: 28 }}>Success</Text>
                </View>
                <View style={{ paddingTop: height * 1/15, alignItems: 'center', paddingLeft: 20, paddingRight: 20 }}>
                    <Text style={{ fontFamily: 'Osans', fontSize: 18, textAlign: 'center' }}>{regmessage }</Text>
                </View>
                <View style={{ paddingTop: height * 1/15, alignItems: 'center', paddingLeft: 20, paddingRight: 20 }}>
                    <Text style={{ fontFamily: 'Osans', fontSize: 18, textAlign: 'center' }}>{reachtext }</Text>
                </View>

                <View style={{ alignItems: 'center' }}>
                            <Button  style={{  borderWidth: 1,width: width * 1 / 1.2, padding: 8, marginTop: 20 }} color='#fff' uppercase={false} mode="outlined" onPress={
                        () => {
                                    navigation.navigate('Home')
                                }
                            }><Text style={{ fontFamily: 'Osanssemi', color: 'grey' }}>Go to Home</Text></Button>
                        </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        //padding: 0,
        //alignItems: 'center',
        
    },

})