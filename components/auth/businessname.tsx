import * as React from 'react'
import { ImageBackground, View, StyleSheet, ActivityIndicator, KeyboardAvoidingView, ScrollView, Image, TouchableNativeFeedback } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { Layout, Text, Input, Icon, Select, SelectItem, Card } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import LottieView from 'lottie-react-native'
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import PinInput from 'react-pin-input';
import firebase from 'firebase';
import { fireman } from '../../api/base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { ToastAndroid } from 'react-native';
import { Pressable } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';
import { logger } from "react-native-logs"
import { Spinner } from '@ui-kitten/components';
import Ionicons from '@expo/vector-icons/build/Ionicons';
import { Dimensions } from 'react-native';
import { TextInput, Button } from 'react-native-paper'

const { width, height } = Dimensions.get("window")

export default function Business({ route, navigation }) {

    const key = useSelector((state) => state.key)
    const bid = useSelector((state) => state.busnessid)
    const [name, setName] = useState('')
    const [location, setLocation] = useState('')
    const [loading, setLoading] = useState(false)
   
    const log = logger.createLogger()

    const updateMerch = () => {
        setLoading(true)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${key}`);

        var raw = JSON.stringify({
            "merchant_name": name,
            "business_name": name,
            //"email": email,
            "location": location
        });

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`https://development.zanifu.com/api/merchants${bid}`, requestOptions)
            .then(response => {
                var status = response.status
                log.debug(status)

                response.text().then((data) => {
                    var vitu = JSON.parse(data)

                    log.debug(vitu)
                })

                if (status == 200) {
                    navigation.navigate('Uploads')
                    setLoading(false)
                } else {
                    ToastAndroid.showWithGravityAndOffset(
                        'Something went wrong',
                        ToastAndroid.SHORT,
                        ToastAndroid.BOTTOM,
                        25,
                        50
                    )
                    setLoading(false)
                }
            })
            .catch(error => console.log('error', error));
    }

    let [fontsLoaded] = useFonts({
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })

    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <Spinner
                    />
                </View>

            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <View style={{ paddingTop: Constants.statusBarHeight * 2 }}>
                    <View style={{
                        alignContent: 'flex-start',
                        paddingLeft: 20,
                        paddingRight: 20,
                        paddingBottom: 40
                    }}>
                        <Pressable onPress={() => navigation.goBack()}>
                            <Ionicons name="arrow-back-sharp" color="black" size={33} />
                        </Pressable>
                    </View>


                    <View style={{ alignItems: 'center' }}>
                        <TextInput
                            placeholder='Business Name'
                            mode='outlined'
                            label="Business Name"
                            keyboardType='keyword'
                            onChangeText={
                                text => {
                                    setName(text)
                                }
                            }
                            style={{
                                backgroundColor: '#fff',
                                borderRadius: 10,
                                width: width * 1 / 1.16,
                                fontFamily: 'Osans'
                            }}
                        />
                    </View>

                    <View style={{ alignItems: 'center', paddingTop: 20 }}>
                        <TextInput
                            placeholder='Location'
                            mode='outlined'
                            label="Location"
                            keyboardType='keyword'
                            onChangeText={
                                text => {
                                    setLocation(text)
                                }
                            }
                            style={{
                                backgroundColor: '#fff',
                                borderRadius: 10,
                                width: width * 1 / 1.16,
                                fontFamily: 'Osans'
                            }}
                        />
                    </View>

                    <View style={{ alignItems: 'center', paddingTop: 20 }}>
                        {
                            loading ? <View><Spinner /></View> :
                                <Button
                                    style={{
                                        width: width * 1 / 1.2,
                                        //padding: 8,
                                        marginTop: 20
                                    }}
                                    color='#3b3d3e'
                                    labelStyle={{
                                        fontFamily: 'Osansbold'
                                     }}
                                    uppercase={false}
                                    contentStyle={{
                                        height: 60
                                     }}
                                    mode="contained"
                                    onPress={
                                () => {
                                    if (name == '') {
                                        ToastAndroid.showWithGravityAndOffset(
                                            'Please fill Business name',
                                            ToastAndroid.LONG,
                                            ToastAndroid.BOTTOM,
                                            25,
                                            50
                                        )
                                    }
                                    else if (location == '') {
                                        ToastAndroid.showWithGravityAndOffset(
                                            'Please fill Location',
                                            ToastAndroid.LONG,
                                            ToastAndroid.BOTTOM,
                                            25,
                                            50
                                        )
                                    } else {
                                        //log.debug(bid)
                                        updateMerch()
    
                                    }
                                }
                            }><Text style={{ fontFamily: 'Osanssemi', color: 'white' }}>Continue</Text></Button>
                        }
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'Inter_900Black',
        flex: 1,
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        padding: 0,
        //alignItems: 'center'
    },
    root: { flex: 1, padding: 20 },
    title: { textAlign: 'center', fontSize: 30 },
    codeFieldRoot: { marginTop: 40 },
    cell: {
        padding: 0,
        margin: 10,
        width: 40,
        height: 40,
        lineHeight: 38,
        fontSize: 24,
        borderWidth: 2,
        borderColor: '#00000030',
        textAlign: 'center',
    },
    focusCell: {
        padding: 10,
        borderColor: '#000',
    },
})