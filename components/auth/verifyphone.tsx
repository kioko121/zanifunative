import * as React from 'react'
import {
    ImageBackground,
    View,
    StyleSheet,
    ActivityIndicator,
    KeyboardAvoidingView,
    ScrollView,
    Image,
    TouchableNativeFeedback,
    BackHandler,
} from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { Button, Layout, Text, Input, Icon, Select, SelectItem, Card } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import LottieView from 'lottie-react-native'
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import PinInput from 'react-pin-input';
import firebase from 'firebase';
import { fireman } from '../../api/base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { ToastAndroid } from 'react-native';
import { Pressable } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';
import { logger } from "react-native-logs"
import { Spinner } from '@ui-kitten/components';

export default function Verify({ route, navigation }) {

    const [pin, setPin] = useState()
    const pinInput = useRef()
    const [value, setValue] = useState('');
    const [modalvisible, setModalvisible] = useState(false)
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });
    const CELL_COUNT = 6;

    const dispatch = useDispatch()
    const key = useSelector((state) => state.key)
    const log = logger.createLogger()
    const { current } = route.params;


    function handleBackButtonClick() {
        navigation.navigate('Login');
        return true;
    }

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    })


    let [fontsLoaded] = useFonts({
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })

    const doVerify = (nun) => {
        setModalvisible(true)
        log.info(current)

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Accept", "application/json");

        var raw = JSON.stringify({
            "phone": current,
            "password": nun
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://development.zanifu.com/api/login", requestOptions)
            .then(response => {
                log.debug(response.status)

                if (response.status == 200) 
                {
                response.text().then((data) => {
                    log.debug(data)
                    var first = JSON.parse(data)

                    if(first.token){
                        dispatch({
                            type: "ADD_KEY",
                            payload: first.token
                        })

                        dispatch({
                            type: "ADD_KEY",
                            payload: first.token
                        })

                        dispatch({
                            type: "ADD_NEWBIE",
                            payload: false
                        })

                        navigation.navigate('Home')
                    }
                })
            }
            else{
                setModalvisible(false)
                response.text().then((data) => {
                    log.debug(data)
                    var first = JSON.parse(data)

                    ToastAndroid.showWithGravityAndOffset(
                        first.message,
                        ToastAndroid.LONG,
                        ToastAndroid.BOTTOM,
                        25,
                        50
                    )
                })
                
            }

            })
            
            .catch(error => {
                log.debug(pin)
                log.debug(current)
                log.error('error', error)

            });
        /*const cridential = firebase.auth.PhoneAuthProvider.credential(
            verification,
            nun
        )

        fireman.auth().signInWithCredential(cridential).then((result) => {
            console.log(`pin used was ${nun}`)
            console.log(result)
            navigation.navigate('Home')
        }).catch((error) => {
            console.log(error)
            ToastAndroid.showWithGravityAndOffset(
                error.message,
                ToastAndroid.SHORT,
                ToastAndroid.BOTTOM,
                25,
                50
            )
        })*/
    }

    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <Spinner
                    />
                </View>

            </View>
        )
    } else {
        return (
            <View style={styles.container}>

                <View style={{ paddingTop: 20,  paddingBottom: 20 , alignItems:  'flex-start', paddingLeft: 40}}>
                    <Pressable onPress={() => navigation.goBack()}>
                        <MaterialCommunityIcons name="arrow-left" size={33} />
                    </Pressable>
                </View>
                <View style={{ alignItems: 'flex-start', justifyContent: 'center', paddingBottom: 30, paddingLeft: 40, paddingRight: 20 }}>
                    <Text style={{ fontFamily: 'Osanssemi', fontSize: 20 }}>Enter PIN</Text>
                    <View style={{ paddingTop: 10}}>
                    <Text style={{ fontFamily: 'Osanssemi', fontSize: 17, color: '#666464' }}>Enter your Zanifu pin</Text>
                    </View>
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 40, paddingLeft: 20, paddingRight: 20 }}>

                    {modalvisible ?
                        <View style={styles.container}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                                <Spinner
                                />
                            </View>

                        </View> :
                        <SmoothPinCodeInput
                            cellStyle={{
                                borderRadius: 10,
                                borderBottomWidth: 2,
                                borderWidth: 1,
                                marginLeft: 20
                            
                            }}
                            cellStyleFocused={{
                                borderBottomWidth: 4
                            }}
                            textStyle={{
                                fontFamily: 'Hindregular',
                                color: 'black'
                             }}
                            password mask="*"
                            cellSize={66}
                            codeLength={4}
                            value={pin}
                            onFulfill={
                                (data) => {
                                    console.log(`pin complete is ${data}`)
                                    doVerify(data)
                                    //console.log(pin.length)
                                }
                            }
                            //onBackspace={doVerify}
                            onTextChange={(data) => {

                                setPin(data)
                                console.log(data)
                                //setPin(data)
                            }} />}

                </View>
                <View>
                    <View style={{ paddingTop: 120 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 40 }}>
                            <Text style={{ textDecorationLine: 'underline', fontFamily: 'Osansbold' }}>Forgot PIN ?</Text>
                        </View>
                    </View>
                </View>
            </View>


        )
    }
}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'Inter_900Black',
        flex: 1,
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        padding: 0,
        //alignItems: 'center'
    },
    root: { flex: 1, padding: 20 },
    title: { textAlign: 'center', fontSize: 30 },
    codeFieldRoot: { marginTop: 40 },
    cell: {
        padding: 0,
        margin: 10,
        width: 40,
        height: 40,
        lineHeight: 38,
        fontSize: 24,
        borderWidth: 2,
        borderColor: '#00000030',
        textAlign: 'center',
    },
    focusCell: {
        padding: 10,
        borderColor: '#000',
    },
})