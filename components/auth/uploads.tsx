import * as React from 'react'
import { ImageBackground, View, StyleSheet, FlatList, ActivityIndicator, KeyboardAvoidingView, ScrollView, Image, TouchableNativeFeedback } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { Layout, Text, Input, Icon, Select, SelectItem, Card, ViewPager } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import LottieView from 'lottie-react-native'
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import PinInput from 'react-pin-input';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { ToastAndroid } from 'react-native';
import { Pressable } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';
import { logger } from "react-native-logs"
import { Spinner } from '@ui-kitten/components';
import Ionicons from '@expo/vector-icons/build/Ionicons';
import { Dimensions } from 'react-native';
import { TextInput, Button, Chip } from 'react-native-paper'
import * as ImagePicker from 'expo-image-picker';
import * as DocumentPicker from 'expo-document-picker';

const { width, height } = Dimensions.get("window")

export default function Uploads({ route, navigation }) {

    const key = useSelector((state) => state.key)
    const bid = useSelector((state) => state.busnessid)
    const [name, setName] = useState('')
    const [location, setLocation] = useState('')
    const [selected, setSelected] = useState(0)
    const [idimage, setIdimage] = useState(null);
    const [idimageback, setIdimageback] = useState(null);
    const [permit, setPermit] = useState(null)
    const [mpesa, setMpesa] = useState(null)
    const [other, setOther] = useState(null)
    const [iddone, setIddone] = useState(false)
    const [permitdone, setPermitdone] = useState(false)
    const [mpesadone, setMpesadone] = useState(false)
    const [loading, setLoading] = useState(false)
    const [bugs, addBugs] = useState([])
    const [isloading, setIsloading] = useState(false)
    const log = logger.createLogger()
    const dispatch = useDispatch()

    const things = [
        { id: 1, name: "National Id" },
        { id: 2, name: "Business Permit" },
        { id: 3, name: "Mpesa Statement" },
        { id: 4, name: "Photo of your outlet" },
        { id: 5, name: "Other documents(optional)" }
    ]

    const uploadimages = () => {
        setLoading(true)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "multipart/form-data");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${key}`);

        var formdata = new FormData();
        formdata.append("front", idimage);
        formdata.append("back", idimageback);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: formdata,
            redirect: 'follow'
        };

        fetch(`https://development.zanifu.com/api/merchants/${bid}/national-id`, requestOptions)
            .then(response => {
                var status = response.status
                log.debug("wait fo it")
                log.debug(status)

                if (status == 200) {
                    setIddone(true)
                    setLoading(false)


                } else {
                    setIddone(false)
                    ToastAndroid.showWithGravityAndOffset(
                        'Something went wrong',
                        ToastAndroid.SHORT,
                        ToastAndroid.BOTTOM,
                        25,
                        50
                    )
                }

            })

            .catch(error => {
                //log.debug(`Bearer ${key}`)
                //log.debug(`https://development.zanifu.com/api/merchants/${bid}/national-ids`)
                log.error('error', error)
                setIddone(false)
                ToastAndroid.showWithGravityAndOffset(
                    'Something went wrong, please try again later',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            });



    }

    const pickIdImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        console.log(result);

        if (!result.cancelled) {
            setIdimage(result.uri);

            if (idimageback == null) {
                ToastAndroid.showWithGravityAndOffset(
                    'Please fill back',
                    ToastAndroid.SHORT,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            }

            else {
                uploadimages()
            }
        }
    };

    const pickIdImageback = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        console.log(result);

        if (!result.cancelled) {
            setIdimageback(result.uri);

            if (idimage == null) {
                ToastAndroid.showWithGravityAndOffset(
                    'Please fill front',
                    ToastAndroid.SHORT,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            }

            else {
                uploadimages()
            }

        };
    }

    const pickPermit = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        console.log(result);

        if (!result.cancelled) {
            setPermit(result.uri);

            uploadPermit()
        }
    };

    const pickMpesa = async () => {
        let result = await DocumentPicker.getDocumentAsync({});

        console.log(result);

        if (!result.cancelled) {
            setMpesa(result.uri);

            uploadMpesa()
        }
    };

    const uploadMpesa = () => {
        setLoading(true)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "multipart/form-data");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${key}`);

        var formdata = new FormData();
        formdata.append("business_permit", permit);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: formdata,
            redirect: 'follow'
        };

        fetch(`https://development.zanifu.com/api/merchants/${bid}/mpesa-statement`, requestOptions)
            .then(response => {
                var status = response.status
                log.debug(status)

                if (status == 200) {
                    setMpesadone(true)
                    setLoading(false)
                }
                response.text().then((data) => {
                    var vitu = JSON.parse(data)

                    log.debug(vitu)
                })
            })
            .catch(error => {
                log.debug(`Bearer ${key}`)
                log.debug(`https://development.zanifu.com/api/merchants/${bid}/mpesa-statement`)
                log.error('error', error)

                ToastAndroid.showWithGravityAndOffset(
                    'Something went wrong, please try again later',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            });
    }

    const pickOther = async () => {
        let result = await DocumentPicker.getDocumentAsync({});

        console.log(result);

        if (!result.cancelled) {
            setOther(result.uri);

            uploadOther()
            addBugs(oldArray => [...oldArray, { name: result.name }])
        }
    };

    const uploadOther = () => {
        setLoading(true)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "multipart/form-data");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${key}`);

        var formdata = new FormData();
        formdata.append("files[]", permit);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: formdata,
            redirect: 'follow'
        };

        fetch(`https://development.zanifu.com/api/merchants/${bid}/other-documents`, requestOptions)
            .then(response => {
                var status = response.status
                log.debug(status)

                if (status == 200) {
                    setMpesadone(true)
                    setLoading(false)
                }
                response.text().then((data) => {
                    var vitu = JSON.parse(data)

                    log.debug(vitu)
                })
            })
            .catch(error => {
                log.debug(`Bearer ${key}`)
                log.debug(`https://development.zanifu.com/api/merchants/${bid}/mpesa-statement`)
                log.error('error', error)

                ToastAndroid.showWithGravityAndOffset(
                    'Something went wrong, please try again later',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            });
    }



    const uploadPermit = () => {
        setLoading(true)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "multipart/form-data");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${key}`);

        var formdata = new FormData();
        formdata.append("business_permit", permit);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: formdata,
            redirect: 'follow'
        };

        fetch(`https://development.zanifu.com/api/merchants/${bid}/business-permit`, requestOptions)
            .then(response => {
                var status = response.status
                log.debug(status)

                if (status == 200) {
                    setPermitdone(true)
                    setLoading(false)
                }
                response.text().then((data) => {
                    var vitu = JSON.parse(data)

                    log.debug(vitu)
                })
            })
            .catch(error => {
                log.debug(`Bearer ${key}`)
                log.debug(`https://development.zanifu.com/api/merchants/${bid}/business-permit`)
                log.error('error', error)

                ToastAndroid.showWithGravityAndOffset(
                    'Something went wrong, please try again later',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                )
            });
    }




    let [fontsLoaded] = useFonts({
        'Osans': require('../../assets/OpenSans-Regular.ttf'),
        'Osanssemi': require('../../assets/OpenSans-SemiBold.ttf'),
        'Osansbold': require('../../assets/OpenSans-Bold.ttf')
    })

    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <Spinner
                    />
                </View>

            </View>
        )
    } else {
        return (
            <View style={styles.container}>
                <ViewPager
                    style={{ paddingLeft: 20, paddingRight: 20, alignItems: 'center', alignContent: 'center', justifyContent: 'center' }}
                    selectedIndex={selected}
                    scrollEnable={false}
                    swipeEnabled={false}
                    onSelect={index => setSelected(index)}
                >
                    {/*pageone*/}
                    <View style={{ paddingTop: Constants.statusBarHeight }}>
                        <View
                            style={{
                                paddingTop: Constants.statusBarHeight,
                                alignItems: 'flex-start',
                            }}>

                        </View>
                        <View style={{
                            alignContent: 'flex-start',
                            width: width,
                            flexDirection: 'column'
                        }}>
                            <Pressable onPress={() => navigation.goBack()}>
                                <Ionicons name="arrow-back-sharp" color="black" size={33} />
                            </Pressable>
                        </View>

                        <View style={{ paddingTop: 20 }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 19 }}>Upload documents</Text>
                        </View>

                        <View style={{ paddingTop: 20, width: width - 10 }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 17, color: 'grey' }}>Hi, we will require the following documents to be able to process your application.</Text>
                        </View>

                        <View>
                            <FlatList
                                data={things}
                                renderItem={
                                    (item) => (
                                        <View style={{ flexDirection: 'row', paddingTop: 20 }}>
                                            <View style={{ borderRadius: 20, borderWidth: 1, width: 30, height: 30, alignItems: 'center', borderColor: 'grey' }}>
                                                <Text style={{ fontFamily: 'Osans' }}>{item.item.id}</Text>

                                            </View>
                                            <View style={{ paddingLeft: 20 }}>
                                                <Text style={{ fontFamily: 'Osans' }}>{item.item.name}</Text>
                                            </View>
                                        </View>
                                    )
                                }
                            ></FlatList>
                        </View>

                        <View style={{ width: width - 20, alignItems: 'center' }}>
                            <Button
                                style={{
                                    width: width * 1 / 1.2,
                                    //padding: 8,
                                    marginTop: 20
                                }}
                                color='#3b3d3e'
                                uppercase={false}
                                labelStyle={{
                                    fontFamily: 'Osansbold'
                                 }}
                                contentStyle={{
                                    height: 60
                                 }}
                                mode="contained"
                                onPress={
                                () => {
                                    setSelected(1)
                                }
                            }>Start Uploading</Button>
                        </View>

                    </View>

                    {/*pagetwo*/}
                    <View style={{ paddingTop: Constants.statusBarHeight}}>
                        <View
                            style={{
                                paddingTop: Constants.statusBarHeight,
                                alignItems: 'flex-start',
                            }}>

                        </View>
                        <View style={{
                            alignContent: 'flex-start',
                            width: width * 1 / 1.4,
                            flexDirection: 'column',
                            paddingLeft: 20,
                            paddingRight: 20
                        }}>
                            <Pressable onPress={() => navigation.goBack()}>
                                <Ionicons name="arrow-back-sharp" color="black" size={33} />
                            </Pressable>
                        </View>
                        <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center', paddingTop: 20, width: width }}>
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#c4c4c4', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#f2f2f2', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#f2f2f2', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#f2f2f2', margin: 10 }} />

                        </View>

                        <View style={{ width: width, paddingLeft: 20, paddingRight: 20, paddingTop: 20 }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 23 }}>National ID</Text>
                        </View>
                        <View style={{ width: width, paddingLeft: 20, paddingRight: 20, paddingTop: 20 }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 16, color: 'gray' }}>Upload the front side of your ID</Text>
                        </View>

                        <View style={{ alignItems: 'center', paddingTop: 20 }}>
                            {
                                idimage == null ? <View style={{ alignItems: 'center' }}>
                                    <Pressable onPress={pickIdImage}>
                                        <View style={{
                                            borderWidth: 1,
                                            borderStyle: 'dashed',
                                            height: 80,
                                            width: width * 1 / 1.2,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            borderRadius: 10,
                                            borderColor: 'grey',
                                            flexDirection: 'row'
                                        }}>
                                            <Ionicons name="add" size={40} color='black' />
                                            <Text style={{
                                                fontFamily: 'Hindsemi',
                                                color: 'grey'
                                            }}>ID Front</Text>
                                        </View>
                                    </Pressable>
                                    <View style={{ paddingTop: 20 }}>
                                        <Text style={{ fontFamily: 'Hindsemi' }}>Click card to upload</Text>
                                    </View>

                                    <View>

                                    </View>
                                </View>
                                    : <Card style={{
                                        borderWidth: 1,
                                        height: 200,
                                        width: 200,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                        <Image source={{ uri: idimage }} style={{ width: 200, height: 200 }} />
                                    </Card>
                            }
                        </View>

                        <View style={{ alignItems: 'center', paddingTop: 20 }}>
                            {
                                idimageback == null ? <View style={{ alignItems: 'center' }}>
                                    <Pressable onPress={pickIdImageback}>
                                        <View style={{
                                            borderWidth: 1,
                                            borderStyle: 'dashed',
                                            height: 80,
                                            width: width * 1 / 1.2,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            borderRadius: 10,
                                            borderColor: 'grey',
                                            flexDirection: 'row'
                                        }}>
                                            <Ionicons name="add" size={40} color='black' />
                                            <Text style={{
                                                fontFamily: 'Hindsemi',
                                                color: 'grey'
                                            }}>ID Back</Text>
                                        </View>
                                    </Pressable>
                                    <View style={{ paddingTop: 20 }}>
                                        <Text style={{ fontFamily: 'Hindsemi' }}>Click card to upload</Text>
                                    </View>
                                </View>
                                    : <Card style={{
                                        borderWidth: 1,
                                        height: 200,
                                        width: 200,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                        <Image source={{ uri: idimageback }} style={{ width: 200, height: 200 }} />
                                    </Card>
                            }

                            <View style={{ alignItems: 'center' }}>
                                {
                                    loading ? <View style={{ paddingTop: 20 }}><Spinner /></View> :
                                        <Button
                                            style={{
                                                width: width * 1 / 1.2,
                                                //padding: 8,
                                                marginTop: 20
                                            }}
                                            color='#3b3d3e'
                                            uppercase={false}
                                            labelStyle={{
                                                fontFamily: 'Osansbold'
                                             }}
                                            mode="contained"
                                            contentStyle={{
                                                height: 60
                                            }
                                            }
                                            onPress={
                                            () => {
                                                log.debug('hmmmm...............')
                                                if (iddone) {
                                                    setSelected(2)
                                                }

                                                if (iddone == false) {
                                                    ToastAndroid.showWithGravityAndOffset(
                                                        'Please upload ID',
                                                        ToastAndroid.SHORT,
                                                        ToastAndroid.BOTTOM,
                                                        25,
                                                        50
                                                    )
                                                }
                                            }
                                        }>Continue</Button>
                                }
                            </View>

                            
                        </View>


                    </View>

                    {/*pagethree*/}
                    <View style={styles.container}>
                        <View
                            style={{
                                paddingTop: Constants.statusBarHeight,
                                alignItems: 'flex-start',
                            }}>

                        </View>
                        <View style={{
                            alignContent: 'flex-start',
                            width: width * 1 / 1.4,
                            //flexDirection: 'column',
                            paddingLeft: 20,
                            paddingRight: 20
                        }}>
                            <Pressable onPress={() => navigation.goBack()}>
                                <Ionicons name="arrow-back-sharp" color="black" size={33} />
                            </Pressable>
                        </View>
                        <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center', paddingTop: 20, width: width }}>
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#c4c4c4', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#c4c4c4', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#f2f2f2', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#f2f2f2', margin: 10 }} />

                        </View>

                        <View style={{
                            paddingTop: 20,
                            paddingLeft: 20, paddingRight: 20
                        }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 23 }}>Business Permit</Text>
                        </View>

                        <View style={{
                            paddingTop: 20,
                            paddingLeft: 20, paddingRight: 20
                        }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 16, color: 'gray' }}>Upload your current county business permit</Text>
                        </View>

                        <View style={{
                            paddingTop: 20,
                            paddingLeft: 20,
                            paddingRight: 20
                        }}>
                            {
                                permit == null ? <View style={{ alignItems: 'center' }}>
                                    <Pressable onPress={pickPermit}>
                                        <View style={{
                                            borderWidth: 1,
                                            borderStyle: 'dashed',
                                            height: 80,
                                            width: width * 1 / 1.2,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            borderRadius: 10,
                                            borderColor: 'grey',
                                            flexDirection: 'row'
                                        }}>
                                            <Ionicons name="add" size={40} color='black' />
                                            <Text style={{
                                                fontFamily: 'Hindsemi',
                                                color: 'grey'
                                            }}>Permit</Text>
                                        </View>
                                    </Pressable>
                                    <View style={{ paddingTop: 20 }}>
                                        <Text style={{ fontFamily: 'Hindsemi' }}>Click card to upload</Text>
                                    </View>
                                </View>
                                    : <Card style={{
                                        borderWidth: 1,
                                        height: 200,
                                        width: width * 1 / 1.2,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                        <Image source={{ uri: permit }} style={{ width: 200, height: 200 }} />
                                    </Card>
                            }

                        </View>

                        <View style={{ alignItems: 'center' }}>
                            {
                                loading ? <View style={{ paddingTop: 20 }}><Spinner /></View> :
                                    <Button
                                        style={{
                                            width: width * 1 / 1.2,
                                            //padding: 8,
                                            marginTop: 20
                                        }}
                                        color='#3b3d3e'
                                        uppercase={false}
                                        labelStyle={{
                                            fontFamily: 'Osansbold'
                                         }}
                                        mode="contained"
                                        contentStyle={{
                                            height: 60
                                         }}
                                        onPress={
                                        () => {
                                            if (permitdone) {
                                                setSelected(3)
                                            }

                                            if (permitdone == false) {
                                                ToastAndroid.showWithGravityAndOffset(
                                                    'Please upload permit',
                                                    ToastAndroid.SHORT,
                                                    ToastAndroid.BOTTOM,
                                                    25,
                                                    50
                                                )
                                            }
                                        }
                                    }>Continue</Button>
                            }
                        </View>
                    </View>

                    {/*pagefour*/}
                    <View style={styles.container}>
                        <View
                            style={{
                                paddingTop: Constants.statusBarHeight,
                                alignItems: 'flex-start',
                            }}>

                        </View>
                        <View style={{
                            alignContent: 'flex-start',
                            width: width * 1 / 1.4,
                            //flexDirection: 'column',
                            paddingLeft: 20,
                            paddingRight: 20
                        }}>
                            <Pressable onPress={() => navigation.goBack()}>
                                <Ionicons name="arrow-back-sharp" color="black" size={33} />
                            </Pressable>
                        </View>
                        <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center', paddingTop: 20, width: width }}>
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#c4c4c4', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#c4c4c4', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#c4c4c4', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#f2f2f2', margin: 10 }} />

                        </View>

                        <View style={{
                            paddingTop: 20,
                            paddingLeft: 20, paddingRight: 20
                        }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 23 }}>Mpesa Statement</Text>
                        </View>

                        <View style={{
                            alignItems: 'center',
                            width: width + 10,
                            paddingTop: 20,
                            paddingLeft: 20, paddingRight: 20
                        }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 16, color: 'gray' }}>Upload a copy of your mpesa statement for the past 6 months</Text>
                        </View>

                        <View style={{
                            alignItems: 'center',
                            width: width + 10,
                            paddingTop: 20,
                            paddingLeft: 20, paddingRight: 20
                        }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 12, color: '#756e6e' }}>(Dial *234# from your mobile phone to access the menu, Select 'My M-PESA Information',then M-PESA Statement)</Text>
                        </View>


                        <View style={{
                            paddingTop: 20,
                            paddingLeft: 20,
                            paddingRight: 20,
                            width: width + 10
                        }}>
                            {
                                mpesa == null ? <View style={{ alignItems: 'center' }}>
                                    <Pressable onPress={pickMpesa}>
                                        <View style={{
                                            borderWidth: 1,
                                            borderStyle: 'dashed',
                                            height: 80,
                                            width: width * 1 / 1.2,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            borderRadius: 10,
                                            borderColor: 'grey',
                                            flexDirection: 'row'
                                        }}>
                                            <Ionicons name="add" size={40} color='black' />
                                            <Text style={{
                                                fontFamily: 'Hindsemi',
                                                color: 'grey'
                                            }}>Permit</Text>
                                        </View>
                                    </Pressable>
                                    <View style={{ paddingTop: 20 }}>
                                        <Text style={{ fontFamily: 'Hindsemi' }}>Click card to upload</Text>
                                    </View>
                                </View>
                                    : <Card style={{
                                        borderWidth: 1,
                                        height: 200,
                                        width: width * 1 / 1.2,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                        <Text style={{ fontFamily: 'Osans' }}>Document Uploaded</Text>
                                    </Card>
                            }

                        </View>

                        <View style={{ alignItems: 'center', width: width + 10 }}>
                            {
                                loading ? <View style={{ paddingTop: 20 }}><Spinner /></View> :
                                    <Button
                                        style={{
                                            width: width * 1 / 1.2,
                                            //padding: 8,
                                            marginTop: 20
                                        }}
                                        color='#3b3d3e'
                                        labelStyle={{
                                            fontFamily: 'Osansbold'
                                         }}
                                        uppercase={false}
                                        contentStyle={{
                                            height: 60
                                         }}
                                        mode="contained"
                                        onPress={
                                        () => {
                                            if (mpesadone) {
                                                setSelected(4)
                                            }

                                            if (mpesadone == false) {
                                                ToastAndroid.showWithGravityAndOffset(
                                                    'Please Mpesa',
                                                    ToastAndroid.SHORT,
                                                    ToastAndroid.BOTTOM,
                                                    25,
                                                    50
                                                )
                                            }
                                        }
                                    }>Continue</Button>
                            }
                        </View>

                    </View>


                    {/*pagefive*/}
                    <View style={styles.container}>
                        <View
                            style={{
                                width: width + 10,
                                paddingTop: Constants.statusBarHeight,
                                alignItems: 'flex-start',
                            }}>

                        </View>
                        <View style={{
                            alignContent: 'flex-start',
                            width: width + 10,
                            //flexDirection: 'column',
                            paddingLeft: 20,
                            paddingRight: 20
                        }}>
                            <Pressable onPress={() => navigation.goBack()}>
                                <Ionicons name="arrow-back-sharp" color="black" size={33} />
                            </Pressable>
                        </View>
                        <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'center', paddingTop: 20, width: width }}>
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#c4c4c4', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#c4c4c4', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#c4c4c4', margin: 10 }} />
                            <View style={{ height: 12, borderRadius: 20, width: 40, backgroundColor: '#c4c4c4', margin: 10 }} />

                        </View>

                        <View style={{
                            alignItems: 'center',
                            width: width + 10,
                            paddingTop: 20,
                            paddingLeft: 20, paddingRight: 20
                        }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 23 }}>Other Documents (Optional)</Text>
                        </View>

                        <View style={{
                            alignItems: 'center',
                            width: width + 10,
                            paddingTop: 20,
                            paddingLeft: 40, paddingRight: 20
                        }}>
                            <Text style={{ fontFamily: 'Osans', fontSize: 16, color: 'gray' }}>Upload other documents specific to your business like ERC licence.. e.t.c</Text>
                        </View>

                        <View style={{ paddingTop: 20 }}>
                            <FlatList
                                data={bugs}
                                horizontal
                                renderItem={
                                    (item) => (
                                        <Chip mode='outlined' style={{ backgroundColor: 'white', borderWidth: 1, height: 40, alignItems: 'center' }} ><Text style={{ color: 'black', fontFamily: 'Osans' }}>{item.item.name}</Text></Chip>
                                    )
                                }
                            ></FlatList>
                        </View>


                        <View style={{
                            alignItems: 'center',
                            width: width + 10,
                            paddingTop: 20,
                            paddingLeft: 20,
                            paddingRight: 20
                        }}>
                            <View style={{ alignItems: 'center' }}>
                                <Pressable onPress={pickOther}>
                                    <View style={{
                                        borderWidth: 1,
                                        borderStyle: 'dashed',
                                        height: 80,
                                        width: width * 1 / 1.2,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: 10,
                                        borderColor: 'grey',
                                        flexDirection: 'row'
                                    }}>
                                        <Ionicons name="add" size={40} color='black' />
                                        <Text style={{
                                            fontFamily: 'Hindsemi',
                                            color: 'grey'
                                        }}>Other</Text>
                                    </View>
                                </Pressable>
                                <View style={{ paddingTop: 20 }}>
                                    <Text style={{ fontFamily: 'Hindsemi' }}>Click card to upload</Text>
                                </View>
                            </View>

                        </View>

                        <View style={{ alignItems: 'center', width: width + 10 }}>
                            {
                                loading ? <View style={{ paddingTop: 20 }}><Spinner /></View> :
                                    <Button
                                        loading={isloading}
                                        style={{
                                            width: width * 1 / 1.2,
                                            //padding: 8,
                                            marginTop: 20
                                        }}
                                        color='#3b3d3e'
                                        labelStyle={{
                                            fontFamily: 'Osansbold'
                                         }}
                                        uppercase={false}
                                        mode="contained"
                                        contentStyle={{
                                            height: 60
                                         }}
                                        onPress={
                                            () => {
                                            setIsloading(true)

                                            dispatch({
                                                type: "ADD_NEWBIE",
                                                payload: false
                                            })

                                                navigation.navigate('Success')
                                                setIsloading(false)
                                        }
                                    }>Save Details</Button>
                            }
                        </View>

                    </View>





                </ViewPager>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        //padding: 0,
        //alignItems: 'center',

    },

})