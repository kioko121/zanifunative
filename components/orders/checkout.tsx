import * as React from 'react';
import {
    View,
    StyleSheet,
    TouchableNativeFeedback,
    ToastAndroid,
    ScrollView,
    Image,
    ActivityIndicator,
} from 'react-native';
import Constants from 'expo-constants';
import { fireman } from '../../api/base';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, Button, Layout, Text, Input, Icon, Select, SelectItem, Card, Spinner } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import { Divider } from '@ui-kitten/components';
import { grey100 } from 'react-native-paper/lib/typescript/styles/colors';
import NumericInput from 'react-native-numeric-input'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import RBSheet from "react-native-raw-bottom-sheet";

export default function Checkout({navigation}){
    let [fontsLoaded] = useFonts({
        'Hindbold': require('../../assets/HindSiliguri-Bold.ttf'),
        'Hindregular': require('../../assets/HindSiliguri-Regular.ttf'),
        'Hindmedium': require('../../assets/HindSiliguri-Medium.ttf'),
        'Hindsemi': require('../../assets/HindSiliguri-SemiBold.ttf'),
    })

    if (!fontsLoaded) {
        return (<View style={styles.container}>
            <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                <LottieView
                    autoPlay
                    style={{ width: 800, height: 800 }}
                    source={require('../../assets/9305-loading-bloob.json')}
                />
            </View>
        </View>
        )
    }else{
        return (

            <ApplicationProvider {...eva} theme={eva.light}>
                <RBSheet
                        ref={refCheck}
                        height={400}
                        closeOnDragDown={true}
                        closeOnPressMask={false}
                        customStyles={{
                            wrapper: {
                                backgroundColor: "#00000023",
                            },
                            draggableIcon: {
                                backgroundColor: "#000"
                            }
                        }}
                    ></RBSheet>
            </ApplicationProvider>
        )
    }
}