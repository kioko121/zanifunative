import * as React from 'react'
import {
  ImageBackground,
  View,
  StyleSheet,
  ActivityIndicator,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TouchableNativeFeedback,
  FlatList,

} from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, Button, Layout, Text, Input, Icon, Select, SelectItem, Card } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import PinInput from 'react-pin-input';
import firebase from 'firebase';
import { fireman } from '../../api/base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { ToastAndroid } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import kakitu from '../../globals'
import { Spinner } from '@ui-kitten/components';
import Ionicons from '@expo/vector-icons/build/Ionicons';
import { logger } from "react-native-logs"
import { useSelector } from 'react-redux';
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window")

export default function Myorder({ route, navigation }) {


  const { itemid } = route.params;
  const key = useSelector((state) => state.key)
  const log = logger.createLogger()

  const [order, setOrder] = useState([])
  const [loaded, setLoaded] = useState(false)

  const [id, setId] = useState()
  const [status, setStatus] = useState()
  const [count, setCount] = useState()
  const [date, setDate] = useState()
  const [amount, setAmount] = useState()
  const [products, setProducts] = useState()


  const getMyorder = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", `Bearer ${key}`);

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch(`https://development.zanifu.com/api/carts/${itemid}`, requestOptions)
      .then(response => {
        var status = response.status
        log.debug(status)
        if (status == 200) {
          response.text().then((data) => {
            var vitu = JSON.parse(data)

            log.debug(vitu.data.products)
            setOrder(vitu.data)
            setId(vitu.data.id)
            setCount(vitu.data.products_count)
            setDate(vitu.data.created_at)
            setAmount(vitu.data.amount)
            setProducts(vitu.data.products)

            setLoaded(true)
          })
        }
      })
      .catch(error => console.log('error', error));
  }

  useEffect(() => {
    getMyorder()
  }, [])



  return (
    <View style={styles.container}>
      <TouchableNativeFeedback onPress={() => navigation.goBack()}>
        <Ionicons name="arrow-back-sharp" color="black" size={33} />
      </TouchableNativeFeedback>

      {
        !loaded ? <View style={{ alignItems: 'center', alignContent: 'center' }}>
          <Spinner />
        </View> :
          < View style={{ paddingTop: 20, flexDirection: 'column' }}>

            <View>
              <Text style={{ fontFamily: 'Open Sans Bold', color: 'black', fontSize: 23 }}>{`Order ID: ${id}`}</Text>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', alignContent: 'center', paddingTop: 20 }}>
              <Text style={{ fontFamily: 'Open Sans Bold', color: 'black' }}>
                {status ? status : "On going"}
              </Text>
              <View style={{ height: 4, width: 4, marginLeft: 10, marginRight: 10, backgroundColor: 'black', borderRadius: 50 }} />
              <Text style={{  color: 'gray' }}>
                {date}
              </Text>
            </View>

            <View style={{ paddingTop: 20}}>
            <Text style={{ fontFamily: 'Open Sans SemiBold', color: 'black', fontSize: 24, paddingTop: height * 1/ 18}}>Your Items</Text>
            </View>

          {/*item flatlist */}
          <View style={{ paddingTop: 40}}>
          <FlatList
          data={products}
          renderItem={
            (item) => (
              <View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 10}}>
              <Text style={{ color: 'gray', fontSize: 18}}>{item.item.title ? item.item.title  : "item name 404" }</Text>
              <Text style={{ fontFamily: 'Open Sans Bold', fontSize: 18, color: 'black'}}>{item.item.price ? `KES ${item.item.price}`  : "item price 404" }</Text>
              </View>


              </View>
            )
          } />

          <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingLeft: width * 1/6, paddingTop: 40}}>
          <Text style={{  color: 'gray', fontSize: 22}}> Total</Text>
          <Text style={{ fontFamily: 'Open Sans SemiBold', color: 'black', fontSize: 22}}>{`KES ${amount}`}</Text>
          </View>
          </View>
          </View>

      }
    </View>

  )
}

const styles = StyleSheet.create({
  container: {
    fontFamily: 'Open Sans Regular',
    flex: 1,
    //justifyContent: 'center',
    paddingTop: Constants.statusBarHeight * 2,
    backgroundColor: '#fff',
    padding: 8,
    paddingLeft: 20,
    paddingRight: 20
  },
  container2: {
    fontFamily: 'Inter_900Black',
    flex: .5,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
  elect: {

  },
  fab: {
    position: 'absolute',
    margin: 6,
    right: 0,
    bottom: 10,
  },
  indicator: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomNavigationView: {
    backgroundColor: '#fff',
    width: '100%',
    height: 250,
    justifyContent: 'center',
    alignItems: 'center',
  }
});
