import * as React from 'react';
import {
    View,
    StyleSheet,
    TouchableNativeFeedback,
    ToastAndroid,
    ScrollView,
    FlatList,
    Image,
    ActivityIndicator,
    RefreshControl,
    Picker,
    TextInput,
    Pressable,
    ImageBackground
} from 'react-native';
import Constants from 'expo-constants';
import { fireman } from '../../api/base';
import { useState, useEffect, useRef, } from 'react'
import {
    ApplicationProvider,
    Button,
    Layout,
    Text,
    Card,
    RadioGroup
} from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import { Divider } from '@ui-kitten/components';

import NumericInput from 'react-native-numeric-input'

import { MaterialCommunityIcons } from '@expo/vector-icons'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import RBSheet from "react-native-raw-bottom-sheet";

import { RadioButton } from 'react-native-paper';
import { Searchbar } from 'react-native-paper';
import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window")
import { Chip } from 'react-native-paper';
import { BlurView } from 'expo-blur';
import { useDispatch, useSelector } from 'react-redux';
import { logger } from "react-native-logs"



export default function Homepage({ navigation }) {
    let [fontsLoaded] = useFonts({
        //'Hindbold': require('../../assets/HindSiliguri-Bold.ttf'),
        'Hindregular': require('../../assets/HindSiliguri-Regular.ttf'),
        'Hindmedium': require('../../assets/HindSiliguri-Medium.ttf'),
        'Hindsemi': require('../../assets/HindSiliguri-SemiBold.ttf'),
    })

    const [productsdata, setProducts] = useState([])
    const [cart, setCart] = useState([])
    const refRBSheet = useRef();
    const refCheck = useRef()
    const refPaymethod = useRef()
    const refMpesa = useRef()
    const [gocheck, setcheckout] = useState(false)
    const [visible, setVisible] = useState(false);
    const [checked, setChecked] = useState('first')
    const [nihowmuch, setNihowmuch] = useState()
    const [categories, setCategories] = useState()
    const log = logger.createLogger()

    const chosen = useSelector((state) => state.chosen)
    const newbie = useSelector((state) => state.newbie)
    const key = useSelector((state) => state.key)

    const toggleBottomNavigationView = () => {
        //Toggling the visibility state of the bottom sheet
        setVisible(!visible);
    };


    const getItems = async () => {

        try {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            myHeaders.append("Accept", "application/json");
            myHeaders.append("Authorization", `Bearer ${key}`);

            var requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };

            fetch("https://development.zanifu.com/api/products", requestOptions)
                .then(response => {
                    var status = response.status


                    response.text().then((data) => {
                        log.debug(data)
                    })
                })
                .catch(error => console.log('error', error));
        } catch (err) {
            log.error(err)
        }
    }

    const getCategories = async () => {
        try {
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            var requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow'
            };

            fetch("https://development.zanifu.com/api/categories", requestOptions)
                .then(response => response.text())
                .then(result => {
                    var first = JSON.parse(result)
                    setCategories(first["data"])

                    //setCategories(JSON.parse(result))
                    //console.log(categories)
                })
                .catch(error => console.log('error', error));
        } catch (err) {
            log.debug("at categories")
            log.error(err)
        }
    }

    const goCheckout = () => {
        setcheckout(true)
    }

    const getProducts = async () => {
        try {
            var ref = fireman.database().ref('/products')
            ref.once('value', function (snapshot) {
                var vitu = []
                snapshot.forEach(function (childSnapshot) {
                    var key = childSnapshot.key
                    var data = childSnapshot.val()

                    vitu.push({ key: key, name: data.name, price: data.price, pic: data.pic })
                })
                setProducts(vitu)
                console.log(productsdata)
            })
        } catch (err) {
            log.error(err)
        }
    }

    const getCart = async () => {
        try {
            var ninja = 0
            var things = []
            var username = fireman.auth().currentUser?.displayName
            var ref = fireman.database().ref(`/carts/${username}`)
            ref.on('value', (snapshot) => {
                snapshot.forEach(function (childSnapshot) {
                    var key = childSnapshot.key
                    var data = childSnapshot.val()

                    var mig = data.price * data.quantity

                    var kis = ninja + mig

                    setNihowmuch(kis)

                    things.push({ name: data.name, price: data.price, quantity: data.quantity })
                })
            })

            setCart(things)
            //console.log("*******************************")
            //console.log(things)

            //console.log(cart)
        } catch (err) {
            log.error(err)
        }
    }

    const onRefresh = () => {

        getItems()
        getCategories()
        getProducts()
    }

    useEffect(() => {
        getItems()
        getCategories()
        getProducts()
        getCart()

    }, [])


    if (!fontsLoaded) {
        return (<View style={styles.container}>
            <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                <LottieView
                    autoPlay
                    style={{ width: 800, height: 800 }}
                    source={require('../../assets/9305-loading-bloob.json')}
                />
            </View>
        </View>
        )
    }
    else {
        return (

            <ScrollView
                style={styles.container}
                refreshControl={
                    <RefreshControl  onRefresh={() => onRefresh} />
                }
            >

                {/*sheet1*/}
                <RBSheet
                    ref={refRBSheet}
                    height={500}
                    closeOnDragDown={true}
                    closeOnPressMask={false}
                    customStyles={{
                        wrapper: {
                            backgroundColor: "#00000023",
                        },
                        draggableIcon: {
                            backgroundColor: "#000"
                        }
                    }}
                >
                    <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                        <Text style={{ fontFamily: 'Hindsemi', fontSize: 20 }}>Your Cart</Text>
                        <View>
                            {
                                cart.map((item) => (
                                    <View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 20 }}>
                                            <Text style={{ fontFamily: 'Hindregular', fontSize: 17 }}>{item.name}</Text>
                                            <Text style={{ fontFamily: 'Hindregular', fontSize: 17 }}> {`KES ${item.price}`}</Text>
                                        </View>
                                        <View style={{ paddingTop: 10 }}>
                                            <NumericInput
                                                rounded
                                                totalWidth={80}
                                                value={item.quantity}
                                                onChange={
                                                    value => {
                                                        var user = fireman.auth().currentUser?.displayName

                                                        var ref = fireman.database().ref(`carts/${user}`)
                                                        ref.child(item.name).set({
                                                            //'name': item.name,
                                                            'price': item.price,
                                                            'quantity': value,
                                                            'name': item.name
                                                        })

                                                    }
                                                } />
                                        </View>
                                        <View style={{ paddingTop: 10, paddingBottom: 10 }}>
                                            <Divider />
                                        </View>
                                    </View>
                                ))
                            }
                        </View>
                        <View style={{ paddingTop: 10, justifyContent: 'space-between', flexDirection: 'row' }}>
                            <Text style={{ fontFamily: 'Hindsemi' }}>Subtotal</Text>
                            {/*<Text style={{ fontFamily: 'Hindsemi' }}>{`KES 32,000`}</Text> */}
                            <Text style={{ fontFamily: 'Hindsemi' }}>{`KES ${nihowmuch}`}</Text>
                        </View>
                        <View style={{ paddingTop: 20 }}>
                            <Button style={{
                                backgroundColor: '#292929'
                            }}
                                onPress={() => {
                                    //getCart()

                                    goCheckout()
                                    refRBSheet.current.close()
                                    refCheck.current.open()
                                }}>
                                <Text style={{ fontFamily: 'Hindsemi', fontSize: 16 }}>
                                    Go to checkout
                                </Text>
                            </Button>
                        </View>
                    </View>
                </RBSheet>

                {/*sheet2*/}
                <RBSheet
                    ref={refCheck}
                    height={500}
                    closeOnDragDown={true}
                    closeOnPressMask={false}
                    customStyles={{
                        wrapper: {
                            backgroundColor: "#00000023",
                        },
                        draggableIcon: {
                            backgroundColor: "#000"
                        }
                    }}
                >
                    <View style={{ justifyContent: 'center', paddingRight: 20, paddingLeft: 20 }}>
                        <MaterialCommunityIcons name="arrow-left" color='black' size={20} />
                        <View style={{ paddingTop: 10 }}>
                            <Text style={{ fontFamily: 'Hindsemi', fontSize: 25 }}>Checkout</Text>
                        </View>

                        <View style={{ paddingTop: 10 }}>
                            <Text style={{ fontFamily: 'Hindregular', fontSize: 20, color: 'black' }}>Your Items</Text>

                            <View>
                                {
                                    cart.map((item) => (
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 7 }}>
                                            <View style={{
                                                width: 30,
                                                height: 30,
                                                backgroundColor: '#e9e9e9',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                borderRadius: 5
                                            }}>
                                                <Text style={{ fontFamily: 'Hindsemi' }}>{item.quantity}</Text>
                                            </View>
                                            <View>
                                                <Text style={{ fontFamily: 'Hindsemi' }}>{item.name}</Text>
                                            </View>
                                            <View>
                                                <Text style={{ fontFamily: 'Hindsemi' }}>{`KES ${item.price}`}</Text>
                                            </View>
                                        </View>
                                    ))
                                }
                            </View>
                            <View>
                                <TouchableWithoutFeedback>
                                    <View style={{ borderRadius: 20, backgroundColor: '#e9e9e9', flexDirection: 'row', width: 80, height: 30, alignItems: 'center', justifyContent: 'center', marginTop: 20, marginBottom: 20 }}>
                                        <MaterialCommunityIcons name="plus" color='black' size={16} />
                                        <Text style={{ fontFamily: 'Hindsemi', fontSize: 10 }}>
                                            Add Items
                                        </Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingTop: 20, paddingBottom: 20 }}>
                                <Text style={{ fontFamily: 'Hindsemi', fontSize: 18 }}>Total</Text>
                                <Text style={{ fontFamily: 'Hindsemi', fontSize: 18 }}>{` KES ${nihowmuch}`}</Text>
                            </View>


                            <View>
                                <Button style={{ backgroundColor: '#292929' }} onPress={
                                    () => {
                                        refCheck.current.close()
                                        refPaymethod.current.open()
                                    }
                                }>
                                    <Text>Payment Method</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </RBSheet>

                {/*sheet3*/}
                <RBSheet
                    ref={refPaymethod}
                    height={500}
                    closeOnDragDown={true}
                    closeOnPressMask={false}
                    customStyles={{
                        wrapper: {
                            backgroundColor: "#00000023",
                        },
                        draggableIcon: {
                            backgroundColor: "#000"
                        }
                    }}
                >
                    <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                        <View style={{ paddingTop: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ fontFamily: 'Hindsemi', fontSize: 18 }}>Select how you want to pay</Text>
                            <MaterialCommunityIcons name="window-close" color='black' size={20} />
                        </View>
                    </View>

                    <View>
                        <RadioGroup>
                            <View style={{
                                height: 100,
                                borderStyle: 'solid',
                                borderWidth: 1,
                                marginLeft: 20,
                                marginRight: 20,
                                borderRadius: 10,
                                borderColor: '#a5a5a5',
                                padding: 10,
                                justifyContent: 'center'
                            }}>
                                <Text style={{ fontFamily: 'Hindsemi' }}>Zanifu Credit</Text>
                                <View style={{ flexDirection: 'row', paddingRight: 20, justifyContent: 'space-between' }}>

                                    <View style={{ width: 300 }}>
                                        <Text style={{ fontFamily: 'Hindregular', color: 'grey' }}>Pay with Zanfiu credit and get a loan to pay for your stock</Text>
                                    </View>
                                    <RadioButton
                                        onPress={() => {
                                            setChecked('first')
                                        }}
                                        status={checked === 'first' ? 'checked' : 'unchecked'}
                                    ></RadioButton>

                                </View>

                            </View>

                            <View style={{
                                height: 100,
                                borderStyle: 'solid',
                                borderWidth: 1,
                                marginLeft: 20,
                                marginRight: 20,
                                borderRadius: 10,
                                borderColor: '#a5a5a5',
                                padding: 10,
                                justifyContent: 'center'
                            }}>
                                <Text style={{ fontFamily: 'Hindsemi' }}>Mpesa</Text>
                                <View style={{ flexDirection: 'row', paddingRight: 20, justifyContent: 'space-between' }} >

                                    <Text style={{ fontFamily: 'Hindregular', color: 'grey' }}>Pay safely with mpesa</Text>
                                    <RadioButton
                                        onPress={() => {
                                            setChecked('second')
                                            refPaymethod.current.close()
                                            refMpesa.current.open()
                                        }}
                                        status={checked === 'second' ? 'checked' : 'unchecked'}
                                    ></RadioButton>

                                </View>

                            </View>


                            <View style={{
                                height: 100,
                                borderStyle: 'solid',
                                borderWidth: 1,
                                marginLeft: 20,
                                marginRight: 20,
                                borderRadius: 10,
                                borderColor: '#a5a5a5',
                                padding: 10,
                                justifyContent: 'center'
                            }}>
                                <Text style={{ fontFamily: 'Hindsemi' }}>Pay on Delivery</Text>
                                <View style={{ flexDirection: 'row', paddingRight: 20, justifyContent: 'space-between' }}>

                                    <View style={{ width: 300 }}>
                                        <Text style={{ fontFamily: 'Hindregular', color: 'grey' }}>Pay when stock is delivered</Text>
                                    </View>
                                    <RadioButton
                                        onPress={() => {
                                            setChecked('third')
                                        }}
                                        status={checked === 'third' ? 'checked' : 'unchecked'}
                                    ></RadioButton>

                                </View>

                            </View>

                        </RadioGroup>
                    </View>

                </RBSheet>

                {/*sheet4*/}
                <RBSheet
                    ref={refMpesa}
                    height={500}
                    closeOnDragDown={true}
                    closeOnPressMask={false}
                    customStyles={{
                        wrapper: {
                            backgroundColor: "#00000023",
                        },
                        draggableIcon: {
                            backgroundColor: "#000"
                        }
                    }}
                >
                    <View style={{ paddingLeft: 20, paddingRight: 20 }}>
                        <View style={{ marginTop: 20 }}>
                            <Text style={{
                                fontFamily: 'Hindsemi',
                                color: '#6f6f6f',
                                fontSize: 17
                            }}>
                                How do you want to pay?
                            </Text>
                            <View style={{
                                borderWidth: 1,
                                //backgroundColor: 'black',
                                //borderStyle: 'solid',
                                //borderColor: 'black',
                                paddingRight: 0,
                                borderRadius: 5,
                                marginTop: 20
                            }}>
                                <Picker
                                    style={{ borderColor: '#2c2c2c', borderWidth: 2, height: 50 }}
                                >
                                    <Picker.Item label="Mpesa" value="mpesa" />
                                    <Picker.Item label="Mpessa" value="mpesad" />
                                </Picker>
                            </View>
                        </View>

                        {
                            checked === "second" ?
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{
                                        fontFamily: 'Hindsemi',
                                        color: '#6f6f6f',
                                        fontSize: 17
                                    }}>
                                        Enter the till number to pay
                                    </Text>
                                    <View style={{
                                        borderWidth: 1,
                                        //backgroundColor: 'black',
                                        //borderStyle: 'solid',
                                        //borderColor: 'black',
                                        paddingRight: 0,
                                        borderRadius: 5,
                                        marginTop: 20
                                    }}>
                                        <TextInput
                                            style={{ height: 50, padding: 5, fontFamily: 'Hindregular' }}
                                            placeholder="Till no"
                                        //onChangeText={onChangeText}
                                        //value={text}
                                        />
                                    </View>
                                </View>
                                : null
                        }

                        <View style={{ paddingTop: 30, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ fontFamily: 'Hindsemi', fontSize: 20 }}>Total</Text>
                            <Text style={{ fontFamily: 'Hindsemi', fontSize: 20 }}>{`KES ${nihowmuch}`}</Text>
                        </View>
                        <View style={{ paddingTop: 10 }}>
                            <Button style={{ backgroundColor: '#292929' }} onPress={() => {
                                ToastAndroid.showWithGravityAndOffset(
                                    `work in progress, still under construction`,
                                    ToastAndroid.SHORT,
                                    ToastAndroid.BOTTOM,
                                    25,
                                    50
                                )

                                /*var ref = fireman.database().ref('orders')
                                ref.push({
                                    'price': 23333,
                                    'quantity': 4,
                                    'time': new Date().toLocaleString()
                                })*/
                                var simu = fireman.auth().currentUser?.phoneNumber
                                console.log(simu)

                                var ref = fireman.database().ref(`orders/${simu}`)

                                ref.push({
                                    'user': simu,
                                    time: new Date().toLocaleTimeString(),
                                    'cart': cart,
                                    total: nihowmuch
                                })


                            }}>
                                <Text style={{ fontFamily: 'Hindsemi' }}>Place Order</Text>
                            </Button>
                        </View>
                    </View>

                </RBSheet>

                <View style={{ paddingTop: 40, paddingLeft: 20 }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between'
                        }}>
                        <Searchbar
                            iconColor="black"
                            inputStyle={{
                                fontFamily: 'Hindregular',
                                fontSize: 15
                            }}
                            style={{
                                backgroundColor: "#f1f1f1",
                                fontFamily: 'Hindregular',
                                fontSize: 15,
                                borderColor: "black",
                                borderWidth: 0,
                                width: width * 1 / 1.4,
                                borderRadius: 15,
                                elevation: 0
                            }}
                            placeholder="Search"
                        ></Searchbar>
                        <Pressable
                            onPress={
                                () => getCategories()
                            }>
                            <View
                                style={{
                                    borderRadius: 50,
                                    width: 50,
                                    height: 50,
                                    backgroundColor: "#767384",
                                    alignContent: 'center',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    flex: 1
                                }}>
                                <MaterialCommunityIcons name="google-circles-extended" color='white' size={20} />
                            </View>
                        </Pressable>
                    </View>

                    <View
                        style={{
                            width: width * 1 / 1.2,
                            backgroundColor: "#460898",
                            borderRadius: 20,
                            marginTop: 20,
                            marginRight: 20

                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                            <LottieView
                                autoPlay
                                style={{ width: 150, height: 150 }}
                                source={require('../../assets/4946-coin.json')}
                            />
                            <View>
                                <Text style={{ fontFamily: 'Hindregular', color: '#fff' }}>Your loan limit is</Text>
                                <Text style={{ fontFamily: 'Hindbold', color: '#fff' }}>KES 4200</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ paddingTop: 20, paddingBottom: 20 }}>
                        <Text style={{ fontFamily: 'Hindbold', fontSize: 16 }}>Categories</Text>
                    </View>

                    <View>
                        {
                            /*
                        categories["data"].map((item) => (
                            <Text>{item.name}</Text>
                        ))
                        */
                        }

                        {categories == null ? <Text>Loading</Text> :
                            <FlatList
                                keyExtractor={(item, index) => index.toString()}
                                horizontal
                                data={categories}
                                renderItem={
                                    (item) => (
                                        <Chip
                                            style={{
                                                margin: 8,
                                                backgroundColor: "#fff",
                                                elevation: 0,
                                                borderWidth: 1,
                                                borderColor: "#d8d8d8"
                                            }}
                                            icon="information"
                                            onPress={() => console.log(item.item.name)}>
                                            <Text>{item.item.name}</Text>
                                        </Chip>
                                    )
                                }

                            />}
                    </View>

                    <View
                        style={{
                            paddingTop: 20,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center'
                        }}
                    >
                        <Text style={{ fontFamily: 'Hindbold', fontSize: 16 }}>All Products</Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Hindsemi', fontSize: 14, color: "grey" }}>
                                Sort by :
                            </Text>
                            <Picker
                                //selectedValue={selectedValue}

                                itemStyle={{ fontSize: 10, fontFamily: 'Hindbold' }}
                                style={{ width: 100, fontSize: 10 }}
                                mode="dropdown"
                            //onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
                            >
                                <Picker.Item label="Latest" value="java" />
                                <Picker.Item label="JavaScript" value="js" />
                            </Picker>
                        </View>
                    </View>

                    <View
                        style={{
                            width: width
                        }}
                    >
                        {
                            <FlatList
                                numColumns={2}

                                data={productsdata}
                                renderItem={
                                    (item) => (
                                        <ImageBackground
                                            imageStyle={{ borderRadius: 10 }}
                                            resizeMode='cover'
                                            style={{
                                                width: 140,
                                                borderRadius: 20,
                                                height: 140,
                                                margin: 8
                                            }} source={{ uri: item.item.pic }}>
                                            <BlurView
                                                tint="light"
                                                intensity={0}
                                                style={{
                                                    right: 10,
                                                    top: 10,
                                                    borderRadius: 5,
                                                    position: 'absolute'
                                                }}
                                            >
                                                <MaterialCommunityIcons name="heart-outline" color='black' size={20} />
                                            </BlurView>
                                            <BlurView
                                                tint='light'
                                                intensity={100}
                                                style={{
                                                    position: 'absolute',
                                                    borderRadius: 0,
                                                    justifyContent: 'space-between',
                                                    alignItems: 'center',
                                                    paddingLeft: 10,
                                                    paddingRight: 10,
                                                    paddingBottom: 10,
                                                    //width: 80,
                                                    bottom: 0,
                                                    left: 0,
                                                    right: 0,
                                                    backgroundColor: "white",
                                                    flexDirection: 'row'
                                                }}>
                                                <View style={{ flexDirection: 'column' }}>
                                                    <Text style={{
                                                        fontFamily: 'Hindregular',
                                                        color: 'black',
                                                        fontWeight: 'bold',
                                                        fontSize: 15
                                                    }}>{item.item.name}</Text>
                                                    <Text style={{
                                                        fontFamily: 'Hindbold',
                                                        color: 'grey',
                                                        fontWeight: 'bold',
                                                        fontSize: 11
                                                    }}>{`KES  ${item.item.price}`}</Text>
                                                </View>
                                                <View
                                                    style={{
                                                        backgroundColor: '#fff',
                                                        borderRadius: 20
                                                    }}
                                                >
                                                    <NumericInput
                                                        rounded
                                                        totalWidth={80}
                                                        onChange={
                                                            value => {
                                                                var user = fireman.auth().currentUser?.displayName

                                                                var ref = fireman.database().ref(`carts/${user}`)
                                                                ref.child(item.name).set({
                                                                    //'name': item.name,
                                                                    'price': item.price,
                                                                    'quantity': value,
                                                                    'name': item.name
                                                                })

                                                                ToastAndroid.showWithGravityAndOffset(
                                                                    `${item.name} was added to cart`,
                                                                    ToastAndroid.SHORT,
                                                                    ToastAndroid.BOTTOM,
                                                                    25,
                                                                    50
                                                                )
                                                            }
                                                        } />
                                                </View>
                                            </BlurView>
                                        </ImageBackground>
                                    )
                                }
                            />

                        }
                    </View>
                </View>

                <View style={{ position: 'absolute', bottom: 20, left: 30, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ position: 'relative', justifyContent: 'center', alignItems: 'center' }}>
                        <TouchableWithoutFeedback onPress={() => {
                            getCart()
                            console.log('pressed')
                            refRBSheet.current.open()

                        }}>
                            <Card style={{ width: 350, backgroundColor: 'black', display: 'none' }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ fontFamily: 'Hindregular', color: '#fff' }}>My cart</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontFamily: 'Hindregular', color: '#fff' }}>{`Kes ${nihowmuch}`}</Text>
                                        <MaterialCommunityIcons name="forward" color='white' size={20} />
                                    </View>
                                </View>
                            </Card>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </ScrollView>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        padding: 8,
    },
    container2: {
        fontFamily: 'Inter_900Black',
        flex: .5,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },

    select: {

    },
    fab: {
        position: 'absolute',
        margin: 6,
        right: 0,
        bottom: 10,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    bottomNavigationView: {
        backgroundColor: '#fff',
        width: '100%',
        height: 250,
        justifyContent: 'center',
        alignItems: 'center',
    }
});
