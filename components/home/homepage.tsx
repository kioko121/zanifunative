import * as React from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    Text,
    Pressable,
    Vibration,
    RefreshControl,
    BackHandler,
    Modal,
    TouchableOpacity,
    StatusBar
} from 'react-native';
import Constants from 'expo-constants';
import { fireman } from '../../api/base';
import { useState, useEffect, useRef, } from 'react'

import { useFonts } from '@expo-google-fonts/inter';

import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window")

import { useDispatch, useSelector } from 'react-redux';
import { logger } from "react-native-logs"
import { Ionicons } from '@expo/vector-icons';
import NumericInput from 'react-native-numeric-input';
import Animated, { useSharedValue, useAnimatedStyle, withSpring } from 'react-native-reanimated';

import { Spinner } from '@ui-kitten/components';

import { LogBox } from 'react-native';



function wait(ms) {
    var start = new Date().getTime();
    var end = start;
    while (end < start + ms) {
        end = new Date().getTime();
    }
}

export default function Homepage({ navigation }) {

    const [productsdata, setProducts] = useState([])

    const [allproducts, setAllproducts] = useState([])

    const [loading, setLoading] = useState(true)
    const [count, setCount] = useState(0)
    const [iscart, setIscart] = useState(false)


    const key = useSelector((state) => state.key)
    const phone = useSelector((state) => state.phone)
    const noob = useSelector((state) => state.newbie)

    const log = logger.createLogger()

    const loaded = true
    const dispatch = useDispatch()

    const offset = useSharedValue(0);
    const rotation = useSharedValue(0);
    const color = useSharedValue('white')

    const reducer = (accumulator, curr) => accumulator + curr;


    var tut = []
    const tutfull = []



    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getItems()
        getProducts()
        getCartItems()
        setRefreshing(false)
    }, []);



    function handleBackButtonClick() {
        navigation.navigate('Home');
        return true;
    }



    const getCartItems = async () => {
        try {

            var things = []

            var ref = fireman.database().ref(`/cartitems/${phone}`)
            ref.once('value', (snapshot) => {

                setCount(snapshot.numChildren())

            })


        } catch (err) {
            log.error(err)
        }
    }

    const getItems = async () => {
        setIscart(false)
        setLoading(true)
        try {
            if (key) {
                var myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                myHeaders.append("Accept", "application/json");
                myHeaders.append("Authorization", `Bearer ${key}`);

                var requestOptions = {
                    method: 'GET',
                    headers: myHeaders,
                    redirect: 'follow'
                };

                fetch("https://development.zanifu.com/api/products", requestOptions)
                    .then(response => {
                        var status = response.status
                        log.info(status)
                        response.text().then((data) => {
                            var vitu = JSON.parse(data)
                            //log.debug(vitu.data)
                            setAllproducts(vitu.data)
                            setLoading(false)
                        })
                    })
                    .catch(error => console.log('error', error));
            } else {
                console.log('key not set')
            }
        } catch (err) {
            log.error(err)
        }
    }



    const getProducts = async () => {
        try {
            var ref = fireman.database().ref(`/favourites/${phone}`)
            ref.once('value', function (snapshot) {
                var vitu = []
                snapshot.forEach(function (childSnapshot) {
                    var key = childSnapshot.key
                    var data = childSnapshot.val()

                    vitu.push({ key: key, name: data.name, price: data.price, pic: data.pic })
                })
                setProducts(vitu)
                console.log(productsdata)
            })
        } catch (err) {
            log.error(err)
        }
    }

    useEffect(() => {
        log.debug(key)
        //getItems()

        getItems()
        LogBox.ignoreLogs(['Setting a timer']);
        //getProducts()
        getCartItems()
        BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
        };
    }, [])

    return (
        <View style={styles.container}>

            <StatusBar translucent backgroundColor='transparent' barStyle='dark-content' />
            <View style={{ paddingTop: 0 }}>
                <View style={{ paddingTop: 20, paddingBottom: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text style={{ fontFamily: 'Open Sans Bold', fontSize: 23 }}>Products</Text>
                </View>
                {
                    loading ? <View
                        style={{
                            paddingTop: height * 1 / 10,
                            justifyContent: 'center',
                            alignContent: 'center',
                            alignItems: 'center'
                        }}>
                        <Spinner />
                    </View> :
                        <View
                            style={{
                                justifyContent: 'center',
                                paddingBottom: 60
                            }}
                        >
                            <View style={{ alignItems: 'center', position: 'relative', paddingBottom: 10 }}>
                                {
                                    noob ?
                                        <View style={{ width: width * 1 / 1.1, backgroundColor: '#f6f6f6', padding: 20, flexDirection: 'column', borderRadius: 20 }}>
                                            <Text style={{ fontFamily: 'Open Sans Regular', color: '#5b5a5a' }}>Sign up for Zanifu credit to unlock loans to pay for your stock. You can still place orders and pay via Mpesa or on delivery</Text>
                                            <View>
                                                <Button style={{ width: width * 1 / 1.8, padding: 2, marginTop: 20 }} color='#3b3d3e' uppercase={false} mode="contained" onPress={
                                                    () => {
                                                        navigation.navigate('Business')
                                                    }
                                                }><Text style={{ fontFamily: 'Open Sans Regular', color: 'white' }}>Apply for zanifu credit</Text></Button>
                                            </View>
                                        </View> :
                                        <View style={{
                                            backgroundColor: '#f6f6f6',
                                            height: 80,
                                            borderRadius: 8,
                                            padding: 10,
                                            width: width * 1 / 1.2,
                                            flexDirection: 'column'
                                        }}>
                                            <Text style={{ fontFamily: 'Open Sans Regular', fontSize: 17, color: 'grey' }}>Your loan limit is</Text>
                                            <Text style={{ fontFamily: 'Open Sans Regular', fontSize: 17, color: 'black' }}>Kes 48, 000</Text>
                                        </View>
                                }
                            </View>

                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={refreshing}
                                        onRefresh={onRefresh}
                                    />
                                }
                                contentContainerStyle={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    alignContent: 'center',
                                    //position:'relative'
                                    //width: width
                                }}
                                numColumns={1}
                                data={allproducts}
                                renderItem={
                                    (item) => (
                                        <Pressable onPress={() => {
                                            /*navigation.navigate('Product', {
                                                picked: item.item.id
                                            })*/

                                        }}>
                                            <View
                                                style={{
                                                    //width: width ,
                                                    borderRadius: 10,
                                                    height: 140,
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                    //alignContent: 'center',

                                                }}
                                            >
                                                <View
                                                    style={{
                                                        flexDirection: 'row',
                                                        //elevation: 2,
                                                        backgroundColor: "white",
                                                        borderRadius: 0,
                                                        height: 120,
                                                        width: width * 1 / 1.1
                                                    }}>

                                                    <Image
                                                        style={{
                                                            height: 120,
                                                            width: 100,
                                                            borderTopLeftRadius: 0,
                                                            borderBottomLeftRadius: 0
                                                        }}
                                                        source={{ uri: item.item.image }} />
                                                    <View
                                                        style={{ paddingLeft: 20, flexDirection: 'column', alignContent: 'center', paddingTop: 20, width: width * 1 / 1.6 }}
                                                    >

                                                        <Text style={{
                                                            fontFamily: 'Open Sans Regular', fontSize: 16
                                                        }}>{item.item.title}</Text>


                                                        <View >
                                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingRight: 20, paddingTop: 0 }}>
                                                                <Text style={{
                                                                    fontFamily: 'Open Sans SemiBold', fontSize: 16
                                                                }}>{`KES ${item.item.price}`}</Text>
                                                                <NumericInput
                                                                    rounded
                                                                    //borderColor="black"
                                                                    totalWidth={80}
                                                                    onChange={
                                                                        value => {

                                                                            var id = item.item.id
                                                                            var qty = value
                                                                            //var jsa = { 'id': id, 'quantity': qty }

                                                                            var itemname = item.item.title.replace(/[^a-zA-Z ]/g, "");

                                                                            /*var ref = fireman.database().ref(`carts/${key}`)
                                                                            ref.child(itemname).set({
                                                                                //'name': item.name,
                                                                                'id': id,
                                                                                'quantity': qty,
                                                                            })
                                                                            */


                                                                            var ref = fireman.database().ref(`cartitems/${phone}`)
                                                                            ref.child(itemname).set({
                                                                                //'name': item.name,
                                                                                'id': id,
                                                                                'name': item.item.title,
                                                                                'image': item.item.image,
                                                                                'price': item.item.price,
                                                                                'distrtibutor': item.item.distributor.distributor_name,
                                                                                'quantity': qty,
                                                                            })




                                                                            //setThings(oldArray => [...oldArray, { id:id, quantity: qty }])
                                                                            //tut.push({ id:id, quantity: qty })
                                                                            Vibration.vibrate(1)

                                                                            //log.debug(tut)

                                                                            for (var i = 0; i < tut.length; i++) {
                                                                                //log.debug(tut[i]["id"])

                                                                                if (tut.length > 0) {
                                                                                    if (tut[i]["id"] == id) {
                                                                                        //delete tut[i]
                                                                                        tut.splice(i, 1)
                                                                                    }

                                                                                }

                                                                            }

                                                                            tut.push({
                                                                                'id': id,
                                                                                'name': item.item.title,
                                                                                'image': item.item.image,
                                                                                'price': item.item.price,
                                                                                'distrtibutor': item.item.distributor.distributor_name,
                                                                                'quantity': qty,
                                                                            })


                                                                            var jut = qty * item.item.price

                                                                            tutfull.push(jut)



                                                                            log.debug(tut)

                                                                            setIscart(true)

                                                                            /*dispatch({
                                                                                type: "ADD_CART2",
                                                                                payload: tut
                                                                            })*/

                                                                            //log.debug(cart2)
                                                                            //color.value= 'white'
                                                                            // setColor('white')

                                                                        }
                                                                    } />
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>

                                            </View>
                                            <View style={{ backgroundColor: '#b3b3b3', height: 1 }}></View>

                                        </Pressable>
                                    )
                                }
                            />

                            {
                                iscart ?
                                    <View style={{
                                        //backgroundColor: '#3b3d3e',
                                        //borderRadius: 6,
                                        position: 'absolute',
                                        //right: width * 1 / 3.8,
                                        top: height * 1 / 1.4,
                                        //margin: 8,
                                        width: width,
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        //paddingLeft: 20,
                                        //paddingRight: 20,
                                        //height: 50,
                                        alignContent: 'center',
                                        justifyContent: 'center'
                                    }}>

                                        <Button
                                            onPress={() => navigation.navigate('Cart', { data: tut })}
                                            color='#3b3d3e'
                                            uppercase={false}
                                            icon=""
                                            labelStyle={{
                                                fontFamily: 'Open Sans Bold'
                                            }}
                                            contentStyle={{
                                                height: 60,
                                                width: width * 1 / 1.2,
                                            }}
                                            mode="contained"
                                        >
                                            View Cart
                                        </Button>
                                    </View> : <View />
                            }




                            {/*<FAB
                style={{backgroundColor:'#3b3d3e', borderRadius: 10, position: 'absolute', right: width * 1/3.8, top: height * 1/1.7, margin: 8, width: width * 1/2, height: 40}}
                //small
                color="white"
                label="My cart"
                icon="basket"
                onPress={() => {

                    navigation.navigate('Cart', {
                        data : tut
                    })
                }
                }
            />*/}

                        </View>



                }


            </View>

        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        paddingTop: Constants.statusBarHeight * 2,
        padding: 8
    },
    cartText: {
        fontFamily: 'Open Sans Bold'
    },
    toCart: {
        flexDirection: 'row',
        paddingLeft: 20
    },
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 10,
        width: 20
    },
    modalView: {
        margin: 20,
        width: width * 1 / 1.2,
        height: 50,
        backgroundColor: "#3b3d3e",
        borderRadius: 10,
        //padding: 35,
        justifyContent: 'center',
        padding: 10,
        //alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        bottom: 60,
        position: 'absolute'
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    centeredView: {
        flex: 2,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    fab2: {
        position: 'absolute',
        width: 56,
        height: 56,
        alignItems: 'center',
        justifyContent: 'center',
        right: 20,
        bottom: 20,
        backgroundColor: '#03A9F4',
        borderRadius: 30,
        elevation: 8
    },
    fabIcon: {
        fontSize: 40,
        color: 'white'
    }

})
