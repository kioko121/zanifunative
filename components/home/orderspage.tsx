import * as React from 'react';
import { View, StyleSheet, TouchableNativeFeedback, Image, RefreshControl, StatusBar } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect } from 'react'
import {Button, Text,  Spinner, Divider } from '@ui-kitten/components';

import { fireman } from '../../api/base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector } from 'react-redux';
import { logger } from "react-native-logs"
import { FlatList } from 'react-native-gesture-handler';


export default function Orderspage({ navigation }) {

    const [orders, setOrders] = useState([])
    const key = useSelector((state) => state.key)
    const log = logger.createLogger()
    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        getMyOrders()
        setRefreshing(false)
    }, []);

    const pressMe = (oid) => {
        console.log(oid)
        //AsyncStorage.clear()
        AsyncStorage.removeItem('oid')
        AsyncStorage.multiSet([
            ['oid', oid]
        ])

        global.first = oid

        navigation.navigate('Myorder', {
            ver: oid
        })

        AsyncStorage.removeItem('foo')
        AsyncStorage.setItem('foo', oid)
    }

    const getMyOrders = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${key}`);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("https://development.zanifu.com/api/carts", requestOptions)
            .then(response => {
                var status = response.status
                log.info(status)
                if (status == 200) {
                    response.text().then((data) => {
                        var vitu = JSON.parse(data)
                        console.log(vitu)
                        setOrders(vitu.data)
                    })
                }

            })
            .catch(error => console.log('error', error));
    }

    const getOrders = () => {
        var simu = fireman.auth().currentUser?.phoneNumber
        var ref = fireman.database().ref(`/orders/${simu}`)
        ref.once('value', function (snapshot) {
            var vitu = []
            snapshot.forEach(function (childSnapshot) {
                var key = childSnapshot.key
                var data = childSnapshot.val()

                vitu.push({ key: key, user: data.user, cart: data.cart, time: data.time, total: data.total, status: data.status })
            })
            setOrders(vitu)
        })
    }
    useEffect(() => {
        getMyOrders()
    }, [])


    return (
        <View style={styles.container}>
        <StatusBar translucent backgroundColor='transparent' barStyle='dark-content' />
            <View style={{ paddingTop: 60, paddingBottom: 40 }}>
                <Text style={{ fontFamily: 'Open Sans Bold', fontSize: 25, color: 'black' }}>Orders</Text>
            </View>
            {orders.length <= 0 ?
                <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                    <View style={{ backgroundColor: '#c3c3c3', height: 100, width: 100, justifyContent: 'center', alignItems: 'center', borderRadius: 50 }}>
                        <Image source={require('../../assets/bag.png')} width={30} />
                    </View>
                    <View style={{ paddingTop: 20 }}>
                        <Text style={{ fontFamily: 'Open Sans Regular', color: 'grey' }}>There are no orders at the moment</Text>
                    </View>

                    <View style={{ paddingTop: 30 }}>
                        <Button style={{ backgroundColor: '#767272', borderColor: '#767272' }} onPress={() => navigation.navigate('Home')}>
                            <Text style={{ fontFamily: 'Open Sans Regular', color: '#fff' }}>
                                Place orders
                            </Text>
                        </Button>
                    </View>
                </View>
                : <View>

                    {
                        refreshing ? <View style={{ alignItems: 'center' }}>
                            <Spinner />
                        </View> : <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={refreshing}
                                    onRefresh={onRefresh}
                                />
                            }
                            data={orders}
                            renderItem={
                                (item) => (
                                    <TouchableNativeFeedback onPress={() => navigation.navigate('Myorder', {
                                        itemid: item.item.id
                                    })}>
                                        <View style={{ flexDirection: 'column' }}>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                <Text style={{
                                                    fontFamily: 'Open Sans SemiBold', fontSize: 17, color: 'grey'
                                                }}>{`KES: ${item.item.amount}`}</Text>
                                                <View style={{ borderRadius: 20, backgroundColor: '#c0c0c0', width: 100, justifyContent: 'center', alignItems: 'center' }}>
                                                    <Text style={{
                                                        fontFamily: 'Open Sans Regular', color:'black'
                                                    }}>{!item.item.status ? "Undeclared" : item.item.status}</Text>
                                                </View>



                                            </View>

                                            <View style={{ paddingTop: 15 }}>
                                                <Text style={{ fontFamily: 'Open Sans Regular', color: 'black' }}>{`${item.item.products_count} items`}</Text>
                                                <Text style={{ fontFamily: 'Open Sans SemiBold', color: 'grey', fontSize: 12 }}>{`${item.item.created_at}`}</Text>


                                            </View>

                                            <View style={{ paddingTop: 10, paddingBottom: 10 }}>
                                                <Divider />
                                            </View>
                                        </View>
                                    </TouchableNativeFeedback>
                                )
                            }
                        />
                    }

                </View>}
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'Inter_900Black',
        flex: 1,
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        padding: 8,
        paddingRight: 20,
        paddingLeft: 20
    },
    container2: {
        fontFamily: 'Inter_900Black',
        flex: .5,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },

    select: {

    },
    fab: {
        position: 'absolute',
        margin: 6,
        right: 0,
        bottom: 10,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    bottomNavigationView: {
        backgroundColor: '#fff',
        width: '100%',
        height: 250,
        justifyContent: 'center',
        alignItems: 'center',
    }
});
