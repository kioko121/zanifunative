import * as React from 'react'
import { ScrollView, View, StyleSheet, ImageBackground, Pressable, StatusBar, Vibration } from 'react-native';
import { useState, useEffect } from 'react'
import { useFonts } from '@expo-google-fonts/inter';

import Constants from 'expo-constants';
import { Text } from '@ui-kitten/components';
import { MaterialCommunityIcons } from '@expo/vector-icons';


import { Dimensions } from 'react-native';
import { logger } from "react-native-logs"
const { width, height } = Dimensions.get("window")
import { useSelector } from 'react-redux';
import NumericInput from 'react-native-numeric-input';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { BlurView } from 'expo-blur';
import { fireman } from '../../api/base';
import { Spinner } from '@ui-kitten/components';

export default function Productpage({ route, navigation }) {

    let [fontsLoaded] = useFonts({
        'Hindbold': require('../../assets/HindSiliguri-Bold.ttf'),
        'Hindregular': require('../../assets/HindSiliguri-Regular.ttf'),
        'Hindmedium': require('../../assets/HindSiliguri-Medium.ttf'),
        'Hindsemi': require('../../assets/HindSiliguri-SemiBold.ttf'),
    })

    const key = useSelector((state) => state.key)
    const { picked } = route.params
    const log = logger.createLogger()
    const [image, setImage] = useState()
    const [title, setTitle] = useState()
    const [category, setCategory] = useState()
    const [description, setDescription] = useState()
    const [price, setPrice] = useState()
    const [distributor, setDistributor] = useState()
    const [productid, setProductid] = useState()
    const [loading, setLoading] = useState(true)
    const [love, setLove] = useState(false)

    const checkFavourite = () => {

        var ref = fireman.database().ref(`favourites/${key}`)
        var query = ref.orderByChild('id').equalTo(picked)
        query.once('value', function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var data = childSnapshot.val()
                if (data) {
                    setLove(true)
                }

            })
        })
    }

    const getProduct = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Accept", "application/json");
        myHeaders.append("Authorization", `Bearer ${key}`);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch(`https://development.zanifu.com/api/products/${picked}`, requestOptions)
            .then(response => {
                var status = response.status

                response.text().then((data) => {
                    var vitu = JSON.parse(data)
                    log.debug(vitu)
                    setImage(vitu.data.image)
                    setTitle(vitu.data.title)
                    setProductid(vitu.data.id)
                    setDescription(vitu.data.description)
                    setPrice(vitu.data.price)
                    setDistributor(vitu.data.distributor.distributor_name)
                    setCategory(vitu.data.category.merchant_type)
                    setLoading(false)
                })
            })
            .catch(error => console.log('error', error));
    }

    useEffect(() => {
        getProduct()
        checkFavourite()
    }, [])

    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <Spinner
                    />
                </View>

            </View>
        )
    } else {
        return (
            <View>
                {
                    loading ?
                        <View style={{ backgroundColor: '#fff', height: height + Constants.statusBarHeight, justifyContent: 'center', alignItems: 'center' }}>
                            <Spinner />
                        </View>
                        : <ScrollView style={{ backgroundColor: '#fff', height: height + Constants.statusBarHeight }}>
                            <StatusBar translucent backgroundColor='transparent' barStyle='default' />

                            <ImageBackground
                                source={{ uri: image }}
                                style={{
                                    height: height * 1 / 2,
                                    width: width
                                }}
                            >
                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    paddingTop: Constants.statusBarHeight * 2
                                }}>
                                    <View style={{
                                        backgroundColor: '#e3e9ee',
                                        borderRadius: 10,
                                        marginLeft: 20,
                                        elevation: 2
                                    }}>
                                        <Pressable onPress={() => navigation.goBack()}>
                                            <MaterialCommunityIcons name="chevron-left" size={33} />
                                        </Pressable>
                                    </View>
                                    <View style={{
                                        paddingRight: 20
                                    }}>
                                        {love ? <Pressable onPress={() => {

                                        }
                                        }>
                                            <MaterialCommunityIcons name="heart" color="red" size={20} />
                                        </Pressable> :
                                            <Pressable onPress={() => {
                                                var itemname = title.replace(/[^a-zA-Z ]/g, "");
                                                var ref = fireman.database().ref(`favourites/${key}`)

                                                ref.child(itemname).set({
                                                    'price': price,
                                                    'name': title,
                                                    'id': productid,
                                                    'pic': image,
                                                    'time': new Date().toLocaleString()
                                                })

                                                checkFavourite()
                                            }
                                            }>
                                                <MaterialCommunityIcons name="heart-outline" color="grey" size={20} />
                                            </Pressable>
                                        }
                                    </View>


                                </View>
                            </ImageBackground>
                            <View style={{
                                paddingLeft: 20,
                                paddingRight: 20,
                                paddingTop: 40,
                                width: width,
                                flexDirection: 'row',
                                justifyContent: 'space-between'
                            }}>
                                <View style={{ width: width * 1 / 1.7 }}>
                                    <Text
                                        numberOfLines={2}
                                        ellipsizeMode='tail'
                                        style={{
                                            fontFamily: 'Hindbold',
                                            fontSize: 20,
                                        }}
                                    >{title}</Text>
                                    <Text
                                        style={{
                                            fontFamily: 'Hindsemi',
                                            color: '#476194'
                                        }}
                                    >{distributor}</Text>
                                </View>

                                <View
                                    style={{
                                        backgroundColor: '#d7dbe2',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        padding: 10,
                                        height: 50,
                                        borderRadius: 10
                                    }}
                                >
                                    <NumericInput
                                        iconStyle={{

                                        }}
                                        containerStyle={{
                                            elevation: 5,
                                            borderWidth: 0
                                        }}
                                        inputStyle={{
                                            fontFamily: 'Hindsemi',
                                            backgroundColor: '#d7dbe2',
                                            borderWidth: 0
                                        }}
                                        rounded
                                        totalWidth={80}
                                        onChange={
                                            value => {
                                                var id = productid
                                                var qty = value
                                                //var jsa = { 'id': id, 'quantity': qty }

                                                var itemname = title.replace(/[^a-zA-Z ]/g, "");

                                                /*var ref = fireman.database().ref(`carts/${key}`)
                                                ref.child(itemname).set({
                                                    //'name': item.name,
                                                    'id': id,
                                                    'quantity': qty,
                                                })
                                                */


                                                var ref = fireman.database().ref(`cartitems/${key}`)
                                                ref.child(itemname).set({
                                                    //'name': item.name,
                                                    'id': id,
                                                    'name': title,
                                                    'image': image,
                                                    'price': price,
                                                    'distrtibutor': distributor,
                                                    'quantity': qty,
                                                })

                                                Vibration.vibrate(1)
                                            }
                                        } />
                                </View>
                            </View>
                            <View style={{
                                paddingLeft: 20,
                                paddingRight: 20,
                                paddingTop: height * 1 / 17
                            }}>
                                <Text style={{
                                    fontFamily: 'Hindbold',
                                    fontSize: 17
                                }}>Description</Text>
                            </View>

                            <View style={{
                                paddingLeft: 20,
                                paddingRight: 20,
                                paddingTop: 10
                            }}>
                                <Text style={{
                                    fontFamily: 'Hindregular',
                                    fontSize: 16,
                                    color: 'grey'
                                }}>{description}</Text>
                            </View>

                            <View style={{
                                paddingTop: 20,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                paddingLeft: 20,
                                paddingRight: 20,
                                width: width
                            }}>

                                <Text style={{
                                    fontFamily: 'Hindbold',
                                    color: 'black',
                                    fontSize: 30,
                                }}>{`Ksh ${price}`}</Text>

               

                                <View>
                                    <View
                                        style={{
                                            borderRadius: 15,
                                            padding: 10,
                                            paddingLeft: 30,
                                            paddingRight: 30,
                                            backgroundColor: 'black'
                                        }}>
                                        <Pressable onPress={() => navigation.navigate('Cart')}>
                                            <Text style={{ fontFamily: 'Hindsemi', color: 'white' }}>Cart</Text>
                                        </Pressable>
                                    </View>
                                </View>

                            </View>

                            {/*}
                            <View style={{
                                justifyContent: 'center',
                                alignContent: 'center',
                                alignItems: 'center',
                                paddingTop: 20
                            }}>

                                <LinearGradient
                                    // Button Linear Gradient
                                    colors={['#307bd3', '#307bd3', '#307bd3']}
                                    style={{
                                        height: 70,
                                        width: width * 1 / 1.2,
                                        borderRadius: 15,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        flexDirection: 'row',
                                        elevation: 4
                                    }}
                                >
                                    <BlurView
                                        intensity={60}
                                        style={{
                                            width: 50,
                                            height: 50,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            borderRadius: 10,
                                            marginRight: 20
                                        }}>
                                        <Ionicons name="md-basket" size={28} color='#fff' />
                                    </BlurView>
                                    <Text style={{ fontFamily: 'Hindbold', color: '#fff' }} >Add to cart</Text>
                                    <View style={{ height: 30, backgroundColor: '#fff', width: 1, marginLeft: 20 }}></View>
                                    <View style={{ paddingLeft: 10 }}>
                                        <Text style={{
                                            fontFamily: 'Hindbold',
                                            color: '#fff'
                                        }}>{`KES ${price}`}</Text>
                                    </View>
                                </LinearGradient>
                            </View>
                            */}

                        </ScrollView>
                }
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        //padding: 8,

        //alignItems: 'center',
        paddingTop: Constants.statusBarHeight,
        paddingLeft: 20,
        paddingRight: 20
        //flexDirection: 'column',
        //justifyContent: 'space-between'
        //alignContent: 'center',
        //justifyContent:'center'
    }
})