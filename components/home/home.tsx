import * as React from 'react';
import { View, StyleSheet, TouchableNativeFeedback, ToastAndroid, ScrollView } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect } from 'react'
import { ApplicationProvider, Button, Layout, Text, Input, Icon, Select, SelectItem, Card, Spinner } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';

import { NavigationActions } from 'react-navigation'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import Homepage from './homepage';
import Orderspage from './orderspage';
import Myorder from '../orders/myorder';
import Loans from '../loans/loans';
import Payloan from '../loans/payloan';
import Profile from '../profile/profile';
import Login from '../auth/login';
import Category from '../auth/category';
import Productpage from './productpage';
import { BlurView } from 'expo-blur';
import { Dimensions } from 'react-native';
import Ionicons from '@expo/vector-icons/build/Ionicons';
const { width, height } = Dimensions.get("window")
import { useDispatch, useSelector } from 'react-redux';

function SettingsScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Settings!</Text>
        </View>
    );
}

export default function Home({ navigation }) {
    const key = useSelector((state) => state.key)

    const Tab = createBottomTabNavigator()

    let [fontsLoaded] = useFonts({
        'Hindbold': require('../../assets/HindSiliguri-SemiBold.ttf'),
        'Hindregular': require('../../assets/HindSiliguri-Regular.ttf'),
        'Hindmedium': require('../../assets/HindSiliguri-Medium.ttf'),
    })

    const checkUser = async () => {

        if (!key) {

            navigation.navigate('Login')
        }

    }

    useEffect(() => {
        checkUser()
    }, [])
    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <Spinner
                    />
                </View>
            </View>
        )
    } else {
        return (

            <Tab.Navigator
                screenOptions= {({ route }) => ({
                    //unmountOnBlur: true,
                    //unmountOnExit: true,
                    tabBarActiveTintColor: "#574b97",
                    tabBarStyle: {
                        paddingBottom: 10,
                        paddingTop: 10,
                        height: 70,
                        backgroundColor: 'white',
                        elevation: 0,
                        //opacity: 0.5
                        //borderTopLeftRadius: 25,
                        //borderTopRightRadius: 25
                    },
                    tabBarIcon: ({ focused, color, size }) => {
                        let iconName;

                        if (route.name === 'Shop') {
                            iconName = focused ? 'shopping' : 'shopping-outline';
                        } else if (route.name === 'Orders') {
                            iconName = focused ? 'clipboard' : 'clipboard-text-outline';
                        }
                        else if (route.name === 'Loan') {
                            iconName = focused ? 'cash' : 'cash-multiple';
                        } else if (route.name === 'Profile') {
                            iconName = focused ? 'account-cog' : 'account-cog-outline';
                        }

                        // You can return any component that you like here!
                        return <MaterialCommunityIcons name={iconName} color={color} size={20} />

                    },


                })}
            >
                <Tab.Screen
                    name="Shop"
                    options={{
                        headerShown: false,
                        unmountOnBlur: true
                    }}
                    component={Homepage} />
                <Tab.Screen
                    name="Orders"
                    options={{
                        headerShown: false
                    }}
                    component={Orderspage} />

                <Tab.Screen
                    name="Loan"
                    options={{
                        headerShown: false
                    }}
                    component={Loans} />

                <Tab.Screen
                    name="Profile"
                    options={{
                        headerShown: false
                    }}
                    component={Profile} />

                <Tab.Screen

                    name="Myorder"
                    tab
                    options={{
                        tabBarLabelStyle: { fontFamily: 'Hindregular', fontSize: 13 },
                        hideheader: true,
                        headerShown: false,
                        tabBarButton: () => null,
                        tabBarVisible: false,
                        unmountOnBlur: true

                    }}
                    component={Myorder} />

                <Tab.Screen

                    name="Payloan"
                    tab
                    options={{
                        tabBarLabelStyle: { fontFamily: 'Hindregular', fontSize: 13 },
                        hideheader: true,
                        headerShown: false,
                        tabBarButton: () => null,
                        tabBarVisible: false,

                    }}
                    component={Payloan} />



            </Tab.Navigator>



        )
    }
}


const styles = StyleSheet.create({
    container: {
        fontFamily: 'Inter_900Black',
        flex: 1,
        justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        padding: 8,
    },
    container2: {
        fontFamily: 'Inter_900Black',
        flex: .5,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
    },

    select: {

    },
    fab: {
        position: 'absolute',
        margin: 6,
        right: 0,
        bottom: 10,
    },
    indicator: {
        justifyContent: 'center',
        alignItems: 'center',
    },
});
