import * as React from 'react'
import { ImageBackground, View, StyleSheet, ActivityIndicator, KeyboardAvoidingView, ScrollView, Image, TouchableNativeFeedback, Pressable } from 'react-native';
import Constants from 'expo-constants';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, Button, Layout, Text, Input, Icon, Select, SelectItem, Card } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import { fireman } from '../../api/base';

import { Dimensions } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get("window")
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { logger , consoleTransport} from "react-native-logs"
import { useDispatch, useSelector } from 'react-redux';

export default function Categories({ navigation }) {

    const defaultConfig = {
        severity: "debug",
        transport: consoleTransport,
        transportOptions: {
          color: "ansi", // custom option that color consoleTransport logs
        },
        levels: {
          debug: 0,
          info: 1,
          warn: 2,
          error: 3,
        },
        async: true,
        dateFormat: "time",
        printLevel: true,
        printDate: true,
        enabled: true,
      };

    const log = logger.createLogger(defaultConfig)

    const dispatch = useDispatch()



    const categories = [
        { 'name': 'Bakery', 'iconname': 'cake', 'iconcolor': '#fe0085' },
        { 'name': 'Barbers', 'iconname': 'content-cut', 'iconcolor': 'blue' },
        { 'name': 'Butchery', 'iconname': 'sausage', 'iconcolor': '#fe3a00' },
        { 'name': 'Book Store', 'iconname': 'bookshelf', 'iconcolor': '#0098fe' },
        { 'name': 'Car wash', 'iconname': 'car-wash', 'iconcolor': '#00e138' },
        { 'name': 'Chemicals', 'iconname': 'chemical-weapon', 'iconcolor': '#e10000' },
        { 'name': 'Children', 'iconname': 'human-male-child', 'iconcolor': 'black' },
        { 'name': 'Cigar stores', 'iconname': 'cigar', 'iconcolor': '#5d00e1' },
        { 'name': 'Commercial footwear', 'iconname': 'foot-print', 'iconcolor': 'black' },
        { 'name': 'Computers', 'iconname': 'mouse', 'iconcolor': '#00d1e1' },
        { 'name': 'Dairy products', 'iconname': 'cow', 'iconcolor': '#e1cc00' },
        { 'name': 'Drinking places', 'iconname': 'beer', 'iconcolor': '#e17e00' },
        { 'name': 'Drug stores', 'iconname': 'medical-bag', 'iconcolor': 'red' },
        { 'name': 'Eating places', 'iconname': 'food', 'iconcolor': 'green' },
        { 'name': 'Electric parts', 'iconname': 'electric-switch', 'iconcolor': 'blue' },
        { 'name': 'Fast food', 'iconname': 'food-drumstick', 'iconcolor': 'purple' },
        { 'name': 'Eating places', 'iconname': 'food', 'iconcolor': 'green' },
        { 'name': 'Florist', 'iconname': 'flower', 'iconcolor': '#e1008d' },
        { 'name': 'Glassware', 'iconname': 'window-open', 'iconcolor': '#009ae1' },
        { 'name': 'Hardware stores', 'iconname': 'hard-hat', 'iconcolor': 'black' },
        { 'name': 'Health and Beauty', 'iconname': 'all-inclusive', 'iconcolor': 'pink' },
        { 'name': 'Household', 'iconname': 'silverware-spoon', 'iconcolor': '#009ae1' },
        { 'name': 'Industrial', 'iconname': 'warehouse', 'iconcolor': 'black' },
        { 'name': 'Medical, Dental', 'iconname': 'hospital-building', 'iconcolor': 'red' },
        { 'name': 'Uniforms', 'iconname': 'doctor', 'iconcolor': 'maroon' },
        { 'name': 'General', 'iconname': 'circle', 'iconcolor': 'black' },
        { 'name': 'Motorcycle dealers', 'iconname': 'motorbike', 'iconcolor': 'green' },
        { 'name': 'Vehicle parts', 'iconname': 'car', 'iconcolor': 'purple' },
        { 'name': 'Package stores', 'iconname': 'package-variant', 'iconcolor': 'orange' },
        { 'name': 'Paints', 'iconname': 'format-paint', 'iconcolor': 'blue' },
        { 'name': 'Petroleum', 'iconname': 'oil-lamp', 'iconcolor': '#5d00e1' },
        { 'name': 'Phones', 'iconname': 'cellphone-android', 'iconcolor': 'black' },
        { 'name': 'Plumbing', 'iconname': 'pipe-leak', 'iconcolor': '#5d00e1' },
        { 'name': 'Sports apparel', 'iconname': 'football', 'iconcolor': '#00e1c2' },
        { 'name': 'Stationery', 'iconname': 'pencil', 'iconcolor': '#90e100' },
        { 'name': 'Supermarkets', 'iconname': 'shopping', 'iconcolor': '#e17600' },
        { 'name': 'Welding & repair', 'iconname': 'soldering-iron', 'iconcolor': '#e12700' },
    ]

    let [fontsLoaded] = useFonts({
        'Hindbold': require('../../assets/HindSiliguri-Bold.ttf'),
        'Hindregular': require('../../assets/HindSiliguri-Regular.ttf'),
        'Hindmedium': require('../../assets/HindSiliguri-Medium.ttf'),
        'Hindsemi': require('../../assets/HindSiliguri-SemiBold.ttf'),
    })

    if (!fontsLoaded) {
        return (
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center', display: 'flex', paddingLeft: 20, paddingRight: 20 }}>
                    <LottieView
                        autoPlay
                        style={{ width: 800, height: 800 }}
                        source={require('../../assets/9305-loading-bloob.json')}
                    />
                </View>

            </View>
        )
    } else {
        return (
            <ScrollView style={styles.container}>
                <View style={{ paddingTop: 20 }}>
                    <Text
                        style={{
                            fontFamily: 'Hindbold',
                            fontSize: 23
                        }}
                    >Pick a category</Text>
                </View>

                <View
                    style={{
                        width: width,
                        paddingBottom: 40
                    }}
                >
                    <FlatList
                        numColumns={3}
                        data={categories}
                        renderItem={
                            ( item) => (
                                <Pressable onPress={() => {
                                    //fireman.auth().signOut()
                                    log.info(item.index)

                                    dispatch({
                                        type: "ADD_CHOSEN",
                                        payload: item.index
                                    })

                                    navigation.navigate('Home', {
                                        chosen: item.index
                                    })
                                }}>
                                    <View style={{
                                        margin: 8,
                                        height: 100,
                                        width: 100,
                                        borderWidth: 1,
                                        borderRadius: 10,
                                        padding: 8
                                    }}>
                                        <View
                                            style={{
                                                position: 'absolute',
                                                top: 10,
                                                left: 10
                                            }}
                                        >
                                            <MaterialCommunityIcons name={item.item.iconname} color={item.item.iconcolor} size={20} />
                                        </View>
                                        <View
                                            style={{
                                                position: 'absolute',
                                                bottom: 10,
                                                left: 10
                                            }}
                                        >
                                            <Text style={{ fontFamily: 'Hindsemi' }}>{item.item.name}</Text>
                                        </View>
                                    </View>
                                </Pressable>
                            )
                        }
                    />
                </View>


            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#fff',
        padding: 0,
        paddingLeft: width * 1/45,
        paddingRight: width * 1/45,

        //alignItems: 'center'
    }
})