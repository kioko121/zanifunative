import * as React from 'react'
import { ScrollView, View, StyleSheet, Button } from 'react-native';
import { useState, useEffect, useRef } from 'react'
import { ApplicationProvider, List, Layout, Text, Input, ViewPager, Tooltip } from '@ui-kitten/components';
import { useFonts, Inter_900Black } from '@expo-google-fonts/inter';
import * as eva from '@eva-design/eva'
import LottieView from 'lottie-react-native'
import Constants from 'expo-constants';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import PhoneInput from 'react-native-phone-number-input';
//import { FirebaseRecaptchaVerifierModal } from 'expo-firebase-recaptcha';
import { Dimensions } from 'react-native';
import { logger } from "react-native-logs"
const { width, height } = Dimensions.get("window")
import { useDispatch, useSelector } from 'react-redux';
import { log } from 'react-native-reanimated';
import NumericInput from 'react-native-numeric-input';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { BlurView } from 'expo-blur';
import { Item } from 'react-native-paper/lib/typescript/components/List/List';
import Animated, { useSharedValue, useAnimatedStyle, withSpring } from 'react-native-reanimated';
import { fireman } from '../../api/base';

function wait(ms){
    var start = new Date().getTime();
    var end = start;
    while(end < start + ms) {
      end = new Date().getTime();
   }
 }
export default function Testa({ navigation }) {

    const [anheight, setAnheight] = useState(100)
    const [anwidth, setAnwidth] = useState(100)
    const [tomove, setTomove] = useState(0)

    const randomNumber = useSharedValue(100);

    const log = logger.createLogger()

    const cart = useSelector((state) => state.cart)

    const key = useSelector((state) => state.key)

    const offset = useSharedValue(0);

    const style = useAnimatedStyle(() => {
        //return { width: withSpring(randomNumber.value), height: withSpring(randomNumber.value) };
        return { width: withSpring(anwidth), height: withSpring(anheight) };
    });

    const customSpringStyles = useAnimatedStyle(() => {
        return {
          transform: [
            {
              translateX: withSpring(offset.value * 255, {
                damping: 20,
                stiffness: 90,
              }),
            },
          ],
        };
      });

    
    const getCart = async () => {
        try {
            var ninja = 0
            var things = []
            var ref = fireman.database().ref(`/carts/${key}`)
            ref.on('value', (snapshot) => {
                snapshot.forEach(function (childSnapshot) {
                    var key = childSnapshot.key
                    var data = childSnapshot.val()

                    log.debug(data)

                    /*var mig = data.price * data.quantity

                    var kis = ninja + mig

                    setNihowmuch(kis)

                    things.push({ name: data.name, price: data.price, quantity: data.quantity })*/
                })
            })

            //setCart(things)
            //console.log("*******************************")
            //console.log(things)

            //console.log(cart)
        } catch (err) {
            log.error(err)
        }
    }

    useEffect(() => {
        getCart()
    })

    return (
        <View style={{
            width: width,
            justifyContent: 'center',
            flex: 1,
            alignContent: 'center',
            flexDirection: 'column',
            alignItems: 'center'
        }}>
            <Animated.View style={[ styles.box,customSpringStyles]} />
            <Button onPress={() => {
                 //randomNumber.value = Math.random() * 350;
                //setAnheight(200)
                //setAnwidth(200)
                //setAnwidth(100)
                //setAnheight(100)
                //offset.value = Math.random()
                offset.value = 0.6
                wait(100)
                offset.value = 0
            }} title="press me" />
        </View>
    )
}

const styles = StyleSheet.create({
    box: {
        backgroundColor: 'black',
        marginBottom: 50,
        width: 100,
        height: 100,
        borderRadius: 15

    }
})